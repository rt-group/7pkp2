import { _decorator, Component, instantiate, Node, Prefab, ScrollView, SpriteFrame, UITransform } from 'cc';
import { ResMgr } from '../Manager/ResMgr';
import { Resource } from '../Resource';
import { Machine } from './Machine';
const { ccclass, property } = _decorator;

@ccclass('MachineTable')
export class MachineTable extends Component {
    public static Instance: MachineTable = null as unknown as MachineTable;

    @property(Prefab)
    machine: Prefab = null;

    @property(ScrollView)
    scrollView: ScrollView = null;

    content: Node = null;
    high: number =  208; //element 高
    pageNum: Number = 12; //每頁各數
    value_set = [];
    item_set = [];
    startIndex = 0;
    startY = 0;
    machineIds = [];
    hasPeople = [];
      onLoad(): void{
        if(MachineTable.Instance === null){
            MachineTable.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    reset() {
        this.hasPeople = [];
        this.machineIds = [];
        this.item_set = [];
        this.value_set = [];
        this.startIndex = 0;
        this.startY = 0;
        if(this.content){
            this.content.removeAllChildren();
        }
    }

    initData(message) {
        this.value_set = [];
        this.machineIds = message.machineIds.sort(function(a, b) {
            return a-b
          });;
        this.hasPeople = message.hasPeople.sort(function(a, b) {
            return a-b
          });;;

        for(let a = 0; a <= this.machineIds.length; a++){
            this.value_set.push(a);
        }

        this.content = this.scrollView.content;
        this.item_set = [];

        //每次加載3頁
        for(let i = 0; i < this.machineIds.length; i ++){
            let item = instantiate(this.machine);
            item.getComponent(Machine).setMachineId(this.machineIds[i]);
            if(this.hasPeople.indexOf(this.machineIds[i]) != -1){
                item.getComponent(Machine).setPeopleStatus(true);
            }else{
                item.getComponent(Machine).setPeopleStatus(false);
            }
            this.content.addChild(item);
            this.item_set.push(item);
        }

        this.scrollView.node.on(ScrollView.EventType.SCROLL_ENDED, this.onScrollEnd.bind(this), this);

        this.startY = this.content.getPosition().y;
        this.startIndex = 0;
    }


    updateScroll() {
        //向下加载数据
        //当开始位置比value_set的长度小则代表没加载完
        if(this.startIndex + (this.pageNum as number ) * 3 < this.value_set.length &&
            this.content.getPosition().y >= this.startY + (this.pageNum as number ) * 2 * this.high)//content超过2个PAGE的高度
          {
              //_autoScrolling在引擎源码中负责处理scrollview的滚动动作
              if(this.scrollView.isAutoScrolling){ //等自动滚动结束后再加载防止滚动过快，直接跳到非常后的位置
                  this.scrollView.elastic = false; //关闭回弹效果 美观
                  return;
              }
              var down_loaded = (this.pageNum as number ); 
              this.startIndex += down_loaded;
              
              if(this.startIndex + (this.pageNum as number ) * 3>this.value_set.length)
              {
                  //超过数据范围的长度
                  var out_len = this.startIndex + (this.pageNum as number ) * 3 - this.value_set.length;
                  down_loaded -= out_len;
                  this.startIndex -= out_len;
              }
            //   this.load_recode(this.startIndex);
              this.content.getPosition().y -= down_loaded * this.high;
              return;
          }
          //向上加载
          if(this.startIndex>0 && this.content.getPosition().y<=this.startY)
          {
              if(this.scrollView.isScrolling){ 
                  this.scrollView.elastic = false;
                  return;
               }
              var up_loaded = this.pageNum as number;
              this.startIndex -= up_loaded;
              if(this.startIndex<0){
                  up_loaded +=this.startIndex;
                  this.startIndex=0;
              }
            //   this.load_recode(this.startIndex);
              this.content.getPosition().y += up_loaded * this.high;
          }
    }

    onScrollEnd() {
        this.scrollView.elastic = true; 
    }

    update(dt) {
        // this.updateScroll();
    }

    loadRecord(startIdx) {
        this.startIndex = startIdx;
        for(let i = 0; i < (this.pageNum as number * 3); i ++) {
            
        }
    }


}

