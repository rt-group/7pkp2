import { _decorator, Button, Component, Label, Node, Sprite, SpriteFrame } from 'cc';
const { ccclass, property } = _decorator;
import proto from "proto";
import { MsgHelp } from '../Utils/MsgHelp';
import { Connect } from '../Connect/Connect';
@ccclass('Machine')
export class Machine extends Component {
    @property(Label)
    machineId: Label = null;

    @property(Sprite)
    hasPeople: Sprite = null;

    onLoad() {
    }

    start() {
        this.addEventListener();
    }

    addEventListener() {
        this.node.on(Button.EventType.CLICK, this.click, this);
    }

    setPeopleStatus(hasPeople) {
        if(hasPeople){
            this.hasPeople.node.active = true;
            this.node.getComponent(Button).enabled = false;
        }else{
            this.hasPeople.node.active = false;
            this.node.getComponent(Button).enabled = true;
        }
    }

    setMachineId(id) {
        this.machineId.string = id.toString();
    }

    click(event) {
        console.log(event);
        this.CS_GetMachineDetail(this.machineId.string);
    }

    CS_GetMachineDetail(id) {
        var getMachineDetail = proto.PK5Client.GetMachineDetial;
        var getMachineDetailData =getMachineDetail.create({
            machindId: id
        });
        var buffer = getMachineDetail.encode(getMachineDetailData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_GET_MACHINE_DETIAL;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
    }
}

