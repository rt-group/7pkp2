import { _decorator, Button, Component, Label, Node, Sprite } from 'cc';
import proto from "proto";
import { MsgHelp } from '../Utils/MsgHelp';
import { Connect } from '../Connect/Connect';
import { GameApp } from '../GameApp';
import { GameInfo, GAME_SCENE } from '../Utils/GameConfig';
import { RoomManager } from '../Manager/RoomManager';
import { UserInfo } from '../Common/UserInfo';
import { NumberUtil } from '../Utils/NumberUtil';
import { Loader } from '../Loader/Loader';
import { MachineTable } from './MachineTable';
import { Game } from '../Game/Game';

// import { RowLoader } from '../../../AssetPackage/Prefab/Load/RowLoader';
const { ccclass, property } = _decorator;

@ccclass('Lobby')
export class Lobby extends Component {
    public static Instance: Lobby = null as unknown as Lobby;
    @property(Button)
    closeBtn: Button = null;

    @property(Button)
    selectBtn: Button = null;

    @property(Label)
    machineId: Label = null;

    @property(Label)
    todayWinRate: Label = null;

    @property(Label)
    historyWinRate: Label = null;

    @property([Label])
    bonusList: Label [] = [];

    onLoad(): void{
        if(Lobby.Instance === null){
            Lobby.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.addEventListener();
        this.selectBtn.interactable = false;
        this.selectBtn.node.getComponent(Sprite).grayscale = true;
    }

    reset() {
        this.machineId.string = "";
        this.bonusList[0].string = "";
        this.bonusList[1].string = "";
        this.bonusList[2].string = "";
        this.bonusList[3].string = "";
        this.todayWinRate.string = "";
        this.historyWinRate.string = "";
        this.selectBtn.node.getComponent(Sprite).grayscale = true;
        this.selectBtn.interactable = false;
    }

    select() {
        this.CS_IntoGame();
        UserInfo.getInstance().setUserPlayingTable(parseInt(this.machineId.string));
        // Game.currentMachineId = this.machineId.string;
        GameInfo.currentMachineId = this.machineId.string;
    }

    addEventListener() {
        this.closeBtn.node.on(Node.EventType.TOUCH_END, this.backRoom, this);
        this.selectBtn.node.on(Node.EventType.TOUCH_END, this.select, this)
    }

    updateMachineData(data){
        this.machineId.string = data.machindId;
        this.bonusList[0].string = NumberUtil.formatNumber(data.jpList[1]);
        this.bonusList[1].string = NumberUtil.formatNumber(data.jpList[2]);
        this.bonusList[2].string = NumberUtil.formatNumber(data.jpList[3]);
        this.bonusList[3].string = NumberUtil.formatNumber(data.jpList[4]);
        GameInfo.currentMachineBonusList = data.jpList;
        this.todayWinRate.string = NumberUtil.formatNumber(Math.floor((data.toadyWinRate * 100))) + "%";
        this.historyWinRate.string = NumberUtil.formatNumber(Math.floor((data.totalWinRate * 100))) + "%";
        this.selectBtn.node.getComponent(Sprite).grayscale = false;
        this.selectBtn.interactable = true;
    }

    CS_IntoGame() {
        var intoGame = proto.PK5Client.IntoGame;
        var intoGameData =intoGame.create({
            machindId: this.machineId.string
        });
        var buffer = intoGame.encode(intoGameData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_INTO_GAME;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
        // RowLoader.Instance.showLoader();
        Loader.Instance.showRowLoader();
    }

    CS_GetMachineList() {
        var getMachineList = proto.PK5Client.GetMachineList;
        var getMachineListData =getMachineList.create({
        });
        var buffer = getMachineList.encode(getMachineListData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_GET_MACHINE_LIST;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
        // RowLoader.Instance.showLoader();
        Loader.Instance.showRowLoader();
    }


    backRoom() {
        MachineTable.Instance.reset();
        GameApp.Instance.CS_GetBetRange();
    }
}

