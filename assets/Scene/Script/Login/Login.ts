import { _decorator, Button, Component, EditBox, Node, sys } from 'cc';
import { GameApp } from '../GameApp';
import { GAME_SCENE, POPUP_MESSAGE } from '../Utils/GameConfig';
import { PopupManager } from '../Manager/PopupManager';
import { POPUP_BTN_TITLE, POPUP_ICON_TYPE, POPUP_TITLE } from '../Common/Popup';
import { ENV } from '../Connect/Enivornment';
import { Connect } from '../Connect/Connect';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import {AudioID} from '../../../../../framework-fish/Audio/AudioEnum';
const { ccclass, property } = _decorator;

@ccclass('Login')
export class Login extends Component {
    @property(EditBox)
    id: EditBox = null;

    @property(EditBox)
    pw: EditBox = null;

    @property(Button)
    loginBtn: Button = null;

    @property(Node)
    guestTable: Node = null;

    @property(Button)
    guestLoginBtn: Button = null;

    account: string = null;
    password: string = null;

    onLoad() {
        AudioPlayer.play(AudioID.BUTTON_CLICK);
    }
    
    start(){
        this.addEventListener();
        AudioPlayer.playBGM(AudioID.GAME_MUSIC);
    }

    checkTestStatus() {
        const url = new URL(window.location.href);//获取到页面的url地址
        if(url.searchParams.has("isTest")){
            this.guestTable.active = true;
        }else{
            this.guestTable.active = false;
        }
    }

    addEventListener() {
        this.id.node.on(EditBox.EventType.EDITING_DID_ENDED, this.idEditEnd, this);

        this.pw.node.on(EditBox.EventType.EDITING_DID_ENDED, this.pwEditEnd, this);

        this.guestLoginBtn.node.on(Button.EventType.CLICK, this.guestLogin, this);

        this.loginBtn.node.on(Node.EventType.TOUCH_END, this.login, this);

        this.checkTestStatus();
    }

    guestLogin() {
        const value = sys.localStorage.getItem("userData");
        if (value) {
            console.log("success", JSON.parse(value));
            ENV.Account = JSON.parse(value).accout;
            ENV.Pwd = JSON.parse(value).pwd;
            Connect.Instance.wsOpen();
            this.guestLoginBtn.enabled = false;
            this.guestLoginBtn.interactable = false;
        } else {
            console.log("fail");
            let xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
              if(xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 400)) {
                  let response = JSON.parse(xhr.responseText);
                  console.log(response);
                  sys.localStorage.setItem("userData", JSON.stringify(response.data));
                  ENV.Account = response.data.accout;
                  ENV.Pwd = response.data.pwd;
                  Connect.Instance.wsOpen();
              }
            };
            this.guestLoginBtn.enabled = true;
            this.guestLoginBtn.interactable = true;
            xhr.open("POST", "https://token.kaomstudio.com/guestLogin", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xhr.send("platform=web&channel=10003");
        }
    }

    idEditEnd(event) {
        console.log(event);
        this.account = event.string;
    }

    pwEditEnd(event) {
        console.log(event);
        this.password = event.string;
    }


    login() {
        if(this.password == null || this.account == null) {
            PopupManager.Instance.showPopup(POPUP_ICON_TYPE.ERROR_COMMON, POPUP_TITLE.SOMETHING_WENT_WRONG, POPUP_MESSAGE.WRONG_ACCOUNT_OR_PASSWORD, POPUP_BTN_TITLE.OKAY)
            return;
        }
        ENV.Account = this.account;
        ENV.Pwd = this.password;
        Connect.Instance.wsOpen();
    }


}

