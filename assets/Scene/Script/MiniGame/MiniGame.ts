import { _decorator, Button, Component, Node, Sprite } from 'cc';
import proto from "proto";
import { MsgHelp } from '../Utils/MsgHelp';
import { Connect } from '../Connect/Connect';
import { Game } from '../Game/Game';
import { Card, CARD_STATE } from '../Common/Card';
import { UserHandCard } from '../Common/UserHandCard';
import { UserInfo } from '../Common/UserInfo';
import { WinLoseLayer } from '../Game/WinLoseLayer';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { MiniGameBet } from './MiniGameBet';
import { SidePanel } from '../Common/SidePanel';
import { MiniGameInfo } from '../Utils/GameConfig';
const { ccclass, property } = _decorator;

export enum MINI_BET_TYPE {
    SMALL = 0,
    BIG = 1,
    DOUBLE = 2,
}

@ccclass('MiniGame')
export class MiniGame extends Component {
    public static Instance: MiniGame = null as unknown as MiniGame;

    @property(Button)
    bigBtn: Button = null;

    @property(Button)
    smallBtn: Button = null;

    @property(Button)
    doubleBtn: Button = null;

    @property(Button)
    cancelBtn: Button = null;

    @property(Button)
    skipBtn: Button = null;

    @property([Node])
    recordCard: Node[] = []

    @property(Node)
    bettingCard: Node = null;

    miniBetType: number = null;
    isBetChoose: boolean = false;
    betNum: number = null;
    onLoad(): void{
        if(MiniGame.Instance === null){
            MiniGame.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    reset() {
        this.hideSkipBtn();
        this.minibetReset();
        // MiniGameInfo.MINI_TYPE = null;
        if(this.node != null || this.node != undefined)
            this.recordCard.forEach((card) =>{ 
                card.getComponent(Card).setCardState(CARD_STATE.FLIPPED);
                card.getComponent(Card).miniGameReset();
                card.getComponent(Card).setTransparents(false);
                card.getComponent(UserHandCard).miniGameReset();
            });
            this.bettingCard.getComponent(Card).setCardState(CARD_STATE.FLIPPED);
            this.bettingCard.getComponent(Card).miniGameReset();
            this.bettingCard.getComponent(Card).setTransparents(false);
            this.bettingCard.getComponent(UserHandCard).miniGameReset();
    }

    bettingCardReset() {
        this.bettingCard.getComponent(Card).setCardState(CARD_STATE.FLIPPED);
        this.bettingCard.getComponent(Card).miniGameReset();
        this.bettingCard.getComponent(Card).setTransparents(false);
        this.bettingCard.getComponent(UserHandCard).miniGameReset();
    }

    start() {
        this.addEventListener();
        this.setBtnStatus(this.doubleBtn, false);
    }

    init() {
        let doubleBetList = Game.Instance.getFirstDoubleCardData();
        doubleBetList.forEach((element, index) =>{
            if(index >= this.recordCard.length)
                return;
            this.recordCard[index].getComponent(UserHandCard).updateUserHandData(element);
        })

        this.enableBtns();
        this.disableDouble();
        this.hideScoreBtn();
        this.hideSkipBtn();
    }

    updatePokerRecord(data: any) {
        this.bettingCard.getComponent(UserHandCard).updateUserHandData(data.winResult.winPoker[0]);

        let doubleBetList = data.DoubleBetList;

        doubleBetList.forEach((element, index) => {
            if(index > 0 && index <= this.recordCard.length)
                this.recordCard[index - 1].getComponent(UserHandCard).updateUserHandData(element);
        })
    }

    addEventListener() {
        this.bigBtn.node.on(Button.EventType.CLICK, this.betBig, this);
        this.smallBtn.node.on(Button.EventType.CLICK, this.betSmall, this);
        this.doubleBtn.node.on(Button.EventType.CLICK, this.betDouble, this);
        this.cancelBtn.node.on(Button.EventType.CLICK, this.miniGameCancel, this);
        this.skipBtn.node.on(Button.EventType.CLICK, this.skipScore, this);
        this.hideSkipBtn();
        this.hideScoreBtn();
    }

    showScoreBtn() {
        this.cancelBtn.node.active = true;
    }

    hideScoreBtn() {
        this.cancelBtn.node.active = false;
    }

    showSkipBtn() {
        this.skipBtn.node.active = true;
    }

    hideSkipBtn() {
        if(this.skipBtn)
        this.skipBtn.node.active = false;
    }

    disableDouble() {
        this.setBtnStatus(this.doubleBtn, false);
    }

    skipScore() {
        Game.Instance.skipScoreMode();
    }
     
    diableBtns() {
        this.setBtnStatus(this.bigBtn, false);
        this.setBtnStatus(this.smallBtn, false);
        this.setBtnStatus(this.doubleBtn, false);
        this.setBtnStatus(this.cancelBtn, false);
    }

    enableBtns() {
        this.setBtnStatus(this.bigBtn, true);
        this.setBtnStatus(this.smallBtn, true);
        this.setBtnStatus(this.doubleBtn, true);
        this.setBtnStatus(this.cancelBtn, true);
    }

    setBtnStatus(btn: Button, enable: boolean) {
        btn.interactable = enable;
        btn.getComponent(Sprite).grayscale = !enable;
    }

    minibetReset() {
        this.isBetChoose = false;
        this.betNum = null;
        this.setBtnStatus(this.doubleBtn, true);
        this.setBtnStatus(this.cancelBtn, true);
    }

    betBig() {
        // this.CS_BetDouble(1, 0, UserInfo.getInstance().getCurrentGameWinGold() * 2);
        if(Game.Instance.getMiniGameBet() > UserInfo.getInstance().getWalletBalance)
            return;
        if(this.isBetChoose || MiniGameBet.Instance.getBetChooseStatus()){
            if(this.betNum == null){
                this.betNum = MiniGameBet.Instance.getMiniBetNum();
                this.miniBetType = MiniGameBet.Instance.getMiniBetType();
            }
            this.CS_BetDouble(1, 0, this.betNum);
        }else{
            this.betNum = Game.Instance.getMiniGameBet() * 2;
            // this.miniBetType = MINI_BET_TYPE.BIG;
            // Game.Instance.setBonusScore(0);
            MiniGameInfo.MINI_TYPE = MINI_BET_TYPE.BIG;
            let userMoney = Game.Instance.getPlayerMoney() - Game.Instance.getMiniGameBet();
            Game.Instance.setMiniGameFakeMoney(userMoney);
            Game.Instance.setBonusScore(this.betNum);
            AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
            this.setBtnStatus(this.doubleBtn, false);
            this.setBtnStatus(this.cancelBtn, false);
            this.isBetChoose = true;
            SidePanel.Instance.disableLeaveBtn();
        }
    }

    betSmall() {
        // this.CS_BetDouble(0, 1, UserInfo.getInstance().getCurrentGameWinGold() / 2);
        if(this.isBetChoose  || MiniGameBet.Instance.getBetChooseStatus()){
            if(this.betNum == null){
                this.betNum = MiniGameBet.Instance.getMiniBetNum();
                this.miniBetType = MiniGameBet.Instance.getMiniBetType();
            }
            this.CS_BetDouble(0, 1, this.betNum);
        }else{
            this.betNum = Game.Instance.getMiniGameBet() /2;
            // this.miniBetType = MINI_BET_TYPE.SMALL;
            MiniGameInfo.MINI_TYPE = MINI_BET_TYPE.SMALL;
            let userMoney = Game.Instance.getPlayerMoney() + this.betNum;
            Game.Instance.setMiniGameFakeMoney(userMoney);
            Game.Instance.setBonusScore(Game.Instance.getMiniGameBet() / 2);
            AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
            this.setBtnStatus(this.doubleBtn, false);
            this.setBtnStatus(this.cancelBtn, false);
            this.isBetChoose = true;
            SidePanel.Instance.disableLeaveBtn();
        }
       
        // this.miniBetType = MINI_BET_TYPE.SMALL;
        // Game.Instance.setBonusScore(Game.Instance.getMiniGameBet() / 2);
        // AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        // Game.Instance.setUserFakeMoney(fake);
    }

    betDouble() {
        // this.CS_BetDouble(1, 0, UserInfo.getInstance().getCurrentGameWinGold());
        // this.CS_BetDouble(1, 0, Game.Instance.getMiniGameBet());
        this.setBtnStatus(this.doubleBtn, false);
        this.setBtnStatus(this.cancelBtn, false);
        this.betNum = Game.Instance.getMiniGameBet();
        // this.miniBetType = MINI_BET_TYPE.DOUBLE;
        MiniGameInfo.MINI_TYPE = MINI_BET_TYPE.DOUBLE;
        // Game.Instance.setBonusScore(0);
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        this.isBetChoose = true;
        SidePanel.Instance.disableLeaveBtn();
    }

    showMiniWinByType() {
        switch(this.miniBetType){
            case MINI_BET_TYPE.SMALL:
                WinLoseLayer.Instance.showPlayeSmallWin(this.showMiniWinEnd.bind(this));
                break;
            case MINI_BET_TYPE.BIG:
                WinLoseLayer.Instance.showPlayeBigWin(this.showMiniWinEnd.bind(this));
                break;
            case MINI_BET_TYPE.DOUBLE:
                WinLoseLayer.Instance.showPlayeDoubleWin(this.showMiniWinEnd.bind(this));
                break;
        }
    }

    showMiniWinEnd() {
        this.bettingCard.getComponent(Card).setCardState(CARD_STATE.FLIPPED);
        this.bettingCard.getComponent(Card).miniGameReset();
        this.bettingCard.getComponent(Card).setTransparents(false);
        this.bettingCard.getComponent(UserHandCard).miniGameReset();
    }

    //跳過小遊戲
    miniGameCancel() {
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        this.diableBtns();
        let time = setTimeout(() =>{
            Game.Instance.playEndGameRun();
            clearTimeout(time);
        }, 500);
        // Game.Instance.showGame();
    }


    CS_BetDouble(betBigger: number, betSmaller: number, betNum: number) {
        var betDouble = proto.PK5Client.BetDouble;
        var betDoubleData =betDouble.create({
            betBigger: betBigger,
            betSmaller: betSmaller,
            betNum: betNum,
        });
        var buffer = betDouble.encode(betDoubleData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_BET_DOUBEL;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
        this.diableBtns();
    }

}

