import { _decorator, Button, Component, Node } from 'cc';
import { Game } from '../Game/Game';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { SidePanel } from '../Common/SidePanel';
import { MiniGameInfo } from '../Utils/GameConfig';
const { ccclass, property } = _decorator;

export enum MINI_BET_TYPE {
    SMALL = 0,
    BIG = 1,
    DOUBLE = 2,
}

@ccclass('MiniGameBet')
export class MiniGameBet extends Component {
    public static Instance: MiniGameBet = null as unknown as MiniGameBet;
    @property(Button)
    bigBtn: Button = null;

    @property(Button)
    smallBtn: Button = null;

    @property(Button)
    doubleBtn: Button = null;

    @property(Button)
    scoreBtn: Button = null;

    miniBetType: number = null;
    betNum: number = null;
    isBetChoose: boolean = null;

    onLoad(): void{
        if(MiniGameBet.Instance === null){
            MiniGameBet.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    reset() {
        this.miniBetType = null;
        this.betNum = null;
        this.isBetChoose = false;
    }

    init() {
        this.reset();
        this.addEventListener();
    }

    addEventListener() {
        this.reset();
        this.bigBtn.node.on(Button.EventType.CLICK, this.betBig, this);
        this.smallBtn.node.on(Button.EventType.CLICK, this.betSmall, this);
        this.doubleBtn.node.on(Button.EventType.CLICK, this.betDouble, this);
        this.scoreBtn.node.on(Button.EventType.CLICK, this.score, this);
    }

    score() {
        Game.Instance.playEndGameRun();
    }

    betBig() {
        this.betNum = Game.Instance.getMiniGameBet() * 2;
        this.miniBetType = MINI_BET_TYPE.BIG;
        MiniGameInfo.MINI_TYPE = MINI_BET_TYPE.BIG;
        let userMoney = Game.Instance.getPlayerMoney() - Game.Instance.getMiniGameBet();
        Game.Instance.setMiniGameFakeMoney(userMoney);
         Game.Instance.setBonusScore(this.betNum);
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        Game.Instance.showMiniGame();
        this.setBetChooseStatus(true);
        SidePanel.Instance.disableLeaveBtn();
    }

    betSmall() {
        this.betNum = Game.Instance.getMiniGameBet() /2;
        this.miniBetType = MINI_BET_TYPE.SMALL;
        MiniGameInfo.MINI_TYPE = MINI_BET_TYPE.SMALL;
        let userMoney = Game.Instance.getPlayerMoney() + this.betNum;
        Game.Instance.setMiniGameFakeMoney(userMoney);
        Game.Instance.setBonusScore(Game.Instance.getMiniGameBet() / 2);
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        Game.Instance.showMiniGame();
        this.setBetChooseStatus(true);
        SidePanel.Instance.disableLeaveBtn();
    }

    betDouble() {
        this.betNum = Game.Instance.getMiniGameBet();
        this.miniBetType = MINI_BET_TYPE.DOUBLE;
        MiniGameInfo.MINI_TYPE = MINI_BET_TYPE.DOUBLE;
        // Game.Instance.setBonusScore(0);
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        Game.Instance.showMiniGame();
        this.setBetChooseStatus(true);
        SidePanel.Instance.disableLeaveBtn();
    }

    setBetChooseStatus(status: boolean) {
        this.isBetChoose = status;
    }

    getBetChooseStatus() {
        return this.isBetChoose;
    }

    getMiniBetType() {
        return this.miniBetType;
    }

    getMiniBetNum() {
        return this.betNum;
    }
    
}

