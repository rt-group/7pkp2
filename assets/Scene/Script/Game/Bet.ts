import { _decorator, Button, Component, instantiate, Label, Node } from 'cc';
import { ResMgr } from '../Manager/ResMgr';
import { Resource } from '../Resource';
import { GameInfo } from '../Utils/GameConfig';
const { ccclass, property } = _decorator;

@ccclass('Bet')
export class Bet extends Component {
    public static Instance: Bet = null as unknown as Bet;

    @property(Button)
    addBtn: Button = null;

    @property(Button)
    minusBtn: Button = null;

    @property(Label)
    addLabel: Label = null;

    @property(Label)
    minusLabel: Label = null;

    @property(Label)
    betLabel: Label = null;

    public miniBetMoney = [];
    public currentBet = 0;
    public maxBet = 0;
    public currentBetMoney = 0 ;

    onLoad(): void{
        if(Bet.Instance === null){
            Bet.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.addEventListener();
    }

    initLabel() {
        this.betLabel.string = GameInfo.currentRoomRange.miniBetMoney[0].toString();
        let range = GameInfo.currentRoomRange.miniBetMoney[1] - GameInfo.currentRoomRange.miniBetMoney[0];
        this.addLabel.string = "+"+range;
        this.minusLabel.string = "-"+range;
    }

    init() {
        this.miniBetMoney = [];
        this.currentBet = 0;
        this.currentBetMoney = 0;
        this.miniBetMoney = GameInfo.currentRoomRange.miniBetMoney;
        this.maxBet = GameInfo.currentRoomRange.miniBetMoney[GameInfo.currentRoomRange.miniBetMoney.length - 1];
        this.initLabel();
    }

    addEventListener() {
        this.addBtn.node.on(Node.EventType.TOUCH_END, this.add, this);
        this.minusBtn.node.on(Node.EventType.TOUCH_END, this.minus, this);
    }

    add() {
        if(GameInfo.currentRoomRange.miniBetMoney[this.currentBet] >= this.maxBet && this.maxBet != 0)
            return;
        this.currentBet++;
        if(this.currentBet >= GameInfo.currentRoomRange.miniBetMoney.length - 1){
            this.currentBet = GameInfo.currentRoomRange.miniBetMoney.length - 1;
        }
        this.betLabel.string = GameInfo.currentRoomRange.miniBetMoney[this.currentBet].toString();
    }

    setCurrentBet() {
        this.maxBet = GameInfo.currentRoomRange.miniBetMoney[this.currentBet];
    }

    getCurrentBet() {
        return GameInfo.currentRoomRange.miniBetMoney[this.currentBet];
    }

    setAllBet() {
        if(this.maxBet == GameInfo.currentRoomRange.miniBetMoney[GameInfo.currentRoomRange.miniBetMoney.length -1]){
            this.currentBet = GameInfo.currentRoomRange.miniBetMoney.length - 1;
        }else{
            this.currentBet = GameInfo.currentRoomRange.miniBetMoney.indexOf(this.maxBet);
        }
        this.betLabel.string = this.maxBet.toString();
    }

    minus() {
        this.currentBet--;
        if(this.currentBet <= 0){
            this.currentBet = 0
        }
        this.betLabel.string = GameInfo.currentRoomRange.miniBetMoney[this.currentBet].toString();
    }

}

