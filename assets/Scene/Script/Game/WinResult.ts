import { _decorator, CCInteger, Component, Enum, Label, Node, Tween, tween, UIOpacity } from 'cc';
import { RESULT_ENUM, WIN_TYPE } from '../Utils/GameConfig';
import { BetPanel } from './BetPanel';
import { Bet } from './Bet';
import { Game } from './Game';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { MiniGame } from '../MiniGame/MiniGame';
const { ccclass, property } = _decorator;

@ccclass('WinResult')
export class WinResult extends Component {
    @property({type:Enum(WIN_TYPE)})
    winType: WIN_TYPE = WIN_TYPE.NOTHING;

    @property(Number)
    resultNum: Number = 0;

    @property(Label)
    resultLabel: Label = null;

    @property(Label)
    resultName: Label = null;

    private resultStack = 0;
    private tweenResult = null;
    private tweenCallBack = null;
    start() {
        this.reset();
    }

    getWinType() {
        return this.winType;
    }

    getString() {
        return this.resultName.string;
    }

    setResultNum(result: number) {
        this.resultLabel.string = result.toString();
    }

    getResultNum() {
        return this.resultLabel.string;
    }

    reset() {
        this.resultLabel.string = this.resultNum.toString();
        // Tween.stopAll();
        this.resultStack = 0;
        this.stopBlink();
    }

    skipScore() {
        // Tween.stopAllByTarget(this.tweenResult);
        // Tween.stopAll();
        this.stopBlink();
        this.resultLabel.string = "0";
        this.resultStack = 0;
        this.tweenCallBack && this.tweenCallBack();
    }

    playBlink() {
        tween(this.resultLabel.getComponent(UIOpacity))
        .delay(0.5)
        .call(()=>{
            this.resultLabel.getComponent(UIOpacity).opacity = 0;
        })
        .delay(0.5)
        .call(()=>{
            this.resultLabel.getComponent(UIOpacity).opacity = 255;
        })
        .union()
        .repeat(300)
        .start();
      

        tween(this.resultName.getComponent(UIOpacity))
        // .hide()
        .delay(0.5)
        .call(()=>{
            this.resultName.getComponent(UIOpacity).opacity = 0;
        })
        .delay(0.5)
        .call(()=>{
            this.resultName.getComponent(UIOpacity).opacity = 255;
        })
        .union()
        .repeat(300)
        .start();
    }

    runNormatAnimationEnd(callBack: Function) {
        this.stopBlink();
            this.tweenCallBack = callBack;
            let result = {result: parseInt(this.resultLabel.string)};
            let calculateTime = 1+ (parseInt(this.resultLabel.string)* 0.0005);
            AudioPlayer.instance.stop(AudioID.LESS4KING);
            AudioPlayer.instance.stop(AudioID.JP);
            this.tweenResult = tween(result)
            .to(calculateTime, {result: 0}, {
                progress: (start, end, current, ratio) => {
                    let num = Math.round(start - (start * ratio));
                    let fake = Math.round(current - num);
                    Game.Instance.setUserFakeMoney(fake);
                    this.resultLabel.string = String(num);
                    // AudioPlayer.instance.stop(AudioID.BET);
                    return num;
                }
            })
            .call(() =>{
                Game.Instance.updateUserMoney();
            })
            .delay(0.5)
            .call(() =>{
                callBack && callBack();
            })
            this.tweenResult.start();
            if(Game.Instance){
                Game.Instance.showSkipBtn();
            }
            if(MiniGame.Instance && parseInt(this.resultLabel.string) > 0){
                MiniGame.Instance.showSkipBtn();
            }
    }

    stopBlink() {
        Tween.stopAllByTarget(this.resultName.getComponent(UIOpacity));
        Tween.stopAllByTarget(this.resultLabel.getComponent(UIOpacity));

        this.resultName.node.active = true;
        this.resultName.getComponent(UIOpacity).opacity = 255;

        this.resultLabel.node.active = true;
        this.resultLabel.getComponent(UIOpacity).opacity = 255;
        
        // tween(this.resultName.node)
        // .show()
        // .call(() =>{
        //     this.resultName.node.active = true;
        //     this.resultName.getComponent(UIOpacity).opacity = 255;
        // })
        // .start()

        // tween(this.resultLabel.node)
        // .show()
        // .call(() =>{
        //     this.resultLabel.node.active = true;
        //     this.resultLabel.getComponent(UIOpacity).opacity = 255;
        // })
        // .start()
    }

    // resetMultipiler() {
    //     tween(this.resultName.node)
    //     .show()
    //     .call(() =>{
    //         this.resultName.node.active = true;
    //         this.resultName.getComponent(UIOpacity).opacity = 255;
    //     })
    //     .start()

    //     tween(this.resultLabel.node)
    //     .show()
    //     .call(() =>{
    //         this.resultLabel.node.active = true;
    //         this.resultLabel.getComponent(UIOpacity).opacity = 255;
    //     })
    //     .start()
    // }

    updateResultMutipiler() {
        this.resultStack = this.resultStack + (this.resultNum as unknown as  number) * Bet.Instance.getCurrentBet();
        this.resultLabel.string = this.resultStack.toString();
    }
}

