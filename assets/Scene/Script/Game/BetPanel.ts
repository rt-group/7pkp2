import { _decorator, BaseNode, Button, Color, Component, instantiate, Label, Node, Sprite, Tween, tween } from 'cc';
import { ResMgr } from '../Manager/ResMgr';
import { Resource } from '../Resource';
import { Bet } from './Bet';
import { GameInfo, GameLabel, POPUP_MESSAGE, WIN_TYPE } from '../Utils/GameConfig';
import { Game } from '../Game/Game';
import proto from "proto";
import { MsgHelp } from '../Utils/MsgHelp';
import { Connect } from '../Connect/Connect';
import { Loader } from '../Loader/Loader';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { Bonus } from './Bonus';
import { UserInfo } from '../Common/UserInfo';
import { PopupManager } from '../Manager/PopupManager';
import { POPUP_BTN_TITLE, POPUP_ICON_TYPE, POPUP_TITLE } from '../Common/Popup';
// import { CardsLoader } from '../../../AssetPackage/Prefab/Load/CardsLoader';
const { ccclass, property } = _decorator;

export enum BET_TYPE {
    NULL = 0,
    NORMAL = 1,
    ALL = 2, 
}

@ccclass('BetPanel')
export class BetPanel extends Component {
    public static Instance: BetPanel = null as unknown as BetPanel;

    @property(Node)
    betContainer: Node = null;

    @property(Node)
    bonusContainer: Node = null;

    @property(Button)
    betBtn: Button = null;

    @property(Button)
    allBetBtn: Button = null;

    @property(Button)
    huluBtn: Button = null;

    @property(Button)
    skipBtn: Button = null;

    @property(Label)
    huluLabel: Label = null;

    public currentBet = 0;
    public currentHulu = 0;
    public tableBonusList = null;
    public betBtnCheck: boolean = false;
    public betType: number = BET_TYPE.NULL;
    public winType: number = null;
    public isSkipGame: boolean = false;

    onLoad(): void{
        if(BetPanel.Instance === null){
            BetPanel.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.addEventListener();
        // this.resetHulu();
    }

    addEventListener() {
        this.betBtn.node.on(Button.EventType.CLICK, this.CS_singleBet, this);
        this.allBetBtn.node.on(Button.EventType.CLICK, this.CS_allBet, this);
        this.huluBtn.node.on(Button.EventType.CLICK, this.huluBonus, this);
    }

    setWinType(winType: number) {
        this.winType = winType;
    }

    getWinType() {
        return this.winType;
    }

    setTableBonus(list) {
        this.tableBonusList = list;
    }

    updateTableBonus() {
        if(this.tableBonusList == null)
        return;
        this.bonusContainer.getChildByName("bonus_0").getComponent(Label).string = this.tableBonusList[1];
        this.bonusContainer.getChildByName("bonus_1").getComponent(Label).string = this.tableBonusList[2];
        this.bonusContainer.getChildByName("bonus_2").getComponent(Label).string = this.tableBonusList[3];
        this.bonusContainer.getChildByName("bonus_3").getComponent(Label).string = this.tableBonusList[4];
        this.bonusContainer.getChildByName("bonus_4").getComponent(Label).string = this.tableBonusList[0];
    }

    runBonusWinByType(type: number, callBack: Function) {
        let result = {result: parseInt( this.bonusContainer.getChildByName("bonus_"+ type).getComponent(Label).string)};
        let calculateTime = 1+ (parseInt( this.bonusContainer.getChildByName("bonus_"+ type).getComponent(Label).string)* 0.0005);
        AudioPlayer.instance.stop(AudioID.MORE4KING);
        tween(result)
        .to(calculateTime, {result: 0}, {
            progress: (start, end, current, ratio) => {
                let num = Math.round(start - (start * ratio));
                this.bonusContainer.getChildByName("bonus_"+ type).getComponent(Label).string = String(num);
                let fake = Math.round(current - num);
                Game.Instance.setUserFakeMoney(fake);
                // AudioPlayer.instance.stop(AudioID.BET);
                return num;
            }
        })
        .delay(1)
        .call(() =>{
            callBack && callBack();
        })
        .start()
    }

    setBetByType() {
        switch(this.betType){
            case BET_TYPE.NORMAL:
                this.singleBet();
                break;
            case BET_TYPE.ALL:
                this.allBet();
                break;
        }
    }

    CS_singleBet() {
        if(UserInfo.getInstance().getWalletBalance < Bet.Instance.getCurrentBet()){
            PopupManager.Instance.showPopup(POPUP_ICON_TYPE.ERROR_TABLE, POPUP_TITLE.NO_MONEY, POPUP_MESSAGE.PLEASE_CHOOSE_ANOTHER_BET, POPUP_BTN_TITLE.TRY_AGAIN)
        }else{
            this.betType = BET_TYPE.NORMAL;
            Bet.Instance.setCurrentBet();
            Game.Instance.setUserFakeMoney(-Bet.Instance.getCurrentBet());
            AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
            this.CS_Bet();
        }
    }

    skipBet() {
        this.betType = BET_TYPE.NORMAL;
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        this.isSkipGame = true;
        this.CS_skipBet();
    }

    CS_allBet() {
        if(UserInfo.getInstance().getWalletBalance < Bet.Instance.getCurrentBet()){
            PopupManager.Instance.showPopup(POPUP_ICON_TYPE.ERROR_TABLE, POPUP_TITLE.NO_MONEY, POPUP_MESSAGE.PLEASE_CHOOSE_ANOTHER_BET, POPUP_BTN_TITLE.TRY_AGAIN)
        }else{
            this.betType = BET_TYPE.ALL;
            Bet.Instance.setAllBet();
            Bet.Instance.setCurrentBet();
            Game.Instance.setUserFakeMoney(-Bet.Instance.getCurrentBet());
            AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
            this.CS_Bet();
        }
    }

    huluBonus() {
        this.currentHulu++;
        if(this.currentHulu > GameLabel.huluStrArr.length - 1){
            this.currentHulu = 0;
        }
        this.huluLabel.string = GameLabel.huluStrArr[this.currentHulu];
    }

    getHulu() {
        // switch(this.huluLabel.string) {
        //     case "A": 
        //         return 0;
        //     case "J":
        //         return 10;
        //     case "Q":
        //         return 11;
        //     case "K":
        //         return 12;
        //     default:
                return this.currentHulu;
        // }
    }

    resetHulu() {
        this.currentHulu = 0;
        this.huluLabel.string = GameLabel.huluStrArr[this.currentHulu];
    }

    reset() {
        this.betType = BET_TYPE.NULL;
        this.enableBtns();
        this.huluBtn.interactable = true;
        this.huluBtn.enabled = true;
        this.huluBtn.node.getComponent(Sprite).grayscale = false;
        this.currentBet = 0;
        this.isSkipGame = false;
        // this.huluLabel.string = GameLabel.huluStrArr[this.currentHulu];
        this.betContainer.removeAllChildren();
        this.bonusContainer.removeAllChildren();
        Tween.stopAll();
        // this.tableBonusList = null;
        this.winType = null;
    }

    updateSkipBoolean(status) {
        this.isSkipGame = status;
    } 

    disableBtns() {
        this.allBetBtn.interactable = false;
        this.betBtn.interactable = false;
        this.allBetBtn.enabled = false;
        this.betBtn.enabled = false;
        this.allBetBtn.node.getComponent(Sprite).grayscale = true;
        this.betBtn.node.getComponent(Sprite).grayscale = true;
    }

    enableBtns() {
        this.allBetBtn.interactable = true;
        this.betBtn.interactable = true;
        this.allBetBtn.enabled = true;
        this.betBtn.enabled = true;
        this.allBetBtn.node.getComponent(Sprite).grayscale = false;
        this.betBtn.node.getComponent(Sprite).grayscale = false;
    }

    singleBet() {
        // if(this.currentBet >= 4){
        //     //送出給server
        //     this.allBetBtn.interactable = false;
        //     this.betBtn.interactable = false;
        //     this.allBetBtn.node.getComponent(Sprite).grayscale = true;
        //     this.betBtn.node.getComponent(Sprite).grayscale = true;
        //     return;
        // }
        this.betContainer.getChildByName("bet_"+this.currentBet).getComponent(Label).string = Bet.Instance.getCurrentBet();
        this.currentBet++;
    }

    skipSingleBet() {
        this.betContainer.getChildByName("bet_"+this.currentBet).getComponent(Label).string = "0";
        this.currentBet++;
    }

    allBet() {
        // if(this.currentBet >= 4){
        //     //送出給server
        //     this.allBetBtn.interactable = false;
        //     this.betBtn.interactable = false;
        //     this.allBetBtn.node.getComponent(Sprite).grayscale = true;
        //     this.betBtn.node.getComponent(Sprite).grayscale = true;
        //     return;
        // }
        this.betContainer.getChildByName("bet_"+this.currentBet).getComponent(Label).string = Bet.Instance.getCurrentBet();
        this.currentBet++;
    }

    getCurrentRound() {
        return this.currentBet;
    }

    CS_Bet() {
        var bet = proto.PK5Client.Bet;
        var betData =bet.create({
            betNum: Bet.Instance.getCurrentBet(),
            f: this.getHulu()
        });
        var buffer = bet.encode(betData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_BET;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
        Loader.Instance.showCardLoader();
        // CardsLoader.Instance.showLoader();
        this.disableBtns();
        this.huluBtn.interactable = false;
        this.huluBtn.enabled = false;
        this.huluBtn.node.getComponent(Sprite).grayscale = true;
    }

    CS_skipBet() {
        var bet = proto.PK5Client.Bet;
        var betData =bet.create({
            betNum: 0,
            f: this.getHulu()
        });
        var buffer = bet.encode(betData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_BET;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
        Loader.Instance.showCardLoader();
        // CardsLoader.Instance.showLoader();
        this.disableBtns();
        this.huluBtn.interactable = false;
        this.huluBtn.enabled = false;
        this.huluBtn.node.getComponent(Sprite).grayscale = true;
    }

    init() {
        this.reset();
        this.resetHulu();
        this.initChips();
        this.initBonusList();
        this.enableBtns();
        Bet.Instance.init();
    }

    //設定bonus win的值
    getBonusWinType(type: number) {
        switch(type){
            case WIN_TYPE.STRAIGHT_FLUSH:
                return 0;
            case WIN_TYPE.FOUR_KIND_WITH_JOKER:
                return 1;
            case WIN_TYPE.STR_FLUSH:
                return 2;
            case WIN_TYPE.FOUR_KIND:
                return 3;
            case WIN_TYPE.FULL_HOUSE:
                return 4;
        }
    }

    initBonusList() {
        if(this.tableBonusList == null){
            this.tableBonusList = GameInfo.currentMachineBonusList ;
        }
        for(let a = 0; a < 4; a ++) {
            this.bonusContainer.getChildByName("bonus_"+a).getComponent(Label).string = this.tableBonusList[a + 1];
        }
        this.bonusContainer.getChildByName("bonus_4").getComponent(Label).string = this.tableBonusList[0];
    }

    initChips() {
        for(let a = 0; a < 4; a++) {
            let chipPrefab = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.Game.Chip)
            let chip = instantiate(chipPrefab);
            chip.name = "bet_"+a;
            this.betContainer.addChild(chip);
        }

        for(let a = 0; a < 5; a++) {
            let chip1Prefab = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.Game.Chip)
            let chip1 = instantiate(chip1Prefab);
            chip1.name = "bonus_"+a;
            this.bonusContainer.addChild(chip1);
            if(a == 4){
                chip1.getComponent(Label).color = Color.RED;
            }
        }
    }
}

