import { _decorator, Button, Component, instantiate, Label, Node, PolygonCollider2D, Sprite } from 'cc';
import proto from "proto";
import { RoomManager } from '../Manager/RoomManager';
import { UserInfo } from '../Common/UserInfo';
import { UserHandCard} from '../Common/UserHandCard';
import { Card, CARD_STATE } from '../Common/Card';
import { WinResult } from './WinResult';
import { ResMgr } from '../Manager/ResMgr';
import { Resource } from '../Resource';
import { WinLoseLayer } from './WinLoseLayer';
import { BetPanel } from './BetPanel';
import { MiniGame } from '../MiniGame/MiniGame';
import { Loader } from '../Loader/Loader';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { FullBet } from './FullBet';
import { Machine } from '../Lobby/Machine';
import { Lobby } from '../Lobby/Lobby';
import { Popup } from '../Common/Popup';
import { GameInfo } from '../Utils/GameConfig';
import { MiniGameBet } from '../MiniGame/MiniGameBet';
import { SidePanel } from '../Common/SidePanel';

const { ccclass, property } = _decorator;

@ccclass('Game')
export class Game extends Component {
    public static Instance: Game = null as unknown as Game;

    @property(Label)
    playerName: Label = null;

    @property(Label)
    playerMoney: Label = null;

    @property([Card])
    cards: Card[] = [];

    @property([WinResult])
    winMultipler: WinResult[] = [];

    @property(Node)
    miniGameTable: Node = null;

    @property(Node)
    gameTable: Node = null;

    @property(Node)
    jpTable: Node = null;

    @property(Node)
    gameLabel: Node = null;

    @property(Label)
    tableNum: Label = null;

    @property(Node)
    skipScore: Node = null;

    @property(Node)
    miniGameBtnList: Node = null;

    @property(Node)
    betUINode: Node = null;
    
    @property(Node)
    userBetNode: Node = null;

    @property(Node)
    betBtnsUINode: Node = null;

    @property(Node)
    miniGameNotice: Node = null;

    DoubleCardList = [];
    isNormalWin: boolean = false;
    isBonusWin: boolean = false;
    isJpWin: boolean = false;
    isFullBetWin: boolean = false;
    normalWinElement = null;
    jpWinElement = null;
    tableBonusList = null;
    skipMiniGame: boolean = false;
    miniGameBet: number = null;
    onLoad(): void{
        if(Game.Instance === null){
            Game.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.skipScore.getComponent(Button).node.on(Button.EventType.CLICK, this.skipScoreMode, this);
        this.init();
    }

    init() {
        this.hideSkipBtn();
        this.showGame();
        this.setRoomNum();
        this.initStage();
        MiniGameBet.Instance.init();
        SidePanel.Instance.enableLeaveBtn();
    }

    initStage() {
        this.betBtnsUINode.active = true;
        this.userBetNode.active = true;
        this.betUINode.active = true;
        this.miniGameBtnList.active = false;
        this.miniGameNotice.active = false;
    }

    miniStage() {
        this.betBtnsUINode.active = false;
        this.userBetNode.active = false;
        this.betUINode.active = false;
        this.miniGameBtnList.active = true;
        this.miniGameNotice.active = true;
    }

    showSkipBtn() {
        // this.skipScore.active = true;
        this.skipScore.getComponent(Button).interactable = true;
        this.skipScore.getComponent(Button).enabled = true;
        this.skipScore.getComponent(Sprite).grayscale = false;
    }

    hideSkipBtn() {
        // this.skipScore.active = false;
        this.skipScore.getComponent(Button).interactable = false;
        this.skipScore.getComponent(Button).enabled = false;
        this.skipScore.getComponent(Sprite).grayscale = true;
    }

    skipScoreMode() {
        if(this.normalWinElement != null && BetPanel.Instance.getCurrentRound() > 3){
            this.normalWinElement.skipScore();
        }else{
            BetPanel.Instance.skipBet();
        }
            
        this.hideSkipBtn();
    }

    skipMini() {
        this.skipMiniGame = true;
    }

    showGame() {
        this.miniGameTable.active = false;
        this.gameLabel.active = true;
        this.gameTable.active = true;
    }

    setRoomNum() {
        this.tableNum.string = "# "+GameInfo.currentMachineId.toString();
    }

    showGameTable() {
        this.gameTable.active = true;
        this.gameLabel.active = false;
    }

    closeGameTable() {
        this.gameTable.active = false;
        this.gameLabel.active = false;
    }

    showMiniGame() {
        this.miniGameTable.active = true;
        MiniGame.Instance.reset();
        this.cards.forEach((card) =>{ 
            card.getComponent(Card).setCardState(CARD_STATE.FLIPPED);
            card.getComponent(Card).reset();
            card.getComponent(Card).setTransparents(false);
            card.getComponent(UserHandCard).reset();
        });
       this.closeGameTable();
        AudioPlayer.instance.stop(AudioID.LESS4KING);
        AudioPlayer.instance.stop(AudioID.MORE4KING);
        AudioPlayer.instance.stop(AudioID.JP);
        MiniGame.Instance.init();
    }

    setMiniGameBet(bet: any) {
        this.miniGameBet = bet;
    }

    getMiniGameBet() {
        return this.miniGameBet;
    }

    reset() {
        this.tableNum.string = "# "+GameInfo.currentMachineId.toString();
        //小遊戲重置, 主遊戲畫面開啟
        if(MiniGame.Instance != null){
            MiniGame.Instance.reset();
        }
        SidePanel.Instance.enableLeaveBtn();
        this.initStage();
        MiniGameBet.Instance.reset();
        this.setMiniGameBet(null);
        this.hideSkipBtn();
        Game.Instance.showGame();
        this.skipMiniGame = false;
        this.updateBonusMultipler();
        this.winMultipler.forEach((multipler) =>{ multipler.getComponent(WinResult).reset()})
        this.cards.forEach((card, index) =>{ 
            // if(index != 6){
            //     card.getComponent(Card).setCardState(CARD_STATE.FLIPPED);
            // }
            card.getComponent(Card).reset();
            card.getComponent(Card).setTransparents(false);
            card.getComponent(UserHandCard).reset();
        });
        Loader.Instance.stopCardLoader();
        BetPanel.Instance.reset();
        BetPanel.Instance.init();
        UserInfo.getInstance().reset();
        FullBet.Instance.reset();

        if(this.normalWinElement != null || this.normalWinElement != undefined){
            this.normalWinElement.reset();
        }
        this.isBonusWin = false;
        this.isNormalWin = false;
        this.isJpWin = false;
        this.normalWinElement = null;
        this.jpWinElement = null;
        this.jpTable.active = false;
        this.DoubleCardList = [];
    }


    playEndGameRun() {
        if(this.isJpWin){
            this.jpAnimationCallBack();
            // this.jpWinElement.runNormatAnimationEnd(this.jpAnimationCallBack.bind(this));
        }else if(this.isFullBetWin){
            FullBet.Instance.runFullBet(this.fullBetAnimationCallBack.bind(this));
        }else if(this.isBonusWin){
            let winType = BetPanel.Instance.getWinType();
            BetPanel.Instance.runBonusWinByType(BetPanel.Instance.getBonusWinType(winType), this.bonusAnimationCallBack.bind(this));
        }else if(this.isNormalWin){
            this.normalWinElement.runNormatAnimationEnd(this.normalAnimationCallBack.bind(this));
        }else{            
            this.reset();
            this.updateUserMoney();
        }
    }

    playBonusAnimation() {
        let winType = BetPanel.Instance.getWinType();
        BetPanel.Instance.runBonusWinByType(BetPanel.Instance.getBonusWinType(winType), this.bonusAnimationCallBack.bind(this));
    }

    fullBetAnimationCallBack() {
        this.isFullBetWin = false;
        this.playEndGameRun();
    }

    jpAnimationCallBack() {
        this.isJpWin = false;
        this.playEndGameRun();
    }

    bonusAnimationCallBack() {
        this.isBonusWin = false;
        if(this.skipMiniGame){
            // this.normalWinElement.runNormatAnimationEnd(this.normalAnimationCallBack.bind(this));
            this.playEndGameRun();
        }else{
            this.showPlayerWin();
        }
    
        // this.normalWinElement.runNormatAnimationEnd(this.normalAnimationCallBack.bind(this));
        // AudioPlayer.instance.stop(AudioID.MORE4KING);
    }

    normalAnimationCallBack(){
        this.isNormalWin = false;
        this.updateUserMoney();
        // this.reset();
        let time = setTimeout(() =>{
            this.reset();
            clearTimeout(time);
        }, 1000);
    }


    setFirstDoubleCardData(doulbeCards: any) {
        this.DoubleCardList = doulbeCards;
    }

    getFirstDoubleCardData() {
        return this.DoubleCardList;
    }

    handleGameLose() {
        Loader.Instance.showCardLoader();
        AudioPlayer.instance.stop(AudioID.NOWIN);
        this.reset();
    }

    handleGameWin() {
        // this.reset();
        // this.showMiniGame();
    }

    setPrediction(prediction) {
        if(prediction.length <= 0)
            return;
        if(BetPanel.Instance.getCurrentRound() == 3){
            this.cards[1].getComponent(UserHandCard).updateUserPredictData(prediction[1].card);
            this.cards[3].getComponent(UserHandCard).updateUserPredictData(prediction[3].card);
            this.cards[6].getComponent(UserHandCard).updateUserPredictData(prediction[6].card);
        }
        // else if(BetPanel.Instance.getCurrentRound() == 4){
        //     this.cards[1].getComponent(Card).removePrediction();
        //     this.cards[3].getComponent(Card).removePrediction();
        //     this.cards[6].getComponent(Card).removePrediction();
        // }
    }

    removePrediction() {
        this.cards[1].getComponent(Card).removePrediction();
        this.cards[3].getComponent(Card).removePrediction();
        this.cards[6].getComponent(Card).removePrediction();
    }

    updateMiniGameResult(num: number) {
        // let calculateGold = parseInt(this.normalWinElement.getResultNum());
        // this.normalWinElement.setResultNum(calculateGold + num);
        let cal = parseInt(this.normalWinElement.getResultNum()) + num;
        this.normalWinElement.setResultNum(cal);
        this.setMiniGameBet(cal);
    }
    
    playMiniGameEnd() {
        this.normalWinElement.stopBlink();
        // this.normalWinElement.setResultNum();
    }

    setBonusScore(num: number) {
        this.normalWinElement.setResultNum(num);
    }


    updateGameStatus(message) {
        this.setPocker(message.pockers);
        this.setPrediction(message.predict);
        //普通贏 winStr[0].getResultNum()
        this.tableBonusList =  message.jpList;
        if(message.result && message.winResult.winGold > 0){
            let time = setTimeout(() =>{
                SidePanel.Instance.enableLeaveBtn();
                if(message.winResult.betFull){
                    this.isFullBetWin = true;
                    FullBet.Instance.showFullBetPrize();
                    FullBet.Instance.setFullBetPrize(message.winResult.betFull);
                }
                if(message.winResult.winType == 11){
                    BetPanel.Instance.setWinType(7);
                }else if(message.winResult.winType == 12){
                    BetPanel.Instance.setWinType(8);
                }else if(message.winResult.winType == 13){
                    BetPanel.Instance.setWinType(9);
                }else if(message.winResult.winType == 14){
                    BetPanel.Instance.setWinType(10);
                }else{
                    BetPanel.Instance.setWinType(message.winResult.winType);
                }
                this.setMiniGameBet(message.winResult.bonusRight);
                this.endGameTransparents(message.pockers, message.winResult.winPoker);
                if(message.winResult.bonusLeft > 0){
                    this.isBonusWin = true;
                }
                if(message.winResult.fullhouseWin){
                    this.isFullBetWin = true;
                }
               
                this.isNormalWin = true;
                
                
                // message.winResult.winType < 7 ? this.isNormalWin = true : this.isNormalWin = false;
                message.winResult.winType >= 7 ? AudioPlayer.instance.play(AudioID.MORE4KING) : AudioPlayer.instance.play(AudioID.LESS4KING);
                

                if(message.winResult.winJP2 != -1){
                    AudioPlayer.instance.play(AudioID.JP);
                    this.isJpWin = true;
                    this.jpTable.active = true;
                    let jpStr = this.winMultipler.filter((element) => {
                        if(message.winResult.winJP2 == 11){
                            return element.getWinType() == 7;
                        }else if(message.winResult.winJP2 == 12){
                            return element.getWinType() == 8;
                        }else if(message.winResult.winJP2 == 13){
                            return element.getWinType() == 9;
                        }else if(message.winResult.winJP2 == 14){
                            return element.getWinType() == 10;
                        }else{
                            return message.winResult.winJP2 == element.getWinType();
                        }
                    })

                    jpStr[0].playBlink();
                    jpStr[0].setResultNum(message.winResult.jpSet[0]);
                    this.jpWinElement = jpStr[0];
                }
               
                let winStr = this.winMultipler.filter((element)=>{
                    if(message.winResult.winType == 11){
                        return element.getWinType() == 7;
                    }else if(message.winResult.winType == 12){
                        return element.getWinType() == 8;
                    }else if(message.winResult.winType == 13){
                        return element.getWinType() == 9;
                    }else if(message.winResult.winType == 14){
                        return element.getWinType() == 10;
                    }else{
                        return message.winResult.winType == element.getWinType();
                    }
                })

                winStr[0].playBlink();
                winStr[0].setResultNum(message.winResult.bonusRight);
                this.normalWinElement = winStr[0];
                UserInfo.getInstance().setCurrentGameWinGold(message.winResult.winGold);
                UserInfo.getInstance().setCurrentWinNode(winStr[0]);
                UserInfo.getInstance().setWalletBalance = message.nowGold;
                this.setFirstDoubleCardData(message.DoubleBetList);
                if(!this.isBonusWin && !this.isFullBetWin) {
                    this.showPlayerWin();
                }else if(this.isBonusWin || this.isFullBetWin){
                    this.playEndGameRun();
                }
                clearTimeout(time);

            }, 2000)
        //輸
        }else if(message.result && message.winResult.winGold <= 0){
            AudioPlayer.instance.stopBGM();
            AudioPlayer.instance.play(AudioID.NOWIN);
            let time = setTimeout(() =>{
                this.playEndGameRun();
                // WinLoseLayer.Instance.showPlayerLose(this.handleGameLose.bind(this));
                UserInfo.getInstance().setWalletBalance = message.nowGold;
                this.updateUserMoney();
                clearTimeout(time);
            }, 2000);
        //下注階段
        }else if(!message.result && message.winResult.winGold <= 0){
            UserInfo.getInstance().setWalletBalance = message.nowGold;
            this.updateUserMoney();
        }
    }


    showPlayerWin() {
        // WinLoseLayer.Instance.showPlayerWin(this.handleGameWin.bind(this), ()=>{
        // }, this.normalWinElement.getString());
        this.miniStage();
    }
    

    updateBonusMultipler() {
        this.winMultipler.forEach(element => {
            element.getComponent(WinResult).updateResultMutipiler();
        })
    }

    setPocker(pokers) {
        for(let a = 0; a < pokers.length; a++){
            if(pokers[a] == -1){
                this.cards[a].getComponent(UserHandCard).show()
            }else{
                // console.log(pokers[a]);
                this.cards[a].getComponent(UserHandCard).updateUserHandData(pokers[a]);
            }
        }
    }

    endGameTransparents(pockers, winPockers) {
        let unWinPocker = this.findUnWinPockers(pockers, winPockers);
        for(let a = 0; a < unWinPocker.length; a++) {
            let pos = pockers.indexOf(unWinPocker[a]);
            this.cards[pos].getComponent(UserHandCard).changeCardTransparent(true);
            
        }
    }

    findUnWinPockers(pockers, winPockers) {
        return pockers.concat(winPockers).filter((t, i , arr) => {
            return arr.indexOf(t) === arr.lastIndexOf(t);
        })
        
    }

    resetPoker() {
        for(let i=0;i< this.cards.length; i++){
            this.cards[i].getComponent(UserHandCard).reset();
        }
    }

    updateUserMoney() {
        this.playerMoney.string = UserInfo.getInstance().getWalletBalance + "";
    }

    updateUserData() {
        this.playerName.string = UserInfo.getInstance().getUserName;
        this.playerMoney.string = UserInfo.getInstance().getWalletBalance + "";
    }

    setUserFakeMoney(addNum: number) {
        // console.log(addNum);
        this.playerMoney.string = parseInt(this.playerMoney.string) + addNum + "";
    }

    setMiniGameFakeMoney(fakeMoney: number) {
        this.playerMoney.string = fakeMoney.toString();
    }

    getPlayerMoney() {
        return parseInt(this.playerMoney.string);
    }

}

