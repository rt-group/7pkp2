import { _decorator, Component, instantiate, Node } from 'cc';
import { ResMgr } from '../Manager/ResMgr';
import { Resource } from '../Resource';
const { ccclass, property } = _decorator;

@ccclass('Bonus')
export class Bonus extends Component {
    public static Instance: Bonus = null as unknown as Bonus;

    @property(Node)
    bonusContainer: Node = null;
    
    onLoad(): void{
        if(Bonus.Instance === null){
            Bonus.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    init() {
        this.initBonusChips();
    }

    initBonusChips() {
        for(let a = 0; a < 4; a++) {
            let chipPrefab = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.Game.Chip)
            let chip = instantiate(chipPrefab);
            this.bonusContainer.addChild(chip);
        }
    }
}

