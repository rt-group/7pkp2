import { _decorator, Button, Component, Label, Node } from 'cc';
import { Game } from './Game';
const { ccclass, property } = _decorator;

@ccclass('WinLoseLayer')
export class WinLoseLayer extends Component {
    public static Instance: WinLoseLayer = null as unknown as WinLoseLayer;

    @property(Node)
    win: Node = null;

    @property(Label)
    winHandType: Label = null;

    @property(Node)
    lose: Node = null;

    @property(Node)
    bigWin: Node = null;

    @property(Node)
    smallWin: Node = null;

    @property(Node)
    doubleWin: Node = null;

    @property(Button)
    skipMiniBtn: Button = null;

    @property(Button)
    startMiniBtn: Button = null;

    @property(Label)
    jpNum: Label = null;

    callBackDD: Function = null;
    callBack: Function = null;
    delay: number = 2;

    onLoad(): void{
        if(WinLoseLayer.Instance === null){
            WinLoseLayer.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.node.active = false;
        this.addEventListener();
    }

    addEventListener() {
        this.skipMiniBtn.node.on(Node.EventType.TOUCH_END, ()=>{
            this.node.active = false;
            Game.Instance.playEndGameRun();
            Game.Instance.skipMini();
            // Game.Instance.reset();
            // Game.Instance.updateUserMoney();
        })

        this.startMiniBtn.node.on(Node.EventType.TOUCH_END, ()=> {
            this.node.active = false;
            Game.Instance.showMiniGame();
        })
    }

    reset() {
        this.node.active = false;
    }

    showPlayerWin(callBack: Function , callBack2: Function, result: string) {
        this.showNode(true, false, false, false, false);
        this.winHandType.string = result;
        this.callBackDD = callBack2;
    }

    showPlayerLose(callBack: Function) {
        this.showNode(false, true, false, false, false);
        this.callBackOnDelayTimeOver(callBack, 3);
    }

    showPlayeBigWin(callBack: Function) {
        this.showNode(false, false, true, false, false);
        this.callBackOnDelayTimeOver(callBack, 2);
    }

    showPlayeDoubleWin(callBack: Function) {
        this.showNode(false, false, false, false, true);
        this.callBackOnDelayTimeOver(callBack, 2);
    }

    showPlayeSmallWin(callBack: Function) {
        this.showNode(false, false, false, true, false);
        this.callBackOnDelayTimeOver(callBack, 2);
    }

    callBackOnDelayTimeOver(callBack: Function, delay: number) {
        this.callBack = callBack;
        this.scheduleOnce(() => {
          this.callBack && this.callBack();
          this.node.active = false;
        }, delay);
    }

    runJPAnimation(callBack: Function) {

    }

    showNode(win: boolean, lose: boolean, bigWin: boolean, smallWin: boolean, doubleWin: boolean) {
        this.node.active = true;
        this.win.active = win;
        this.lose.active = lose;
        this.bigWin.active = bigWin;
        this.smallWin.active = smallWin;
        this.doubleWin.active = doubleWin;
    }


    




}

