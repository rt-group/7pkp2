import { _decorator, Component, Label, Node, tween } from 'cc';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { Game } from './Game';
const { ccclass, property } = _decorator;

@ccclass('FullBet')
export class FullBet extends Component {
    public static Instance: FullBet = null as unknown as FullBet;

    onLoad(): void{
        if(FullBet.Instance === null){
            FullBet.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    showFullBetPrize() {
        this.node.active = true;
    }

    reset() {
        this.node.getComponent(Label).string = "0";
        this.node.active = false;
    }

    setFullBetPrize(prize: number) {
        this.showFullBetPrize();
        this.node.getComponent(Label).string = prize.toString();
    }

    runFullBet(callBack: Function) {
        let result = {result: parseInt(this.node.getComponent(Label).string)};
        let calculateTime = 1+ (parseInt(this.node.getComponent(Label).string) * 0.0005);
        AudioPlayer.instance.stop(AudioID.LESS4KING);
        AudioPlayer.instance.stop(AudioID.JP);
        tween(result)
        .to(calculateTime, {result: 0}, {
            progress: (start, end, current, ratio) => {
                let num = Math.round(start - (start * ratio));
                let fake = Math.round(current - num);
                Game.Instance.setUserFakeMoney(fake);
                this.node.getComponent(Label).string = String(num);
                // AudioPlayer.instance.stop(AudioID.BET);
                return num;
            }
        })
        .delay(0.5)
        .call(() =>{
            callBack && callBack();
        })
        .start()
    }

    start() {
        this.reset();
    }

    update(deltaTime: number) {
        
    }
}

