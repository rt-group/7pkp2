import { _decorator, Button, Component, Node } from 'cc';
import { BUILD } from 'cc/env';
const { ccclass, property } = _decorator;

@ccclass('Rules')
export class Rules extends Component {
    public static Instance: Rules = null as unknown as Rules;
    @property(Button)
    closeBtn: Button = null;

    onLoad(): void{
        if(Rules.Instance === null){
            Rules.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
    }

    initRuleTable() {
        this.closeBtn.node.on(Node.EventType.TOUCH_END, this.hideRuleTable, this);
        this.hideRuleTable();
    }

    showRuleTable(){
        this.node.active = true;
    }

    hideRuleTable() {
        this.node.active = false;
    }
}

