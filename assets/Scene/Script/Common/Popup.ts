import { _decorator, Button, Component, Label, Node, Sprite, SpriteFrame } from 'cc';
import { GAME_SCENE, POPUP_MESSAGE } from '../Utils/GameConfig';
import { Connect } from '../Connect/Connect';
import { GameApp } from '../GameApp';
import { BetPanel } from '../Game/BetPanel';
import { Game } from '../Game/Game';
import { WinLoseLayer } from '../Game/WinLoseLayer';
import { Loader } from '../Loader/Loader';
const { ccclass, property } = _decorator;
export enum POPUP_TITLE {
    SOMETHING_WENT_WRONG = "Something Went Wrong",
    CONTACT_GAME_PROVIDER = "Please contact your game provider",
    SERVER_NOT_FOUND = "Oops! Server not found",
    NO_INTERNET_CONNECTION = "Oops! No Internet Connection",
    TABLE_ALREADY_IN_USE = "Oops! Table is already in Use",
    NO_HISTORY = "No History available",
    NO_MONEY = "BankKrupt",
}

export enum POPUP_BTN_TITLE {
    OK= "OK",
    TRY_AGAIN = "TRY AGAIN",
    OKAY = "OKAY",
    RECONNECT = "RECONNECT",
}

export enum POPUP_BUTTON_TYPE {
    OK_BUTTON = 0,
    RETRY_BUTTON = 1,
    OKAY_BUTTON = 2,
}

export enum POPUP_ICON_TYPE {
    ERROR_CLOUD = 0,
    ERROR_COMMON = 1,
    ERROR_INTERNET = 2,
    ERROR_TABLE = 3,
}


@ccclass('Popup')
export class Popup extends Component {
    @property(Node)
    popupContainer: Node = null;
  
    @property(Label)
    title: Label = null;

    @property(Label)
    message: Label = null;

    @property(Sprite)
    icon: Sprite = null;
  
    @property(SpriteFrame)
    iconsFrame: SpriteFrame[] = [];

    @property(Button)
    tryBtn: Button = null;

    callBack: any;

    tableId = null;

    onLoad() {
        this.addEventListener();
    }

    addEventListener() {
        this.tryBtn.node.on(Node.EventType.TOUCH_END, this.btnCB, this);
    }

    showPopup() {
        this.node.active = true;
    }

    hidePopup() {
        this.node.active = false;
    }

    updatePopupTitle(iconIndex: POPUP_ICON_TYPE, title: POPUP_TITLE, message: POPUP_MESSAGE, btnTitle: POPUP_BTN_TITLE, cb?) {
        this.icon.spriteFrame = this.iconsFrame[iconIndex];
        this.title.string = title;
        this.callBack = cb;
        this.message.string = message.toUpperCase();
        this.tryBtn.node.getChildByName("title").getComponent(Label).string = btnTitle;
      }

    btnCB() {
        this.hidePopup();
        Loader.Instance.stopCardLoader();
        Loader.Instance.stopRowLoader();
        if(this.message.string == POPUP_MESSAGE.NO_INTERNET_CONNECTION){
            console.log("internet failed");
            if(GameApp.Instance.getCurrentScene() == GAME_SCENE.GAME_SCENE){
                Game.Instance.reset();
                WinLoseLayer.Instance.reset();
            }
            GameApp.Instance.setScene(GAME_SCENE.LOGIN_SCENE);
            Connect.Instance.connect();
        }
        // this.callBack && this.callBack();
        // this.node.removeFromParent();
        // let text = this.tryBtn.node.getChildByName("title").getComponent(Label).string;
        // if (text.toUpperCase() == POPUP_BTN_TITLE.OK) this.node.removeFromParent();
      }
}

