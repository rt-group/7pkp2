import { _decorator } from "cc";
const { ccclass } = _decorator;

@ccclass("UserInfo")
export class UserInfo {
  private static userInfo: UserInfo = null;
  private userName: string = "";
  private walletBalance: number = 0;
  private uid: string = "";
  private userPlayingTable: number = 0;
  private currentWinGold: number = 0;
  private currentWinNode: any = null;

  public static getInstance(): UserInfo {
    if (!this.userInfo) {
      this.userInfo = new UserInfo();
    }
    return this.userInfo;
  }

  initiateUserInfo(data) {
    this.setUserName = data.name;
    this.setWalletBalance = data.money;
    this.uid = data.uid;
  }

  reset() {
    this.currentWinGold = 0;
    this.currentWinNode = null;
  }

  setCurrentWinNode(winNode: any) {
    this.currentWinNode = winNode;
  }

  getCurrentWinNode() {
    return this.currentWinNode;
  }

  setCurrentGameWinGold(winGold: number) {
    this.currentWinGold = winGold;
  }

  getCurrentGameWinGold() {
    return this.currentWinGold;
  }

  setUserPlayingTable(tableNum: number) {
    this.userPlayingTable = tableNum ? tableNum : 0;
  }

  set UserPlayingTable(tableNo: number) {
    this.userPlayingTable = tableNo;
  }
  get UserPlayingTable(): number {
    return this.userPlayingTable;
  }

  set setUid(uid: string) {
    this.uid = uid;
  }

  get getUid(): string {
    return this.uid;
  }

  set setUserName(name: string) {
    this.userName = name;
  }
  get getUserName(): string {
    return this.userName;
  }

  set setWalletBalance(balance: number) {
    this.walletBalance = balance;
  }
  get getWalletBalance(): number {
    return this.walletBalance;
  }

  set updateWalletBalance(by: number) {
    this.walletBalance = Number(this.walletBalance) + Number(by);
  }

}
