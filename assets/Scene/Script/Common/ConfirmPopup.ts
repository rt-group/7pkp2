import { _decorator, Button, Component, Label, Node, tween, Vec3 } from 'cc';
import { POPUP_MESSAGE } from '../Utils/GameConfig';
const { ccclass, property } = _decorator;

@ccclass('ConfirmPopup')
export class ConfirmPopup extends Component {
  @property(Label)
  message: Label = null;

  @property(Button)
  cancelBtn: Button = null;

  @property(Button)
  confirmBtn: Button = null;

  cancelCB = null;
  confirmCB = null;


  onLoad() {
    this.addEventListener();
  }

  addEventListener() {
    this.cancelBtn.node.on(Node.EventType.TOUCH_END, this.cancelBtnCB, this);
    this.confirmBtn.node.on(Node.EventType.TOUCH_END, this.confirmBtnCB, this);
  }

  showConfirmPopup() {
    this.node.active = true;
  }

  hideConfirmPopup() {
    this.node.active = false;
  }

  cancelBtnCB() {
    this.playPopUpCloseAnimation(this.node);
  }

  updateMessage(message: string) {
    this.message.string = message;
    this.playPopUpOpenAnimation(this.node);
  }
  playPopUpOpenAnimation(node: Node) {
    node.active = true;
    tween(node)
      .to(0, { scale: new Vec3(0.4, 0.6, 0) })
      .to(0.099, { scale: new Vec3(1.1, 1.15, 1) })
      .to(0.0462, { scale: new Vec3(1.15, 1, 1) })
      .to(0.0462, { scale: new Vec3(1.15, 1.06, 1) })
      .to(0.066, { scale: new Vec3(1, 1, 1) })
      .start();
  }

  playPopUpCloseAnimation(node: Node) {
    tween(node)
      .to(0.0462, { scale: new Vec3(1.15, 1.06, 1) })
      .to(0.0462, { scale: new Vec3(1.15, 1, 1) })
      .to(0.099, { scale: new Vec3(1.1, 1.15, 1) })
      .to(0, { scale: new Vec3(0.4, 0.6, 0) })
      .to(0.066, { scale: new Vec3(0, 0, 0) })
      .call(() => {
        node.active = false;
      })
      .start();
  }

  confirmBtnCB() {
    this.playPopUpCloseAnimation(this.node);
  }
}

