import { _decorator, Button, Component, Node, Toggle } from 'cc';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
const { ccclass, property } = _decorator;

@ccclass('Setting')
export class Setting extends Component {
    public static Instance: Setting = null as unknown as Setting;

    @property(Button)
    closeBtn: Button = null;

    @property(Button)
    musicBtn: Button = null;

    @property(Button)
    soundBtn: Button = null;

    private musicChecked: boolean = false;
    private soundChecked: boolean = false;

    onLoad(): void{
        if(Setting.Instance === null){
            Setting.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.musicChecked = false;
        this.soundChecked = false;
        this.updateAudioStatus();
    }

    updateAudioStatus() {
        this.musicBtn.getComponent(Toggle).isChecked = this.musicChecked;
        this.soundBtn.getComponent(Toggle).isChecked = this.soundChecked;

        if( this.musicBtn.getComponent(Toggle).isChecked){
            AudioPlayer.instance.enableBGM = false;
            AudioPlayer.instance.stopBGM();
        }else{
            AudioPlayer.instance.enableBGM = true;
        }

        if(this.soundBtn.getComponent(Toggle).isChecked) {
            AudioPlayer.instance.enableSE = false;
        }else{
            AudioPlayer.instance.enableSE = true;
        }
    }

    initSetting() {
        this.addEventListener();
        this.hideSettingTable();
    }

    addEventListener() {
        this.closeBtn.node.on(Node.EventType.TOUCH_END, this.hideSettingTable, this);
        this.musicBtn.node.on(Node.EventType.TOUCH_END, this.musicClick, this);
        this.soundBtn.node.on(Node.EventType.TOUCH_END, this.soundClick, this);
    }

    musicClick(e) {
        console.log(e);
        this.musicChecked = !this.musicChecked;
        this.updateAudioStatus();
    }

    soundClick(e) {
        console.log(e);
        this.soundChecked = !this.soundChecked;
        this.updateAudioStatus();
    }

    showSettingTable() {
        this.node.active = true;
    }

    hideSettingTable() {
        this.node.active = false;
    }


    
}

