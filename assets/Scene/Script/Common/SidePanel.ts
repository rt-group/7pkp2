import { _decorator, Button, Component, Node, Sprite } from 'cc';
import { RoomManager } from '../Manager/RoomManager';
import { Setting } from './Setting';
import { Rules } from './Rules';
import { BetPanel } from '../Game/BetPanel';
import proto from "proto";
import { MsgHelp } from '../Utils/MsgHelp';
import { Connect } from '../Connect/Connect';
import { Loader } from '../Loader/Loader';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
// import { RowLoader } from '../../../AssetPackage/Prefab/Load/RowLoader';
const { ccclass, property } = _decorator;

@ccclass('SidePanel')
export class SidePanel extends Component {
    public static Instance: SidePanel = null as unknown as SidePanel;

    @property(Button)
    leaveBtn: Button = null;

    @property(Button)
    ruleBtn: Button = null;

    @property(Button)
    settingBtn: Button = null;


    onLoad(): void{
        if(SidePanel.Instance === null){
            SidePanel.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        this.addEventListener();
    }

    addEventListener() {
        this.leaveBtn.node.on(Button.EventType.CLICK, this.CS_LeaveMachine, this);
        this.ruleBtn.node.on(Button.EventType.CLICK, this.showRuleTable, this);
        this.settingBtn.node.on(Button.EventType.CLICK, this.showSettingTable, this);
    }

    disableLeaveBtn() {
        this.leaveBtn.enabled = false;
        this.leaveBtn.interactable = false;
        this.leaveBtn.getComponent(Sprite).grayscale = true;
    }

    enableLeaveBtn() {
        this.leaveBtn.enabled = true;
        this.leaveBtn.interactable = true;
        this.leaveBtn.getComponent(Sprite).grayscale = false;
    }

    showSettingTable() {
        Setting.Instance.showSettingTable();
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
    }

    showRuleTable() {
        Rules.Instance.showRuleTable();
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
    }

    CS_LeaveMachine() {
        AudioPlayer.instance.play(AudioID.BUTTON_CLICK);
        var leaveGame = proto.PK5Client.LeaveMachine;
        var leaveGameData =leaveGame.create({
        });
        var buffer = leaveGame.encode(leaveGameData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_LEAVE_MACHINE;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
        // RowLoader.Instance.showLoader();
        Loader.Instance.showRowLoader();
    }

    backLobby() {
        //移動到大廳
        BetPanel.Instance.reset();
        RoomManager.Instance.CS_SetBet();
        
    }
}

