import { Component, _decorator } from "cc";
import { Card } from "./Card";
import { CardType } from "../Utils/GameConfig";
import { Resource } from "../Resource";
const { ccclass } = _decorator;

@ccclass("UserHandCard")
export class UserHandCard extends Component{
//   public static Instance: UserHandCard = null as unknown as UserHandCard;
  private symbolId: number = null;
  private suitType: string = "";
  private cardValue: number = -1;
  private isWinnerCard: boolean = false;
  public predictSuitPath: string[] = [];
  public predictValue: number[] = [];

//   onLoad(): void{
//     if(UserHandCard.Instance === null){
//         UserHandCard.Instance = this;
//     }else{
//         this.destroy();
//         return;
//     }
// }

  reset() {
    this.symbolId = null;
    this.suitType = "";
    this.cardValue = -1;
    this.isWinnerCard = false;
    this.predictSuitPath = [];
    this.predictValue = [];
    let cardNum =  this.node.getComponent(Card).getCardNum();
    if(cardNum == 6){
      this.node.getComponent(Card).removeSpriteFrame();
    }else{
      this.node.active=false;
    }
  }

  miniGameReset() {
    this.symbolId = null;
    this.suitType = "";
    this.cardValue = -1;
    this.isWinnerCard = false;
    this.predictSuitPath = [];
    this.predictValue = [];
  }

  show(){
    this.node.active=true;
  }

  updateUserHandData(userHandData) {
    this.setSymbolId = userHandData;
    this.setSuitType = this.calculateSuitType(userHandData);
    this.setCardValue = userHandData.symbolId != -1 ? this.calculateCardValue(userHandData): -1;
    // this.setWinnerCardStatus = userHandData.isWinnerCard;
    this.node.active=true;
    this.node.getComponent(Card).updateCardData(this, false);
  }

  updateUserPredictData(predictData) {
    if(predictData.length <= 0)
      return;
    for(let a = 0; a < predictData.length; a++){
       let suit = this.calculatePredictSuitType(predictData[a]);
       this.predictSuitPath[a] = suit;
       let predictValue = this.calculateCardValue(predictData[a]);
       this.predictValue[a] = predictValue;
    }
    this.node.getComponent(Card).updatePredictData(this);
  }

  changeCardTransparent(end: boolean) {
    this.node.getComponent(Card).setTransparents(end);
  }

  calculatePredictSuitType(symbolId: number) {
    switch(Math.floor(symbolId/13)){
        case CardType.CardState.Spade:
            return Resource.CardIcon.SPADE;
        case CardType.CardState.Hearts:
            return Resource.CardIcon.HEART;
        case CardType.CardState.Diamonds:
            return Resource.CardIcon.DIAMOND;
        case CardType.CardState.Club:
            return Resource.CardIcon.CLUB;
    }
  }

  calculateSuitType(symbolId: number) {
    if(symbolId == 52) return "b";
    if(symbolId == 53) return "r";
    switch(Math.floor(symbolId/13)){
        case CardType.CardState.Spade:
            return "s";
        case CardType.CardState.Hearts:
            return "h";
        case CardType.CardState.Diamonds:
            return "d";
        case CardType.CardState.Club:
            return "c";
    }
  }

  calculateCardValue(symbolId: number) {
    if(symbolId == 52 || symbolId == 53) return 14;
    return symbolId % 13 + 1;
  }

  get getPredictValue() : number[] {
    return this.predictValue;
  }

  get getPredictSuitType(): string[] {
    return this.predictSuitPath;
  }

  set setSymbolId(symbolID: number) {
    this.symbolId = symbolID;
  }

  get getSymbolID(): number {
    return this.symbolId;
  }

  set setSuitType(suitType: string) {
    this.suitType = suitType;
  }

  get getSuitType(): string {
    return this.suitType;
  }

  set setCardValue(cardValue: number) {
    this.cardValue = cardValue;
  }

  get getCardValue(): number {
    return this.cardValue;
  }

  set setWinnerCardStatus(cardStatus: boolean) {
    this.isWinnerCard = cardStatus;
  }

  get getWinnerCardStatus(): boolean {
    return this.isWinnerCard;
  }

  get imgName() {
    return this.cardValue + this.suitType + ".png";
  }

  update(deltaTime: number) {}
}
