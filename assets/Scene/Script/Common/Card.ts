import { _decorator, Component, Node, Sprite, resources, SpriteFrame, tween, Vec3, Texture2D, Tween, Label, instantiate, Layout, Color, color, CCInteger } from "cc";
import { ResMgr } from "../Manager/ResMgr";
import { Resource } from "../Resource";
import { UserHandCard } from "./UserHandCard";
const { ccclass, property } = _decorator;

export enum CARD_STATE{
  FLIPPED,
  FACE,
}
@ccclass("Card")
export class Card extends Component {
  @property(CCInteger)
  cardNum = 0;

  @property(Sprite)
  card: Sprite = null;
  
  @property({type : SpriteFrame})
  cardBackSprite : SpriteFrame = null;

  @property(Node)
  overlay: Node = null;

  @property(Node)
  predictionContainer: Node = null;

  value: number = -1;
  suit: string = "";
  id: string = "";
  isWinningHand: boolean = false;
  predictValue: number[] = [];
  predictSuitPath: string[] = [];

  start() {
  }

  update(deltaTime: number) {}

  reset() {
    this.value = -1;
    this.suit = "";
    this.id = "";
    this.predictionContainer.removeAllChildren();
    this.predictValue = [];
    this.predictSuitPath = [];
    this.isWinningHand = false;
    // this.overlay.active = true;
    if(this.cardNum == 6){
      this.removeSpriteFrame();
    }else{
      this.card.spriteFrame = this.cardBackSprite;
    }
    this.node.getComponent(Sprite).color = Color.WHITE;
  }

  removeSpriteFrame() {
    this.card.spriteFrame = undefined;
  }

  getCardNum() {
    return this.cardNum;
  }

  miniGameReset() {
    this.value = -1;
    this.suit = "";
    this.id = "";
    this.predictionContainer.removeAllChildren();
    this.predictValue = [];
    this.predictSuitPath = [];
  }

  updateCardData(cardData: any, checkGrayScaleStatus: boolean) {
    if(this.suit == cardData.getSuitType && this.value == cardData.getCardValue ) return;
    this.overlay.active = false;
    this.id = cardData.getSymbolID;
    this.value = cardData.getCardValue;
    this.suit = cardData.getSuitType;
    // this.isWinningHand = cardData.getWinnerCardStatus;
    this.changeCardTexture();
    // if (checkGrayScaleStatus) this.needToDoGrayscale();
  }

  updatePredictData(predictData: any) {
    this.predictValue = predictData.getPredictValue;
    this.predictSuitPath = predictData.getPredictSuitType;
    this.creatPrediction();
  }

  setTransparents(isChanged: boolean){
    if(isChanged){
       this.node.getComponent(Sprite).color = Color.GRAY;
    }else{
       this.node.getComponent(Sprite).color = Color.WHITE;
    }
  }

  setWiningCard(status){
    if(status){
      tween(this.node)
      .repeatForever(tween()
      .by(0.5,{scale : new Vec3(0.02 ,0.02,1)})
      .by(0.5,{scale : new Vec3(-0.02,-0.02,1)})
      ).start();

    }else{
      this.overlay.active = true;
    }
  }

  creatPrediction() {
    for(let a = 0; a < this.predictValue.length; a ++){
        let preditPrefab = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.Game.Prediction);
        let preditNode = instantiate(preditPrefab);
        let iconFrame = ResMgr.Instance.getAsset(Resource.cardIconStr, this.predictSuitPath[a]);
        const spriteFrame = new SpriteFrame();
        const tex = new Texture2D();
        tex.image = iconFrame;
        spriteFrame.texture = tex;
        preditNode.getChildByName("Flower").getComponent(Sprite).spriteFrame = spriteFrame;

        if(this.predictValue[a] == 14){
           preditNode.getChildByName("Num").getComponent(Label).string = "JOKER";
        }else{
           preditNode.getChildByName("Num").getComponent(Label).string = this.predictValue[a].toString();
        }

        if(this.predictSuitPath[a] == "spade" || this.predictSuitPath[a] == "club"){
          preditNode.getChildByName("Num").getComponent(Label).color = new Color(0, 0 ,0);
        }else{
          preditNode.getChildByName("Num").getComponent(Label).color = new Color(255, 0 ,0);
        }

        this.predictionContainer.addChild(preditNode);
        this.predictionContainer.getComponent(Layout).updateLayout(true);
    }
  }

  removePrediction() {
    this.predictionContainer.removeAllChildren();
  }


  setCardState(state : CARD_STATE){
    switch(state){
      case CARD_STATE.FLIPPED:{
        this.updateCard(this.cardBackSprite);
        break;
      }
    }
  }

  changeCardTexture() {
    let imgName = (this.value + this.suit);
    let cardFrame =  ResMgr.Instance.getAsset(Resource.pokerStr, imgName);
    const spriteFrame = new SpriteFrame();
    const tex = new Texture2D();
    tex.image = cardFrame;
    spriteFrame.texture = tex;
    console.log(imgName)
    this.updateCard(spriteFrame);
  }

  updateCard(sprite : SpriteFrame){
    Tween.stopAllByTarget(this.node);
    let speed : any = 0.1;
    tween(this.node)
      .to(speed ? speed : 0.1, {scale : new Vec3(0,1,1)})
      .call(()=>{  this.card.spriteFrame = sprite })
      .to(speed ? speed : 0.1, {scale : new Vec3(1,1,1)})
      .start();

  }

  needToDoGrayscale() {
    if(!this.isWinningHand)
     this.overlay.active = true;
  }
}
