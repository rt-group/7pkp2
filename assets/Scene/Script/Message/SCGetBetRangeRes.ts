import { _decorator, Component, Node } from 'cc';
import { STATE } from '../Constant/State';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import { GAME_SCENE } from '../Utils/GameConfig';
import { GameApp } from '../GameApp';
import proto from "proto";
import { RoomManager } from '../Manager/RoomManager';
import { Loader } from '../Loader/Loader';
// import { RowLoader } from '../../../AssetPackage/Prefab/Load/RowLoader';
const { ccclass, property } = _decorator;

@ccclass('SCGetBetRangeRes')
export class SCGetBetRangeRes extends NotifyComponent {
    state = STATE.SC_GET_BET_RANGE;


    notify(data) {

        let message = proto.PK5Client.GetBetRangeRes.decode(new Uint8Array(data.data.data));
        console.log("getBetRange ", message);
        //設置房間資料
        GameApp.Instance.setScene(GAME_SCENE.ROOM_SCENE);
        // RowLoader.Instance.stopLoader();
        Loader.Instance.stopRowLoader();
        RoomManager.Instance.setRoomData(message);
    }
}

