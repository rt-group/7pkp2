import { _decorator, Component, Node } from 'cc';
import { STATE } from '../Constant/State';
import proto from "proto";
import { SidePanel } from '../Common/SidePanel';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import { Loader } from '../Loader/Loader';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { MiniGame } from '../MiniGame/MiniGame';
import { Game } from '../Game/Game';
// import { RowLoader } from '../../../AssetPackage/Prefab/Load/RowLoader';
const { ccclass, property } = _decorator;

@ccclass('SCLeaveMachineRes')
export class SCLeaveMachineRes extends NotifyComponent {
    state = STATE.SC_LEAVE_MACHINE;
    notify(data) {
        console.log("Into Game Res.....");
        let message = proto.PK5Client.LeaveMachineRes.decode(new Uint8Array(data.data.data));
        console.log(message);
        Game.Instance.showGame();
        Game.Instance.reset();
        SidePanel.Instance.backLobby();
        Loader.Instance.stopRowLoader();
        AudioPlayer.instance.playBGM();
        // RowLoader.Instance.stopLoader();
    }
}

