import { _decorator, Component, Node } from 'cc';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import { STATE } from '../Constant/State';
import { MsgHelp } from '../Utils/MsgHelp';
import proto from "proto";
import { GAME_SCENE} from '../Utils/GameConfig';
import { GameApp } from '../GameApp';
import { UserInfo } from '../Common/UserInfo';
const { ccclass, property } = _decorator;

@ccclass('SCAuthConnectRes')
export class SCAuthConnectRes extends NotifyComponent {
    state = STATE.SC_AUTH_CONNECT_RES;
    notify(data) {
        let message = proto.PK5Client.AuthConnectRes.decode(new Uint8Array(data.data.data));
        UserInfo.getInstance().initiateUserInfo(message);
        console.log("SCAuthConnectRes.....", message);
        GameApp.Instance.setScene(GAME_SCENE.LOADING_SCENE);
    }
}

