import { _decorator, Component, Node } from 'cc';
import { STATE } from '../Constant/State';
import proto from "proto";
import { BetPanel } from '../Game/BetPanel';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import { UserInfo } from '../Common/UserInfo';
import { Game } from '../Game/Game';
import { WinResult } from '../Game/WinResult';
import { Loader } from '../Loader/Loader';
import { SidePanel } from '../Common/SidePanel';
// import { CardsLoader } from '../../../AssetPackage/Prefab/Load/CardsLoader';
const { ccclass, property } = _decorator;

@ccclass('SCBetRes')
export class SCBetRes extends NotifyComponent {
    state = STATE.SC_BET;
    notify(data) {
        let message = proto.PK5Client.BetRes.decode(new Uint8Array(data.data.data));
        console.log("SCBetRes.....", message);
        // CardsLoader.Instance.stopLoader();
        Loader.Instance.stopCardLoader();
        if(BetPanel.Instance.isSkipGame && message.pockers.length <= 7){
            BetPanel.Instance.skipSingleBet();

            if(message.pockers.length == 7){
                BetPanel.Instance.updateSkipBoolean(false);
            }else{
                BetPanel.Instance.skipBet();
            }
        }else{
            BetPanel.Instance.setBetByType();
            Game.Instance.showSkipBtn();
        }
     
        BetPanel.Instance.setTableBonus(message.fakeJpList);
        BetPanel.Instance.updateTableBonus();
        BetPanel.Instance.setTableBonus(message.jpList);
        SidePanel.Instance.disableLeaveBtn();
        if(message.pockers.length < 7){
            BetPanel.Instance.enableBtns();
        }else{
            Game.Instance.removePrediction();
        }
        Game.Instance.updateBonusMultipler();
        console.log("winGOld....",message.winResult.winGold);
        Game.Instance.showGameTable();
        Game.Instance.updateGameStatus(message);
      
        
    }
}

