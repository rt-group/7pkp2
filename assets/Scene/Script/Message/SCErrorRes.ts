import { _decorator, Component, Node } from 'cc';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import proto from "proto";
import { STATE } from '../Constant/State';
import { PopupManager } from '../Manager/PopupManager';
import { POPUP_BTN_TITLE, POPUP_ICON_TYPE, POPUP_TITLE } from '../Common/Popup';
import { POPUP_MESSAGE } from '../Utils/GameConfig';
import { Loader } from '../Loader/Loader';
const { ccclass, property } = _decorator;

@ccclass('SCErrorRes')
export class SCErrorRes extends NotifyComponent {
    state = STATE.SC_ERROR;
    notify(data) {
        let message = proto.PK5Client.Error.decode(new Uint8Array(data.data.data));
        console.log("Error.....", message);
        //code 2 
        PopupManager.Instance.showPopup(POPUP_ICON_TYPE.ERROR_INTERNET, POPUP_TITLE.SOMETHING_WENT_WRONG, POPUP_MESSAGE.NO_INTERNET_CONNECTION, POPUP_BTN_TITLE.RECONNECT)
        //"rpc error: code = Unavailable desc = connection error: desc = "transport: Error while dialing: dial tcp 127.0.0.1:20091: connect: connection refused""
        //"rpc error: code = Unavailable desc = error reading from server: EOF"
    }
}

