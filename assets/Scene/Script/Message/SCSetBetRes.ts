import { _decorator, Component, Node } from 'cc';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import { STATE } from '../Constant/State';
import proto from "proto";
import { GameApp } from '../GameApp';
import { GAME_SCENE } from '../Utils/GameConfig';
import { Lobby } from '../Lobby/Lobby';
const { ccclass, property } = _decorator;

@ccclass('SCSetBetRes')
export class SCSetBetRes extends NotifyComponent {
    state = STATE.SC_SET_BET;
    
    start() {

    }

    update(deltaTime: number) {
        
    }

    notify(data: any) {
        let message = proto.PK5Client.SetBetRes.decode(new Uint8Array(data.data.data));
        if(message.result)
            GameApp.Instance.setScene(GAME_SCENE.LOBBY_SCENE);
            Lobby.Instance.CS_GetMachineList();
    }
}

