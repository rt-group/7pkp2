import { _decorator, Component, Node } from 'cc';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import proto from "proto";
import { STATE } from '../Constant/State';
import { GameApp } from '../GameApp';
import { GAME_SCENE } from '../Utils/GameConfig';
import { Lobby } from '../Lobby/Lobby';
import { Game } from '../Game/Game';
import { BetPanel } from '../Game/BetPanel';
import { SidePanel } from '../Common/SidePanel';
import { Rules } from '../Common/Rules';
import { Setting } from '../Common/Setting';
import { Loader } from '../Loader/Loader';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
// import { RowLoader } from '../../../AssetPackage/Prefab/Load/RowLoader';
const { ccclass, property } = _decorator;

@ccclass('SCIntoGameRes')
export class SCIntoGameRes extends NotifyComponent {
    state = STATE.SC_INTO_GAME;
    notify(data) {
        console.log("Into Game Res.....");
        let message = proto.PK5Client.IntoGameRes.decode(new Uint8Array(data.data.data));
        console.log(message);
        AudioPlayer.instance.stopBGM();
        Lobby.Instance.reset();
        GameApp.Instance.setScene(GAME_SCENE.GAME_SCENE);
        Game.Instance.updateUserData();
        BetPanel.Instance.init();
        Rules.Instance.initRuleTable();
        Setting.Instance.initSetting();
        // RowLoader.Instance.stopLoader();
        Loader.Instance.stopRowLoader();
        Game.Instance.resetPoker();
    }
}

