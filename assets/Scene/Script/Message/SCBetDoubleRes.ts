import { _decorator, Component, Node } from 'cc';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import { STATE } from '../Constant/State';
import proto from "proto";
import { MINI_BET_TYPE, MiniGame } from '../MiniGame/MiniGame';
import { UserInfo } from '../Common/UserInfo';
import { Game } from '../Game/Game';
import { WinLoseLayer } from '../Game/WinLoseLayer';
import AudioPlayer from '../../../../../framework-fish/Audio/AudioPlayer';
import { AudioID } from '../../../../../framework-fish/Audio/AudioEnum';
import { MiniGameBet } from '../MiniGame/MiniGameBet';
import { SidePanel } from '../Common/SidePanel';
import { MiniGameInfo } from '../Utils/GameConfig';

const { ccclass, property } = _decorator;

@ccclass('SCBetDoubleRes')
export class SCBetDoubleRes extends NotifyComponent {
    state = STATE.SC_BET_DOUBLE;
    notify(data) {
        let message = proto.PK5Client.BetDoubleRes.decode(new Uint8Array(data.data.data));
        console.log("SC_BET_DOUBLE.....", message);
        MiniGame.Instance.updatePokerRecord(message);
        MiniGameBet.Instance.reset();
        //贏
        if(message.result && message.winResult.winGold > 0){
            // MiniGame.Instance.showMiniWinByType();
            // let calculateWinGold = Math.abs(message.nowGold - UserInfo.getInstance().getWalletBalance); 
            UserInfo.getInstance().setCurrentGameWinGold(message.winResult.winGold);
            UserInfo.getInstance().setWalletBalance = message.nowGold;
            MiniGame.Instance.enableBtns();
            Game.Instance.updateMiniGameResult(message.winResult.winGold);
            MiniGame.Instance.minibetReset();
            // MiniGame.Instance.bettingCardReset();
            MiniGame.Instance.showScoreBtn();
            SidePanel.Instance.enableLeaveBtn();
        }
        //輸 
        else if(message.result && message.winResult.winGold <= 0){
            UserInfo.getInstance().setWalletBalance = message.nowGold;
            Game.Instance.updateUserMoney();
            switch(MiniGameInfo.MINI_TYPE){
                case MINI_BET_TYPE.SMALL:
                    Game.Instance.setBonusScore(0);
                    break;
                case MINI_BET_TYPE.BIG:
                    Game.Instance.setBonusScore(0);
                    break;
                case MINI_BET_TYPE.DOUBLE:
                    Game.Instance.setBonusScore(0);
                    break;
            }
            // AudioPlayer.instance.play(AudioID.NOWIN);
            let time = setTimeout(() =>{
                Game.Instance.playEndGameRun();
                clearTimeout(time);
            }, 1000);
            // WinLoseLayer.Instance.showPlayerLose(() =>{
                // let time = setTimeout(() =>{
                    // MiniGame.Instance.reset();
                    // Game.Instance.reset();
                    // Game.Instance.showGame();
                    // clearTimeout(time);
                // }, 3000)
            // })
        }

  
    }
}

