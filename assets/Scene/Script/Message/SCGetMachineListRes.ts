import { _decorator, Component, Node } from 'cc';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import proto from "proto";
import { STATE } from '../Constant/State';
import { MachineTable } from '../Lobby/MachineTable';
import { Loader } from '../Loader/Loader';
// import { RowLoader } from '../../../AssetPackage/Prefab/Load/RowLoader';
const { ccclass, property } = _decorator;

@ccclass('SCGetMachineListRes')
export class SCGetMachineListRes extends NotifyComponent {
    state = STATE.SC_GET_MACHINE_LIST;
    start() {

    }

    update(deltaTime: number) {
        
    }

    notify(data) {
        console.log("GetMachineList.....");
        let message = proto.PK5Client.GetMachineListRes.decode(new Uint8Array(data.data.data));
        MachineTable.Instance.initData(message);
        console.log(message);
        // RowLoader.Instance.stopLoader();
        Loader.Instance.stopRowLoader();
    }
}

