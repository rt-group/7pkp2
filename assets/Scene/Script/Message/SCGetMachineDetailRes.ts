import { _decorator, Component, Node } from 'cc';
import { STATE } from '../Constant/State';
import { NotifyComponent } from '../../../../../framework-fish/Core/view/NotifyComponent';
import proto from "proto";
import { Lobby } from '../Lobby/Lobby';
const { ccclass, property } = _decorator;

@ccclass('SCGetMachineDetailRes')
export class SCGetMachineDetailRes extends NotifyComponent {
    state = STATE.SC_GET_MACHINE_DETAIL;
    start() {

    }

    notify(data) {
        let message = proto.PK5Client.GetMachineDetialRes.decode(new Uint8Array(data.data.data));
        Lobby.Instance.updateMachineData(message);
        console.log(message);
    }
}

