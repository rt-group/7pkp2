import { _decorator, Component, instantiate, Node } from 'cc';
import { ResMgr } from './Manager/ResMgr';
import { Resource } from './Resource';
import { GAME_SCENE } from './Utils/GameConfig';
import { Loading } from './Loading/Loading';
import { PopupManager } from './Manager/PopupManager';
import proto from "proto";
import { MsgHelp } from './Utils/MsgHelp';
import { Connect } from './Connect/Connect';
import { Loader } from './Loader/Loader';
import { Game } from './Game/Game';
// import { RowLoader } from '../../AssetPackage/';
const { ccclass, property } = _decorator;

@ccclass('GameApp')
export class GameApp extends Component {
    public static Instance: GameApp = null as unknown as GameApp;

    public checkResourceLoaded: boolean = false;
    public currentScene: number = null;

    onLoad(): void{
        if(GameApp.Instance === null){
            GameApp.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
       this.setScene(GAME_SCENE.LOGIN_SCENE);
    }

    public setScene(scene: number, data?: any) {
        this.hideAllChildren();
        this.currentScene = scene;
        switch(scene){
            case GAME_SCENE.LOGIN_SCENE:
                this.EnterLogin();
                break;
            case GAME_SCENE.LOADING_SCENE:
                this.loadResource();
                break;
            case GAME_SCENE.ROOM_SCENE:
                this.EnterRoom();
                break;
            case GAME_SCENE.LOBBY_SCENE:
                this.EnterLobby();
                break;
            case GAME_SCENE.GAME_SCENE:
                this.EnterGame(data);
                break;
        }
    }

    public GameStart():void {
        this.setScene(GAME_SCENE.LOGIN_SCENE);
    }

    public getCurrentScene() {
        return this.currentScene;
    }


    public loadResource() {
           //先讀取loading介面的資源
           ResMgr.Instance.preloadResPackage(Resource.loadPkg, ()=>{}, ()=>{
            this.EnterLoading();
            //讀取所有資源
            ResMgr.Instance.preloadResPackage(Resource.resPkg, (now: number, total: number)=>{
                let progressNum = Math.floor((now/total) * 100);
                Loading.Instance.updateLoadingProgress(progressNum);
            }, ()=>{
                let time = setTimeout(()=>{
                    // this.removeLoading();
                    // this.setScene(GAME_SCENE.ROOM_SCENE);
                    this.CS_GetBetRange();
                    this.checkResourceLoaded = true;
                    // Loader.Instance.showRowLoader();
                    // RowLoader.Instance.showLoader();
                    clearTimeout(time);
                },500);
            });

        })
        //end
    }

    public hideAllChildren() {
        this.node.children.forEach((element) => {
            element.active = false;
        })
    }

    public CS_GetBetRange() {
        var betRange = proto.PK5Client.GetBetRange;
        var betRangeData =betRange.create({
        });
        var buffer = betRange.encode(betRangeData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_GET_BET_RANGE;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
    }

    public checkLoadingFinished() {
        return this.checkResourceLoaded;
    }

    public EnterRoom(): void {
        if(this.node.getChildByName("RoomTable")) {
            this.node.getChildByName("RoomTable").active = true;
        }else{
            let roomPath = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.Lobby.Room);
            let roomTable = instantiate(roomPath);
            this.node.addChild(roomTable);
        }
    }

    public EnterLoading(): void {
        // ResMgr.Instance.preloadResPackage(Resource.loadPkg, ()=> {}, ()=> {
            let loadPath = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.UI.LoadingTable);
            let loadTable = instantiate(loadPath);
            this.node.addChild(loadTable);
        // });
    }

    public EnterLogin(): void {
        ResMgr.Instance.preloadResPackage(Resource.loginPkg, ()=> {}, ()=> {
            let loginPath = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.UI.LoginTable);
            let loginTable = instantiate(loginPath);
            this.node.addChild(loginTable);

            PopupManager.Instance.createPopup();
        });
    }

    public EnterLobby(): void {
        if(this.node.getChildByName("Lobby")) {
            this.node.getChildByName("Lobby").active = true;
        }else{
            let lobbyPath = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.UI.LobbyTable);
            let lobbyTable = instantiate(lobbyPath);
            this.node.addChild(lobbyTable);
        }
    }

    public EnterGame(data: any): void {
        if(this.node.getChildByName("Game")) {
            this.node.getChildByName("Game").active = true;
            Game.Instance.setRoomNum();
            Game.Instance.hideSkipBtn();
        }else{
            let gamePath = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.Game.Game);
            let gameTable = instantiate(gamePath);
            this.node.addChild(gameTable);
        }
    }
}

