import { _decorator, Component, Node } from 'cc';
import { ResMgr } from './Manager/ResMgr';
import { GameApp } from './GameApp';
const { ccclass, property } = _decorator;

@ccclass('GameLaunch')
export class GameLaunch extends Component {
    public static Instance: GameLaunch = null as unknown as GameLaunch;

    onLoad(): void{
        if(GameLaunch.Instance === null){
            GameLaunch.Instance = this;
        }else{
            this.destroy();
            return;
        }

        //初始化遊戲框架 , 網路管理, 資源管理, 協議管理...etc
        this.node.addComponent(ResMgr).init();
        //end

        //遊戲邏輯統一入口
        this.node.addComponent(GameApp);
        //end
    }

    start(): void{
        //檢查資源
        //end

        GameApp.Instance.GameStart();
    }
}

