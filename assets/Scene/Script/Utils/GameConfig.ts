export class CardType {
  public static CardState = {
    Spade:0,     //黑桃
    Hearts:1,    //红桃
    Diamonds:2,      //梅花
    Club:3,   //方块
    Ghost: 4    //鬼牌

  }
}

export enum WIN_TYPE {
   NOTHING = 0,
   A_PAIR = 1,
   TWO_PAIR = 2,
   THREE_KIND = 3,
   STRAIGHT = 4,
   FLUSH = 5,
   FULL_HOUSE = 6,
   FOUR_KIND = 7,
   STR_FLUSH = 8,
   FOUR_KIND_WITH_JOKER = 9,
   STRAIGHT_FLUSH = 10,
  //  FOUR_KIND_NO_JOKER = 11,
  //  STRAIGHT_FLUSH_NO_JOKER = 12,
  //  FOUR_KIND_WITH_ONE_JOKER = 13,
  //  STRAIGHT_FLUSH_WITH_NO_JOKER = 14
}

export enum GAME_SCENE {
    LOGIN_SCENE = 0,
    LOADING_SCENE = 1,
    ROOM_SCENE = 2,
    LOBBY_SCENE = 3,
    GAME_SCENE = 4,
  }

  export enum SIDE_MENU {
    EXIT = 0,
    TABLES = 1,
    RULES = 2,
    SETTING = 3,
  }

  export enum POPUP_MESSAGE {
    GAME_QUIT = "Do you really want to quit the game? You will lose your current progress.",
    EXISTING_GAME = "You can't play on table xxx. As you have an active game. Confirm to play on the table ",
    CONFIRM_TO_PLAY = "Press confim to play on exiting table",
    SOMETHING_WENT_WRONG = "Make sure wifi or cellular data is turned \n on and then try again.",
    SERVER_NOT_FOUND = "We are having issue in connecting to server, please try again after sometime.",
    NO_INTERNET_CONNECTION = "LOW INTERNET CONNECTION DETECTED",
    GAME_LOOSE = "You Loose! Please try again!",
    GAME_WON = "Congratulations! You won the game",
    SELECT_ANOTHER_TABLE = "PLEASE SELECT ANOTHER TABLE",
    NO_HISTORY = "Selected table has no history",
    UNAUTHORISED = "Unauthorised access!",
    WRONG_ACCOUNT_OR_PASSWORD = "Oops! AccountID or Password went wrong",
    NO_ENOUGH_MONEY = "Oops! run our of money",
    PLEASE_CHOOSE_ANOTHER_BET = "PLEASE SELECT ANOTHER BET",
  }

  export const LowRewords = {
    fullHouse: "FULL HOUSE",
    flush: "FLUSH",
    straight: "STRAIGHT",
    threeOfKind: "3 OF A KIND",
    twoPair: "2 PAIR (10)",
    acePair: "ACE PAIR",
  };
  
  export const HighRewords = {
    royalFlush: "ROYAL FLUSH",
    fiveOfKind: "5 OF A KIND",
    straightFlush: "STR FLUSH",
    fourOfKind: "4 OF A KIND",
  };

  export const GameInfo = {
    currentMachineId: "",
    currentRoomRange: null,
    currentMachineBonusList: null
  }

  export const MiniGameInfo = {
    MINI_TYPE: null,
  }

  export const GameLabel = {
    huluStrArr: ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
  }

  export enum RESULT_ENUM {
    ROYAL_FLUSH = 500,
    FIVE_KIND = 200,
    STR_FLUSH = 120,
    FOUR_KIND = 50,
    FULL_HOUSE = 7,
    FLUSH = 5,
    STRAIGHT = 3,
    THREE_KIND = 2,
    TWO_PAIR = 1,
    ACE_PAIR = 1
}



