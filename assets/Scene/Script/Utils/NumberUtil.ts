export class NumberUtil{
    public static add(arg1, arg2) {
        let r1, r2, m;

        if(arg1.toString().indexOf(".")>=0){
            try {
                r1 = arg1.toString().split(".")[1].length;
            } catch (e) {
                r1 = 0;
            }
        }else{
            r1 = 0;
        }

        if(arg2.toString().indexOf(".")>=0) {
            try {
                r2 = arg2.toString().split(".")[1].length;
            } catch (e) {
                r2 = 0;
            }
        }else{
            r2 = 0;
        }

        m = Math.pow(10, Math.max(r1, r2));
        return (this.mul(arg1, m) + this.mul(arg2, m)) / m;
    }
    //浮點數相減
    public static sub(arg1, arg2) {
        //   console.debug('arg1:'+arg1+' arg2:'+arg2);
        let r1, r2, m, n;

        if(arg1.toString().indexOf(".")>=0){
            try {
                r1 = arg1.toString().split(".")[1].length
            } catch (e) {
                r1 = 0
            }
        }else{
            r1 = 0;
        }

        if(arg2.toString().indexOf(".")>=0) {
            try {
                r2 = arg2.toString().split(".")[1].length
            } catch (e) {
                r2 = 0
            }
        }else{
            r2 = 0;
        }

        m = Math.pow(10, Math.max(r1, r2));
        n = (r1 >= r2) ? r1 : r2;
        //  console.debug('m:'+m+' n:'+n);
        return Number(((arg1 * m - arg2 * m) / m).toFixed(n));
    }
    //浮點數相乘
    public static mul(arg1, arg2) {
        let m = 0, s1 = arg1.toString(), s2 = arg2.toString();

        if(s1.indexOf(".")>=0) {
            try {
                m += s1.split(".")[1].length;
            } catch (e) {
            }
        }

        if(s2.indexOf(".")>=0) {
            try {
                m += s2.split(".")[1].length;
            } catch (e) {
            }
        }
        return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
    }
    //浮點數相除
    public static div(arg1, arg2) {
        let t1 = 0, t2 = 0, r1, r2;

        if(arg1.toString().indexOf(".")>=0) {
            try {
                t1 = arg1.toString().split(".")[1].length
            } catch (e) {
            }
        }

        if(arg2.toString().indexOf(".")>=0) {
            try {
                t2 = arg2.toString().split(".")[1].length
            } catch (e) {
            }
        }

        r1 = Number(arg1.toString().replace(".", ""));
        r2 = Number(arg2.toString().replace(".", ""));

        return this.mul((r1 / r2), Math.pow(10, t2 - t1));
    }
    public static isNumber(arg) {
        return (!isNaN(parseFloat(arg)) && isFinite(arg));
    }

    // 字符串递归方法
    public static formatNumber(num, chart=',', length=3) {
    let result = ''
    let nums = num.toString().split('.')
    let int = nums[0]
    let decmial = nums[1] ? '.' + nums[1] : ''
    let index = 0
    for (let n = int.length - 1; n >= 0; n--) {
      index ++
      result = int[n] + result
      if (index % length === 0 && n !== 0) { result = chart + result }
    }
    return result + decmial
    }

  

    //digit 小數位數 , cutZero 去除小數尾端0
    //問題 89950 => 5.29117647*17000 = 89949.99999
    //console.log(NumberUtil.digits( 5.29117647*17000,3,false)) => 89950.000
    //問題 999999999 => 1,000,000.0K
    //console.log(NumberUtil.coinTransV2(999999999)) => 999,999.9K
    public static formatDigits(num:number,digit:number=2,cutZero:boolean=false,floor:boolean=true):string {
        //改取小數點第4位四捨五入,toFixed固定四捨五入
        num = parseFloat(num.toFixed(4));

        //2:無條件捨去 1:四捨五入
        const overDig:number=floor?2:1;
        //第N+1位作四捨五入後取N位
        let res:string = num.toFixed(digit+overDig);
        res=res.substring(0,res.length-overDig);

        if(cutZero || digit === 0){
            res=parseFloat(res).toString();
        }
        //console.log("#digits2:"+num+"=>"+res)
        //console.log("res="+res);
        return res;
    }

    /***小於 distinction 顯示小數digit位數,否則整數 ex 10 以下轉小數**/
    public static formatFloatAuto(num:number, digit:number,distinction:number):string {
        return (num<distinction)?NumberUtil.formatFloat(num,digit):NumberUtil.formatInt(num);
    }

    public static formatFloat(num:number, digit:number):string {
        //digit = digit > 0 && digit <= 20 ? digit : 2;
        //let s = parseFloat((numStr + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
        let s = num.toFixed(digit) + "";
        //let l = s.split(".")[0].split("").reverse(), r = s.split(".")[1];
        const sp=s.split(".");
        const l = sp[0].split("").reverse();
        const r = sp[1];
        let result = "";
        for (let i = 0; i < l.length; i++) {
            result += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
        }
        return result.split("").reverse().join("") + "." + r;
    }

    public static formatInt(num:number):string {
        // let numStr:string=(num || 0).toString();
        let numStr:string=(num || 0).toFixed(0)
        //return numStr;
        let result='';
        while (numStr.length > 3) {
            result = ',' + numStr.slice(-3) + result;
            numStr = numStr.slice(0, numStr.length - 3);
        }
        if (numStr.length) { result = numStr + result; }
        return result;
    }
}



