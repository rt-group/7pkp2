
export class MsgHelp {
    public static encodeMsg( key:number , data: ArrayBuffer ){
        var keyBuf = new Uint8Array(4);
        for (let i = 0; i < 4; i++) {
            keyBuf[i] = key % 256;
            key = Math.floor(key / 256);
        }
    
        
        var temp = new Uint8Array(data.byteLength + 4);
        temp.set(keyBuf,0);
        temp.set(new Uint8Array(data), keyBuf.byteLength);
        

        return temp.buffer;
    }

    public static decodeMsg( msg: ArrayBuffer ){
        var keys = new Uint8Array(msg.slice(0,4));
    
        var msgkey = 0;
        
        for ( var i = keys.length - 1; i >= 0; i--) {
            msgkey = (msgkey * 256) + keys[i];
        }
        
        var arrBuf= msg.slice(4, msg.byteLength);
        
        var objArrayBuf =new Uint8Array( arrBuf ,0 , arrBuf.byteLength );

        return { key:msgkey , data:objArrayBuf.buffer };
    }
}