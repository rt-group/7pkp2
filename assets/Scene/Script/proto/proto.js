/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/dist/protobuf.js");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.PK5Client = (function() {

    /**
     * Namespace PK5Client.
     * @exports PK5Client
     * @namespace
     */
    var PK5Client = {};

    PK5Client.Connect = (function() {

        /**
         * Properties of a Connect.
         * @memberof PK5Client
         * @interface IConnect
         * @property {boolean|null} [result] Connect result
         */

        /**
         * Constructs a new Connect.
         * @memberof PK5Client
         * @classdesc Represents a Connect.
         * @implements IConnect
         * @constructor
         * @param {PK5Client.IConnect=} [properties] Properties to set
         */
        function Connect(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Connect result.
         * @member {boolean} result
         * @memberof PK5Client.Connect
         * @instance
         */
        Connect.prototype.result = false;

        /**
         * Creates a new Connect instance using the specified properties.
         * @function create
         * @memberof PK5Client.Connect
         * @static
         * @param {PK5Client.IConnect=} [properties] Properties to set
         * @returns {PK5Client.Connect} Connect instance
         */
        Connect.create = function create(properties) {
            return new Connect(properties);
        };

        /**
         * Encodes the specified Connect message. Does not implicitly {@link PK5Client.Connect.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.Connect
         * @static
         * @param {PK5Client.IConnect} message Connect message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Connect.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            return writer;
        };

        /**
         * Encodes the specified Connect message, length delimited. Does not implicitly {@link PK5Client.Connect.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.Connect
         * @static
         * @param {PK5Client.IConnect} message Connect message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Connect.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Connect message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.Connect
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.Connect} Connect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Connect.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.Connect();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Connect message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.Connect
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.Connect} Connect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Connect.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Connect message.
         * @function verify
         * @memberof PK5Client.Connect
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Connect.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            return null;
        };

        /**
         * Creates a Connect message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.Connect
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.Connect} Connect
         */
        Connect.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.Connect)
                return object;
            var message = new $root.PK5Client.Connect();
            if (object.result != null)
                message.result = Boolean(object.result);
            return message;
        };

        /**
         * Creates a plain object from a Connect message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.Connect
         * @static
         * @param {PK5Client.Connect} message Connect
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Connect.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.result = false;
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            return object;
        };

        /**
         * Converts this Connect to JSON.
         * @function toJSON
         * @memberof PK5Client.Connect
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Connect.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Connect
         * @function getTypeUrl
         * @memberof PK5Client.Connect
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Connect.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.Connect";
        };

        return Connect;
    })();

    PK5Client.AuthConnect = (function() {

        /**
         * Properties of an AuthConnect.
         * @memberof PK5Client
         * @interface IAuthConnect
         * @property {string|null} [account] AuthConnect account
         * @property {string|null} [pwd] AuthConnect pwd
         * @property {string|null} [vendor] AuthConnect vendor
         * @property {string|null} [platform] AuthConnect platform
         * @property {string|null} [token] AuthConnect token
         */

        /**
         * Constructs a new AuthConnect.
         * @memberof PK5Client
         * @classdesc Represents an AuthConnect.
         * @implements IAuthConnect
         * @constructor
         * @param {PK5Client.IAuthConnect=} [properties] Properties to set
         */
        function AuthConnect(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * AuthConnect account.
         * @member {string} account
         * @memberof PK5Client.AuthConnect
         * @instance
         */
        AuthConnect.prototype.account = "";

        /**
         * AuthConnect pwd.
         * @member {string} pwd
         * @memberof PK5Client.AuthConnect
         * @instance
         */
        AuthConnect.prototype.pwd = "";

        /**
         * AuthConnect vendor.
         * @member {string} vendor
         * @memberof PK5Client.AuthConnect
         * @instance
         */
        AuthConnect.prototype.vendor = "";

        /**
         * AuthConnect platform.
         * @member {string} platform
         * @memberof PK5Client.AuthConnect
         * @instance
         */
        AuthConnect.prototype.platform = "";

        /**
         * AuthConnect token.
         * @member {string} token
         * @memberof PK5Client.AuthConnect
         * @instance
         */
        AuthConnect.prototype.token = "";

        /**
         * Creates a new AuthConnect instance using the specified properties.
         * @function create
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {PK5Client.IAuthConnect=} [properties] Properties to set
         * @returns {PK5Client.AuthConnect} AuthConnect instance
         */
        AuthConnect.create = function create(properties) {
            return new AuthConnect(properties);
        };

        /**
         * Encodes the specified AuthConnect message. Does not implicitly {@link PK5Client.AuthConnect.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {PK5Client.IAuthConnect} message AuthConnect message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AuthConnect.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.account != null && Object.hasOwnProperty.call(message, "account"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.account);
            if (message.pwd != null && Object.hasOwnProperty.call(message, "pwd"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.pwd);
            if (message.vendor != null && Object.hasOwnProperty.call(message, "vendor"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.vendor);
            if (message.platform != null && Object.hasOwnProperty.call(message, "platform"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.platform);
            if (message.token != null && Object.hasOwnProperty.call(message, "token"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.token);
            return writer;
        };

        /**
         * Encodes the specified AuthConnect message, length delimited. Does not implicitly {@link PK5Client.AuthConnect.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {PK5Client.IAuthConnect} message AuthConnect message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AuthConnect.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an AuthConnect message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.AuthConnect} AuthConnect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AuthConnect.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.AuthConnect();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.account = reader.string();
                        break;
                    }
                case 2: {
                        message.pwd = reader.string();
                        break;
                    }
                case 3: {
                        message.vendor = reader.string();
                        break;
                    }
                case 4: {
                        message.platform = reader.string();
                        break;
                    }
                case 5: {
                        message.token = reader.string();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an AuthConnect message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.AuthConnect} AuthConnect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AuthConnect.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an AuthConnect message.
         * @function verify
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        AuthConnect.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.account != null && message.hasOwnProperty("account"))
                if (!$util.isString(message.account))
                    return "account: string expected";
            if (message.pwd != null && message.hasOwnProperty("pwd"))
                if (!$util.isString(message.pwd))
                    return "pwd: string expected";
            if (message.vendor != null && message.hasOwnProperty("vendor"))
                if (!$util.isString(message.vendor))
                    return "vendor: string expected";
            if (message.platform != null && message.hasOwnProperty("platform"))
                if (!$util.isString(message.platform))
                    return "platform: string expected";
            if (message.token != null && message.hasOwnProperty("token"))
                if (!$util.isString(message.token))
                    return "token: string expected";
            return null;
        };

        /**
         * Creates an AuthConnect message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.AuthConnect} AuthConnect
         */
        AuthConnect.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.AuthConnect)
                return object;
            var message = new $root.PK5Client.AuthConnect();
            if (object.account != null)
                message.account = String(object.account);
            if (object.pwd != null)
                message.pwd = String(object.pwd);
            if (object.vendor != null)
                message.vendor = String(object.vendor);
            if (object.platform != null)
                message.platform = String(object.platform);
            if (object.token != null)
                message.token = String(object.token);
            return message;
        };

        /**
         * Creates a plain object from an AuthConnect message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {PK5Client.AuthConnect} message AuthConnect
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        AuthConnect.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.account = "";
                object.pwd = "";
                object.vendor = "";
                object.platform = "";
                object.token = "";
            }
            if (message.account != null && message.hasOwnProperty("account"))
                object.account = message.account;
            if (message.pwd != null && message.hasOwnProperty("pwd"))
                object.pwd = message.pwd;
            if (message.vendor != null && message.hasOwnProperty("vendor"))
                object.vendor = message.vendor;
            if (message.platform != null && message.hasOwnProperty("platform"))
                object.platform = message.platform;
            if (message.token != null && message.hasOwnProperty("token"))
                object.token = message.token;
            return object;
        };

        /**
         * Converts this AuthConnect to JSON.
         * @function toJSON
         * @memberof PK5Client.AuthConnect
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        AuthConnect.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for AuthConnect
         * @function getTypeUrl
         * @memberof PK5Client.AuthConnect
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        AuthConnect.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.AuthConnect";
        };

        return AuthConnect;
    })();

    PK5Client.AuthConnectRes = (function() {

        /**
         * Properties of an AuthConnectRes.
         * @memberof PK5Client
         * @interface IAuthConnectRes
         * @property {string|null} [name] AuthConnectRes name
         * @property {number|null} [money] AuthConnectRes money
         * @property {number|null} [uid] AuthConnectRes uid
         * @property {number|null} [watchAdTimeNow] AuthConnectRes watchAdTimeNow
         */

        /**
         * Constructs a new AuthConnectRes.
         * @memberof PK5Client
         * @classdesc Represents an AuthConnectRes.
         * @implements IAuthConnectRes
         * @constructor
         * @param {PK5Client.IAuthConnectRes=} [properties] Properties to set
         */
        function AuthConnectRes(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * AuthConnectRes name.
         * @member {string} name
         * @memberof PK5Client.AuthConnectRes
         * @instance
         */
        AuthConnectRes.prototype.name = "";

        /**
         * AuthConnectRes money.
         * @member {number} money
         * @memberof PK5Client.AuthConnectRes
         * @instance
         */
        AuthConnectRes.prototype.money = 0;

        /**
         * AuthConnectRes uid.
         * @member {number} uid
         * @memberof PK5Client.AuthConnectRes
         * @instance
         */
        AuthConnectRes.prototype.uid = 0;

        /**
         * AuthConnectRes watchAdTimeNow.
         * @member {number} watchAdTimeNow
         * @memberof PK5Client.AuthConnectRes
         * @instance
         */
        AuthConnectRes.prototype.watchAdTimeNow = 0;

        /**
         * Creates a new AuthConnectRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {PK5Client.IAuthConnectRes=} [properties] Properties to set
         * @returns {PK5Client.AuthConnectRes} AuthConnectRes instance
         */
        AuthConnectRes.create = function create(properties) {
            return new AuthConnectRes(properties);
        };

        /**
         * Encodes the specified AuthConnectRes message. Does not implicitly {@link PK5Client.AuthConnectRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {PK5Client.IAuthConnectRes} message AuthConnectRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AuthConnectRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.name != null && Object.hasOwnProperty.call(message, "name"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.name);
            if (message.money != null && Object.hasOwnProperty.call(message, "money"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.money);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.uid);
            if (message.watchAdTimeNow != null && Object.hasOwnProperty.call(message, "watchAdTimeNow"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.watchAdTimeNow);
            return writer;
        };

        /**
         * Encodes the specified AuthConnectRes message, length delimited. Does not implicitly {@link PK5Client.AuthConnectRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {PK5Client.IAuthConnectRes} message AuthConnectRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        AuthConnectRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an AuthConnectRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.AuthConnectRes} AuthConnectRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AuthConnectRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.AuthConnectRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.name = reader.string();
                        break;
                    }
                case 2: {
                        message.money = reader.int32();
                        break;
                    }
                case 3: {
                        message.uid = reader.int32();
                        break;
                    }
                case 4: {
                        message.watchAdTimeNow = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an AuthConnectRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.AuthConnectRes} AuthConnectRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        AuthConnectRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an AuthConnectRes message.
         * @function verify
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        AuthConnectRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.name != null && message.hasOwnProperty("name"))
                if (!$util.isString(message.name))
                    return "name: string expected";
            if (message.money != null && message.hasOwnProperty("money"))
                if (!$util.isInteger(message.money))
                    return "money: integer expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            if (message.watchAdTimeNow != null && message.hasOwnProperty("watchAdTimeNow"))
                if (!$util.isInteger(message.watchAdTimeNow))
                    return "watchAdTimeNow: integer expected";
            return null;
        };

        /**
         * Creates an AuthConnectRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.AuthConnectRes} AuthConnectRes
         */
        AuthConnectRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.AuthConnectRes)
                return object;
            var message = new $root.PK5Client.AuthConnectRes();
            if (object.name != null)
                message.name = String(object.name);
            if (object.money != null)
                message.money = object.money | 0;
            if (object.uid != null)
                message.uid = object.uid | 0;
            if (object.watchAdTimeNow != null)
                message.watchAdTimeNow = object.watchAdTimeNow | 0;
            return message;
        };

        /**
         * Creates a plain object from an AuthConnectRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {PK5Client.AuthConnectRes} message AuthConnectRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        AuthConnectRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.name = "";
                object.money = 0;
                object.uid = 0;
                object.watchAdTimeNow = 0;
            }
            if (message.name != null && message.hasOwnProperty("name"))
                object.name = message.name;
            if (message.money != null && message.hasOwnProperty("money"))
                object.money = message.money;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.watchAdTimeNow != null && message.hasOwnProperty("watchAdTimeNow"))
                object.watchAdTimeNow = message.watchAdTimeNow;
            return object;
        };

        /**
         * Converts this AuthConnectRes to JSON.
         * @function toJSON
         * @memberof PK5Client.AuthConnectRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        AuthConnectRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for AuthConnectRes
         * @function getTypeUrl
         * @memberof PK5Client.AuthConnectRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        AuthConnectRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.AuthConnectRes";
        };

        return AuthConnectRes;
    })();

    PK5Client.GetBetRange = (function() {

        /**
         * Properties of a GetBetRange.
         * @memberof PK5Client
         * @interface IGetBetRange
         */

        /**
         * Constructs a new GetBetRange.
         * @memberof PK5Client
         * @classdesc Represents a GetBetRange.
         * @implements IGetBetRange
         * @constructor
         * @param {PK5Client.IGetBetRange=} [properties] Properties to set
         */
        function GetBetRange(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new GetBetRange instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {PK5Client.IGetBetRange=} [properties] Properties to set
         * @returns {PK5Client.GetBetRange} GetBetRange instance
         */
        GetBetRange.create = function create(properties) {
            return new GetBetRange(properties);
        };

        /**
         * Encodes the specified GetBetRange message. Does not implicitly {@link PK5Client.GetBetRange.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {PK5Client.IGetBetRange} message GetBetRange message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetBetRange.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified GetBetRange message, length delimited. Does not implicitly {@link PK5Client.GetBetRange.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {PK5Client.IGetBetRange} message GetBetRange message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetBetRange.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetBetRange message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetBetRange} GetBetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetBetRange.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetBetRange();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetBetRange message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetBetRange} GetBetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetBetRange.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetBetRange message.
         * @function verify
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetBetRange.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a GetBetRange message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetBetRange} GetBetRange
         */
        GetBetRange.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetBetRange)
                return object;
            return new $root.PK5Client.GetBetRange();
        };

        /**
         * Creates a plain object from a GetBetRange message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {PK5Client.GetBetRange} message GetBetRange
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetBetRange.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this GetBetRange to JSON.
         * @function toJSON
         * @memberof PK5Client.GetBetRange
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetBetRange.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetBetRange
         * @function getTypeUrl
         * @memberof PK5Client.GetBetRange
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetBetRange.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetBetRange";
        };

        return GetBetRange;
    })();

    PK5Client.GetBetRangeRes = (function() {

        /**
         * Properties of a GetBetRangeRes.
         * @memberof PK5Client
         * @interface IGetBetRangeRes
         * @property {Array.<PK5Client.IBetRange>|null} [betranges] GetBetRangeRes betranges
         */

        /**
         * Constructs a new GetBetRangeRes.
         * @memberof PK5Client
         * @classdesc Represents a GetBetRangeRes.
         * @implements IGetBetRangeRes
         * @constructor
         * @param {PK5Client.IGetBetRangeRes=} [properties] Properties to set
         */
        function GetBetRangeRes(properties) {
            this.betranges = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetBetRangeRes betranges.
         * @member {Array.<PK5Client.IBetRange>} betranges
         * @memberof PK5Client.GetBetRangeRes
         * @instance
         */
        GetBetRangeRes.prototype.betranges = $util.emptyArray;

        /**
         * Creates a new GetBetRangeRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {PK5Client.IGetBetRangeRes=} [properties] Properties to set
         * @returns {PK5Client.GetBetRangeRes} GetBetRangeRes instance
         */
        GetBetRangeRes.create = function create(properties) {
            return new GetBetRangeRes(properties);
        };

        /**
         * Encodes the specified GetBetRangeRes message. Does not implicitly {@link PK5Client.GetBetRangeRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {PK5Client.IGetBetRangeRes} message GetBetRangeRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetBetRangeRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.betranges != null && message.betranges.length)
                for (var i = 0; i < message.betranges.length; ++i)
                    $root.PK5Client.BetRange.encode(message.betranges[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified GetBetRangeRes message, length delimited. Does not implicitly {@link PK5Client.GetBetRangeRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {PK5Client.IGetBetRangeRes} message GetBetRangeRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetBetRangeRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetBetRangeRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetBetRangeRes} GetBetRangeRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetBetRangeRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetBetRangeRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        if (!(message.betranges && message.betranges.length))
                            message.betranges = [];
                        message.betranges.push($root.PK5Client.BetRange.decode(reader, reader.uint32()));
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetBetRangeRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetBetRangeRes} GetBetRangeRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetBetRangeRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetBetRangeRes message.
         * @function verify
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetBetRangeRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.betranges != null && message.hasOwnProperty("betranges")) {
                if (!Array.isArray(message.betranges))
                    return "betranges: array expected";
                for (var i = 0; i < message.betranges.length; ++i) {
                    var error = $root.PK5Client.BetRange.verify(message.betranges[i]);
                    if (error)
                        return "betranges." + error;
                }
            }
            return null;
        };

        /**
         * Creates a GetBetRangeRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetBetRangeRes} GetBetRangeRes
         */
        GetBetRangeRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetBetRangeRes)
                return object;
            var message = new $root.PK5Client.GetBetRangeRes();
            if (object.betranges) {
                if (!Array.isArray(object.betranges))
                    throw TypeError(".PK5Client.GetBetRangeRes.betranges: array expected");
                message.betranges = [];
                for (var i = 0; i < object.betranges.length; ++i) {
                    if (typeof object.betranges[i] !== "object")
                        throw TypeError(".PK5Client.GetBetRangeRes.betranges: object expected");
                    message.betranges[i] = $root.PK5Client.BetRange.fromObject(object.betranges[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a GetBetRangeRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {PK5Client.GetBetRangeRes} message GetBetRangeRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetBetRangeRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.betranges = [];
            if (message.betranges && message.betranges.length) {
                object.betranges = [];
                for (var j = 0; j < message.betranges.length; ++j)
                    object.betranges[j] = $root.PK5Client.BetRange.toObject(message.betranges[j], options);
            }
            return object;
        };

        /**
         * Converts this GetBetRangeRes to JSON.
         * @function toJSON
         * @memberof PK5Client.GetBetRangeRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetBetRangeRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetBetRangeRes
         * @function getTypeUrl
         * @memberof PK5Client.GetBetRangeRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetBetRangeRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetBetRangeRes";
        };

        return GetBetRangeRes;
    })();

    PK5Client.BetRange = (function() {

        /**
         * Properties of a BetRange.
         * @memberof PK5Client
         * @interface IBetRange
         * @property {string|null} [betName] BetRange betName
         * @property {Array.<number>|null} [miniBetMoney] BetRange miniBetMoney
         * @property {number|null} [maxMachineNum] BetRange maxMachineNum
         * @property {number|null} [betIndex] BetRange betIndex
         * @property {number|null} [uid] BetRange uid
         */

        /**
         * Constructs a new BetRange.
         * @memberof PK5Client
         * @classdesc Represents a BetRange.
         * @implements IBetRange
         * @constructor
         * @param {PK5Client.IBetRange=} [properties] Properties to set
         */
        function BetRange(properties) {
            this.miniBetMoney = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * BetRange betName.
         * @member {string} betName
         * @memberof PK5Client.BetRange
         * @instance
         */
        BetRange.prototype.betName = "";

        /**
         * BetRange miniBetMoney.
         * @member {Array.<number>} miniBetMoney
         * @memberof PK5Client.BetRange
         * @instance
         */
        BetRange.prototype.miniBetMoney = $util.emptyArray;

        /**
         * BetRange maxMachineNum.
         * @member {number} maxMachineNum
         * @memberof PK5Client.BetRange
         * @instance
         */
        BetRange.prototype.maxMachineNum = 0;

        /**
         * BetRange betIndex.
         * @member {number} betIndex
         * @memberof PK5Client.BetRange
         * @instance
         */
        BetRange.prototype.betIndex = 0;

        /**
         * BetRange uid.
         * @member {number} uid
         * @memberof PK5Client.BetRange
         * @instance
         */
        BetRange.prototype.uid = 0;

        /**
         * Creates a new BetRange instance using the specified properties.
         * @function create
         * @memberof PK5Client.BetRange
         * @static
         * @param {PK5Client.IBetRange=} [properties] Properties to set
         * @returns {PK5Client.BetRange} BetRange instance
         */
        BetRange.create = function create(properties) {
            return new BetRange(properties);
        };

        /**
         * Encodes the specified BetRange message. Does not implicitly {@link PK5Client.BetRange.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.BetRange
         * @static
         * @param {PK5Client.IBetRange} message BetRange message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetRange.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.betName != null && Object.hasOwnProperty.call(message, "betName"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.betName);
            if (message.miniBetMoney != null && message.miniBetMoney.length) {
                writer.uint32(/* id 2, wireType 2 =*/18).fork();
                for (var i = 0; i < message.miniBetMoney.length; ++i)
                    writer.int32(message.miniBetMoney[i]);
                writer.ldelim();
            }
            if (message.maxMachineNum != null && Object.hasOwnProperty.call(message, "maxMachineNum"))
                writer.uint32(/* id 3, wireType 0 =*/24).sint32(message.maxMachineNum);
            if (message.betIndex != null && Object.hasOwnProperty.call(message, "betIndex"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.betIndex);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.uid);
            return writer;
        };

        /**
         * Encodes the specified BetRange message, length delimited. Does not implicitly {@link PK5Client.BetRange.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.BetRange
         * @static
         * @param {PK5Client.IBetRange} message BetRange message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetRange.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a BetRange message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.BetRange
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.BetRange} BetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetRange.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.BetRange();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.betName = reader.string();
                        break;
                    }
                case 2: {
                        if (!(message.miniBetMoney && message.miniBetMoney.length))
                            message.miniBetMoney = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.miniBetMoney.push(reader.int32());
                        } else
                            message.miniBetMoney.push(reader.int32());
                        break;
                    }
                case 3: {
                        message.maxMachineNum = reader.sint32();
                        break;
                    }
                case 4: {
                        message.betIndex = reader.int32();
                        break;
                    }
                case 5: {
                        message.uid = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a BetRange message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.BetRange
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.BetRange} BetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetRange.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a BetRange message.
         * @function verify
         * @memberof PK5Client.BetRange
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        BetRange.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.betName != null && message.hasOwnProperty("betName"))
                if (!$util.isString(message.betName))
                    return "betName: string expected";
            if (message.miniBetMoney != null && message.hasOwnProperty("miniBetMoney")) {
                if (!Array.isArray(message.miniBetMoney))
                    return "miniBetMoney: array expected";
                for (var i = 0; i < message.miniBetMoney.length; ++i)
                    if (!$util.isInteger(message.miniBetMoney[i]))
                        return "miniBetMoney: integer[] expected";
            }
            if (message.maxMachineNum != null && message.hasOwnProperty("maxMachineNum"))
                if (!$util.isInteger(message.maxMachineNum))
                    return "maxMachineNum: integer expected";
            if (message.betIndex != null && message.hasOwnProperty("betIndex"))
                if (!$util.isInteger(message.betIndex))
                    return "betIndex: integer expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            return null;
        };

        /**
         * Creates a BetRange message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.BetRange
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.BetRange} BetRange
         */
        BetRange.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.BetRange)
                return object;
            var message = new $root.PK5Client.BetRange();
            if (object.betName != null)
                message.betName = String(object.betName);
            if (object.miniBetMoney) {
                if (!Array.isArray(object.miniBetMoney))
                    throw TypeError(".PK5Client.BetRange.miniBetMoney: array expected");
                message.miniBetMoney = [];
                for (var i = 0; i < object.miniBetMoney.length; ++i)
                    message.miniBetMoney[i] = object.miniBetMoney[i] | 0;
            }
            if (object.maxMachineNum != null)
                message.maxMachineNum = object.maxMachineNum | 0;
            if (object.betIndex != null)
                message.betIndex = object.betIndex | 0;
            if (object.uid != null)
                message.uid = object.uid | 0;
            return message;
        };

        /**
         * Creates a plain object from a BetRange message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.BetRange
         * @static
         * @param {PK5Client.BetRange} message BetRange
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        BetRange.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.miniBetMoney = [];
            if (options.defaults) {
                object.betName = "";
                object.maxMachineNum = 0;
                object.betIndex = 0;
                object.uid = 0;
            }
            if (message.betName != null && message.hasOwnProperty("betName"))
                object.betName = message.betName;
            if (message.miniBetMoney && message.miniBetMoney.length) {
                object.miniBetMoney = [];
                for (var j = 0; j < message.miniBetMoney.length; ++j)
                    object.miniBetMoney[j] = message.miniBetMoney[j];
            }
            if (message.maxMachineNum != null && message.hasOwnProperty("maxMachineNum"))
                object.maxMachineNum = message.maxMachineNum;
            if (message.betIndex != null && message.hasOwnProperty("betIndex"))
                object.betIndex = message.betIndex;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            return object;
        };

        /**
         * Converts this BetRange to JSON.
         * @function toJSON
         * @memberof PK5Client.BetRange
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        BetRange.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for BetRange
         * @function getTypeUrl
         * @memberof PK5Client.BetRange
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        BetRange.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.BetRange";
        };

        return BetRange;
    })();

    PK5Client.SetBet = (function() {

        /**
         * Properties of a SetBet.
         * @memberof PK5Client
         * @interface ISetBet
         * @property {number|null} [betIndex] SetBet betIndex
         */

        /**
         * Constructs a new SetBet.
         * @memberof PK5Client
         * @classdesc Represents a SetBet.
         * @implements ISetBet
         * @constructor
         * @param {PK5Client.ISetBet=} [properties] Properties to set
         */
        function SetBet(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SetBet betIndex.
         * @member {number} betIndex
         * @memberof PK5Client.SetBet
         * @instance
         */
        SetBet.prototype.betIndex = 0;

        /**
         * Creates a new SetBet instance using the specified properties.
         * @function create
         * @memberof PK5Client.SetBet
         * @static
         * @param {PK5Client.ISetBet=} [properties] Properties to set
         * @returns {PK5Client.SetBet} SetBet instance
         */
        SetBet.create = function create(properties) {
            return new SetBet(properties);
        };

        /**
         * Encodes the specified SetBet message. Does not implicitly {@link PK5Client.SetBet.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.SetBet
         * @static
         * @param {PK5Client.ISetBet} message SetBet message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetBet.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.betIndex != null && Object.hasOwnProperty.call(message, "betIndex"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.betIndex);
            return writer;
        };

        /**
         * Encodes the specified SetBet message, length delimited. Does not implicitly {@link PK5Client.SetBet.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.SetBet
         * @static
         * @param {PK5Client.ISetBet} message SetBet message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetBet.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SetBet message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.SetBet
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.SetBet} SetBet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetBet.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.SetBet();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.betIndex = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SetBet message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.SetBet
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.SetBet} SetBet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetBet.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SetBet message.
         * @function verify
         * @memberof PK5Client.SetBet
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SetBet.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.betIndex != null && message.hasOwnProperty("betIndex"))
                if (!$util.isInteger(message.betIndex))
                    return "betIndex: integer expected";
            return null;
        };

        /**
         * Creates a SetBet message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.SetBet
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.SetBet} SetBet
         */
        SetBet.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.SetBet)
                return object;
            var message = new $root.PK5Client.SetBet();
            if (object.betIndex != null)
                message.betIndex = object.betIndex | 0;
            return message;
        };

        /**
         * Creates a plain object from a SetBet message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.SetBet
         * @static
         * @param {PK5Client.SetBet} message SetBet
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SetBet.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.betIndex = 0;
            if (message.betIndex != null && message.hasOwnProperty("betIndex"))
                object.betIndex = message.betIndex;
            return object;
        };

        /**
         * Converts this SetBet to JSON.
         * @function toJSON
         * @memberof PK5Client.SetBet
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SetBet.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for SetBet
         * @function getTypeUrl
         * @memberof PK5Client.SetBet
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        SetBet.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.SetBet";
        };

        return SetBet;
    })();

    PK5Client.SetBetRes = (function() {

        /**
         * Properties of a SetBetRes.
         * @memberof PK5Client
         * @interface ISetBetRes
         * @property {boolean|null} [result] SetBetRes result
         * @property {number|null} [uid] SetBetRes uid
         */

        /**
         * Constructs a new SetBetRes.
         * @memberof PK5Client
         * @classdesc Represents a SetBetRes.
         * @implements ISetBetRes
         * @constructor
         * @param {PK5Client.ISetBetRes=} [properties] Properties to set
         */
        function SetBetRes(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SetBetRes result.
         * @member {boolean} result
         * @memberof PK5Client.SetBetRes
         * @instance
         */
        SetBetRes.prototype.result = false;

        /**
         * SetBetRes uid.
         * @member {number} uid
         * @memberof PK5Client.SetBetRes
         * @instance
         */
        SetBetRes.prototype.uid = 0;

        /**
         * Creates a new SetBetRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {PK5Client.ISetBetRes=} [properties] Properties to set
         * @returns {PK5Client.SetBetRes} SetBetRes instance
         */
        SetBetRes.create = function create(properties) {
            return new SetBetRes(properties);
        };

        /**
         * Encodes the specified SetBetRes message. Does not implicitly {@link PK5Client.SetBetRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {PK5Client.ISetBetRes} message SetBetRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetBetRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.uid);
            return writer;
        };

        /**
         * Encodes the specified SetBetRes message, length delimited. Does not implicitly {@link PK5Client.SetBetRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {PK5Client.ISetBetRes} message SetBetRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetBetRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SetBetRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.SetBetRes} SetBetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetBetRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.SetBetRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                case 2: {
                        message.uid = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SetBetRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.SetBetRes} SetBetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetBetRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SetBetRes message.
         * @function verify
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SetBetRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            return null;
        };

        /**
         * Creates a SetBetRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.SetBetRes} SetBetRes
         */
        SetBetRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.SetBetRes)
                return object;
            var message = new $root.PK5Client.SetBetRes();
            if (object.result != null)
                message.result = Boolean(object.result);
            if (object.uid != null)
                message.uid = object.uid | 0;
            return message;
        };

        /**
         * Creates a plain object from a SetBetRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {PK5Client.SetBetRes} message SetBetRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SetBetRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.result = false;
                object.uid = 0;
            }
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            return object;
        };

        /**
         * Converts this SetBetRes to JSON.
         * @function toJSON
         * @memberof PK5Client.SetBetRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SetBetRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for SetBetRes
         * @function getTypeUrl
         * @memberof PK5Client.SetBetRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        SetBetRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.SetBetRes";
        };

        return SetBetRes;
    })();

    PK5Client.GetMachineList = (function() {

        /**
         * Properties of a GetMachineList.
         * @memberof PK5Client
         * @interface IGetMachineList
         * @property {number|null} [page] GetMachineList page
         * @property {number|null} [pageNum] GetMachineList pageNum
         */

        /**
         * Constructs a new GetMachineList.
         * @memberof PK5Client
         * @classdesc Represents a GetMachineList.
         * @implements IGetMachineList
         * @constructor
         * @param {PK5Client.IGetMachineList=} [properties] Properties to set
         */
        function GetMachineList(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetMachineList page.
         * @member {number} page
         * @memberof PK5Client.GetMachineList
         * @instance
         */
        GetMachineList.prototype.page = 0;

        /**
         * GetMachineList pageNum.
         * @member {number} pageNum
         * @memberof PK5Client.GetMachineList
         * @instance
         */
        GetMachineList.prototype.pageNum = 0;

        /**
         * Creates a new GetMachineList instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {PK5Client.IGetMachineList=} [properties] Properties to set
         * @returns {PK5Client.GetMachineList} GetMachineList instance
         */
        GetMachineList.create = function create(properties) {
            return new GetMachineList(properties);
        };

        /**
         * Encodes the specified GetMachineList message. Does not implicitly {@link PK5Client.GetMachineList.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {PK5Client.IGetMachineList} message GetMachineList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineList.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.page != null && Object.hasOwnProperty.call(message, "page"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.page);
            if (message.pageNum != null && Object.hasOwnProperty.call(message, "pageNum"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.pageNum);
            return writer;
        };

        /**
         * Encodes the specified GetMachineList message, length delimited. Does not implicitly {@link PK5Client.GetMachineList.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {PK5Client.IGetMachineList} message GetMachineList message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineList.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetMachineList message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetMachineList} GetMachineList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineList.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetMachineList();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.page = reader.int32();
                        break;
                    }
                case 2: {
                        message.pageNum = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetMachineList message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetMachineList} GetMachineList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineList.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetMachineList message.
         * @function verify
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetMachineList.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.page != null && message.hasOwnProperty("page"))
                if (!$util.isInteger(message.page))
                    return "page: integer expected";
            if (message.pageNum != null && message.hasOwnProperty("pageNum"))
                if (!$util.isInteger(message.pageNum))
                    return "pageNum: integer expected";
            return null;
        };

        /**
         * Creates a GetMachineList message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetMachineList} GetMachineList
         */
        GetMachineList.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetMachineList)
                return object;
            var message = new $root.PK5Client.GetMachineList();
            if (object.page != null)
                message.page = object.page | 0;
            if (object.pageNum != null)
                message.pageNum = object.pageNum | 0;
            return message;
        };

        /**
         * Creates a plain object from a GetMachineList message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {PK5Client.GetMachineList} message GetMachineList
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetMachineList.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.page = 0;
                object.pageNum = 0;
            }
            if (message.page != null && message.hasOwnProperty("page"))
                object.page = message.page;
            if (message.pageNum != null && message.hasOwnProperty("pageNum"))
                object.pageNum = message.pageNum;
            return object;
        };

        /**
         * Converts this GetMachineList to JSON.
         * @function toJSON
         * @memberof PK5Client.GetMachineList
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetMachineList.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetMachineList
         * @function getTypeUrl
         * @memberof PK5Client.GetMachineList
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetMachineList.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetMachineList";
        };

        return GetMachineList;
    })();

    PK5Client.GetMachineListRes = (function() {

        /**
         * Properties of a GetMachineListRes.
         * @memberof PK5Client
         * @interface IGetMachineListRes
         * @property {Array.<string>|null} [hasPeople] GetMachineListRes hasPeople
         * @property {Array.<string>|null} [machineIds] GetMachineListRes machineIds
         * @property {Array.<string>|null} [isLock] GetMachineListRes isLock
         */

        /**
         * Constructs a new GetMachineListRes.
         * @memberof PK5Client
         * @classdesc Represents a GetMachineListRes.
         * @implements IGetMachineListRes
         * @constructor
         * @param {PK5Client.IGetMachineListRes=} [properties] Properties to set
         */
        function GetMachineListRes(properties) {
            this.hasPeople = [];
            this.machineIds = [];
            this.isLock = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetMachineListRes hasPeople.
         * @member {Array.<string>} hasPeople
         * @memberof PK5Client.GetMachineListRes
         * @instance
         */
        GetMachineListRes.prototype.hasPeople = $util.emptyArray;

        /**
         * GetMachineListRes machineIds.
         * @member {Array.<string>} machineIds
         * @memberof PK5Client.GetMachineListRes
         * @instance
         */
        GetMachineListRes.prototype.machineIds = $util.emptyArray;

        /**
         * GetMachineListRes isLock.
         * @member {Array.<string>} isLock
         * @memberof PK5Client.GetMachineListRes
         * @instance
         */
        GetMachineListRes.prototype.isLock = $util.emptyArray;

        /**
         * Creates a new GetMachineListRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {PK5Client.IGetMachineListRes=} [properties] Properties to set
         * @returns {PK5Client.GetMachineListRes} GetMachineListRes instance
         */
        GetMachineListRes.create = function create(properties) {
            return new GetMachineListRes(properties);
        };

        /**
         * Encodes the specified GetMachineListRes message. Does not implicitly {@link PK5Client.GetMachineListRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {PK5Client.IGetMachineListRes} message GetMachineListRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineListRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.hasPeople != null && message.hasPeople.length)
                for (var i = 0; i < message.hasPeople.length; ++i)
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.hasPeople[i]);
            if (message.machineIds != null && message.machineIds.length)
                for (var i = 0; i < message.machineIds.length; ++i)
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.machineIds[i]);
            if (message.isLock != null && message.isLock.length)
                for (var i = 0; i < message.isLock.length; ++i)
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.isLock[i]);
            return writer;
        };

        /**
         * Encodes the specified GetMachineListRes message, length delimited. Does not implicitly {@link PK5Client.GetMachineListRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {PK5Client.IGetMachineListRes} message GetMachineListRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineListRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetMachineListRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetMachineListRes} GetMachineListRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineListRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetMachineListRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        if (!(message.hasPeople && message.hasPeople.length))
                            message.hasPeople = [];
                        message.hasPeople.push(reader.string());
                        break;
                    }
                case 2: {
                        if (!(message.machineIds && message.machineIds.length))
                            message.machineIds = [];
                        message.machineIds.push(reader.string());
                        break;
                    }
                case 3: {
                        if (!(message.isLock && message.isLock.length))
                            message.isLock = [];
                        message.isLock.push(reader.string());
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetMachineListRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetMachineListRes} GetMachineListRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineListRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetMachineListRes message.
         * @function verify
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetMachineListRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.hasPeople != null && message.hasOwnProperty("hasPeople")) {
                if (!Array.isArray(message.hasPeople))
                    return "hasPeople: array expected";
                for (var i = 0; i < message.hasPeople.length; ++i)
                    if (!$util.isString(message.hasPeople[i]))
                        return "hasPeople: string[] expected";
            }
            if (message.machineIds != null && message.hasOwnProperty("machineIds")) {
                if (!Array.isArray(message.machineIds))
                    return "machineIds: array expected";
                for (var i = 0; i < message.machineIds.length; ++i)
                    if (!$util.isString(message.machineIds[i]))
                        return "machineIds: string[] expected";
            }
            if (message.isLock != null && message.hasOwnProperty("isLock")) {
                if (!Array.isArray(message.isLock))
                    return "isLock: array expected";
                for (var i = 0; i < message.isLock.length; ++i)
                    if (!$util.isString(message.isLock[i]))
                        return "isLock: string[] expected";
            }
            return null;
        };

        /**
         * Creates a GetMachineListRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetMachineListRes} GetMachineListRes
         */
        GetMachineListRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetMachineListRes)
                return object;
            var message = new $root.PK5Client.GetMachineListRes();
            if (object.hasPeople) {
                if (!Array.isArray(object.hasPeople))
                    throw TypeError(".PK5Client.GetMachineListRes.hasPeople: array expected");
                message.hasPeople = [];
                for (var i = 0; i < object.hasPeople.length; ++i)
                    message.hasPeople[i] = String(object.hasPeople[i]);
            }
            if (object.machineIds) {
                if (!Array.isArray(object.machineIds))
                    throw TypeError(".PK5Client.GetMachineListRes.machineIds: array expected");
                message.machineIds = [];
                for (var i = 0; i < object.machineIds.length; ++i)
                    message.machineIds[i] = String(object.machineIds[i]);
            }
            if (object.isLock) {
                if (!Array.isArray(object.isLock))
                    throw TypeError(".PK5Client.GetMachineListRes.isLock: array expected");
                message.isLock = [];
                for (var i = 0; i < object.isLock.length; ++i)
                    message.isLock[i] = String(object.isLock[i]);
            }
            return message;
        };

        /**
         * Creates a plain object from a GetMachineListRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {PK5Client.GetMachineListRes} message GetMachineListRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetMachineListRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.hasPeople = [];
                object.machineIds = [];
                object.isLock = [];
            }
            if (message.hasPeople && message.hasPeople.length) {
                object.hasPeople = [];
                for (var j = 0; j < message.hasPeople.length; ++j)
                    object.hasPeople[j] = message.hasPeople[j];
            }
            if (message.machineIds && message.machineIds.length) {
                object.machineIds = [];
                for (var j = 0; j < message.machineIds.length; ++j)
                    object.machineIds[j] = message.machineIds[j];
            }
            if (message.isLock && message.isLock.length) {
                object.isLock = [];
                for (var j = 0; j < message.isLock.length; ++j)
                    object.isLock[j] = message.isLock[j];
            }
            return object;
        };

        /**
         * Converts this GetMachineListRes to JSON.
         * @function toJSON
         * @memberof PK5Client.GetMachineListRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetMachineListRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetMachineListRes
         * @function getTypeUrl
         * @memberof PK5Client.GetMachineListRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetMachineListRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetMachineListRes";
        };

        return GetMachineListRes;
    })();

    PK5Client.GetMachineDetial = (function() {

        /**
         * Properties of a GetMachineDetial.
         * @memberof PK5Client
         * @interface IGetMachineDetial
         * @property {string|null} [machindId] GetMachineDetial machindId
         */

        /**
         * Constructs a new GetMachineDetial.
         * @memberof PK5Client
         * @classdesc Represents a GetMachineDetial.
         * @implements IGetMachineDetial
         * @constructor
         * @param {PK5Client.IGetMachineDetial=} [properties] Properties to set
         */
        function GetMachineDetial(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetMachineDetial machindId.
         * @member {string} machindId
         * @memberof PK5Client.GetMachineDetial
         * @instance
         */
        GetMachineDetial.prototype.machindId = "";

        /**
         * Creates a new GetMachineDetial instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {PK5Client.IGetMachineDetial=} [properties] Properties to set
         * @returns {PK5Client.GetMachineDetial} GetMachineDetial instance
         */
        GetMachineDetial.create = function create(properties) {
            return new GetMachineDetial(properties);
        };

        /**
         * Encodes the specified GetMachineDetial message. Does not implicitly {@link PK5Client.GetMachineDetial.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {PK5Client.IGetMachineDetial} message GetMachineDetial message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineDetial.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.machindId != null && Object.hasOwnProperty.call(message, "machindId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.machindId);
            return writer;
        };

        /**
         * Encodes the specified GetMachineDetial message, length delimited. Does not implicitly {@link PK5Client.GetMachineDetial.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {PK5Client.IGetMachineDetial} message GetMachineDetial message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineDetial.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetMachineDetial message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetMachineDetial} GetMachineDetial
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineDetial.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetMachineDetial();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.machindId = reader.string();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetMachineDetial message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetMachineDetial} GetMachineDetial
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineDetial.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetMachineDetial message.
         * @function verify
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetMachineDetial.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.machindId != null && message.hasOwnProperty("machindId"))
                if (!$util.isString(message.machindId))
                    return "machindId: string expected";
            return null;
        };

        /**
         * Creates a GetMachineDetial message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetMachineDetial} GetMachineDetial
         */
        GetMachineDetial.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetMachineDetial)
                return object;
            var message = new $root.PK5Client.GetMachineDetial();
            if (object.machindId != null)
                message.machindId = String(object.machindId);
            return message;
        };

        /**
         * Creates a plain object from a GetMachineDetial message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {PK5Client.GetMachineDetial} message GetMachineDetial
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetMachineDetial.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.machindId = "";
            if (message.machindId != null && message.hasOwnProperty("machindId"))
                object.machindId = message.machindId;
            return object;
        };

        /**
         * Converts this GetMachineDetial to JSON.
         * @function toJSON
         * @memberof PK5Client.GetMachineDetial
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetMachineDetial.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetMachineDetial
         * @function getTypeUrl
         * @memberof PK5Client.GetMachineDetial
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetMachineDetial.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetMachineDetial";
        };

        return GetMachineDetial;
    })();

    PK5Client.GetMachineDetialRes = (function() {

        /**
         * Properties of a GetMachineDetialRes.
         * @memberof PK5Client
         * @interface IGetMachineDetialRes
         * @property {string|null} [machindId] GetMachineDetialRes machindId
         * @property {Array.<string>|null} [winList] GetMachineDetialRes winList
         * @property {Array.<number>|null} [jpList] GetMachineDetialRes jpList
         * @property {number|null} [totalWinRate] GetMachineDetialRes totalWinRate
         * @property {number|null} [toadyWinRate] GetMachineDetialRes toadyWinRate
         */

        /**
         * Constructs a new GetMachineDetialRes.
         * @memberof PK5Client
         * @classdesc Represents a GetMachineDetialRes.
         * @implements IGetMachineDetialRes
         * @constructor
         * @param {PK5Client.IGetMachineDetialRes=} [properties] Properties to set
         */
        function GetMachineDetialRes(properties) {
            this.winList = [];
            this.jpList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetMachineDetialRes machindId.
         * @member {string} machindId
         * @memberof PK5Client.GetMachineDetialRes
         * @instance
         */
        GetMachineDetialRes.prototype.machindId = "";

        /**
         * GetMachineDetialRes winList.
         * @member {Array.<string>} winList
         * @memberof PK5Client.GetMachineDetialRes
         * @instance
         */
        GetMachineDetialRes.prototype.winList = $util.emptyArray;

        /**
         * GetMachineDetialRes jpList.
         * @member {Array.<number>} jpList
         * @memberof PK5Client.GetMachineDetialRes
         * @instance
         */
        GetMachineDetialRes.prototype.jpList = $util.emptyArray;

        /**
         * GetMachineDetialRes totalWinRate.
         * @member {number} totalWinRate
         * @memberof PK5Client.GetMachineDetialRes
         * @instance
         */
        GetMachineDetialRes.prototype.totalWinRate = 0;

        /**
         * GetMachineDetialRes toadyWinRate.
         * @member {number} toadyWinRate
         * @memberof PK5Client.GetMachineDetialRes
         * @instance
         */
        GetMachineDetialRes.prototype.toadyWinRate = 0;

        /**
         * Creates a new GetMachineDetialRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {PK5Client.IGetMachineDetialRes=} [properties] Properties to set
         * @returns {PK5Client.GetMachineDetialRes} GetMachineDetialRes instance
         */
        GetMachineDetialRes.create = function create(properties) {
            return new GetMachineDetialRes(properties);
        };

        /**
         * Encodes the specified GetMachineDetialRes message. Does not implicitly {@link PK5Client.GetMachineDetialRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {PK5Client.IGetMachineDetialRes} message GetMachineDetialRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineDetialRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.machindId != null && Object.hasOwnProperty.call(message, "machindId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.machindId);
            if (message.winList != null && message.winList.length)
                for (var i = 0; i < message.winList.length; ++i)
                    writer.uint32(/* id 2, wireType 2 =*/18).string(message.winList[i]);
            if (message.jpList != null && message.jpList.length) {
                writer.uint32(/* id 3, wireType 2 =*/26).fork();
                for (var i = 0; i < message.jpList.length; ++i)
                    writer.double(message.jpList[i]);
                writer.ldelim();
            }
            if (message.totalWinRate != null && Object.hasOwnProperty.call(message, "totalWinRate"))
                writer.uint32(/* id 5, wireType 1 =*/41).double(message.totalWinRate);
            if (message.toadyWinRate != null && Object.hasOwnProperty.call(message, "toadyWinRate"))
                writer.uint32(/* id 6, wireType 1 =*/49).double(message.toadyWinRate);
            return writer;
        };

        /**
         * Encodes the specified GetMachineDetialRes message, length delimited. Does not implicitly {@link PK5Client.GetMachineDetialRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {PK5Client.IGetMachineDetialRes} message GetMachineDetialRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetMachineDetialRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetMachineDetialRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetMachineDetialRes} GetMachineDetialRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineDetialRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetMachineDetialRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.machindId = reader.string();
                        break;
                    }
                case 2: {
                        if (!(message.winList && message.winList.length))
                            message.winList = [];
                        message.winList.push(reader.string());
                        break;
                    }
                case 3: {
                        if (!(message.jpList && message.jpList.length))
                            message.jpList = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.jpList.push(reader.double());
                        } else
                            message.jpList.push(reader.double());
                        break;
                    }
                case 5: {
                        message.totalWinRate = reader.double();
                        break;
                    }
                case 6: {
                        message.toadyWinRate = reader.double();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetMachineDetialRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetMachineDetialRes} GetMachineDetialRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetMachineDetialRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetMachineDetialRes message.
         * @function verify
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetMachineDetialRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.machindId != null && message.hasOwnProperty("machindId"))
                if (!$util.isString(message.machindId))
                    return "machindId: string expected";
            if (message.winList != null && message.hasOwnProperty("winList")) {
                if (!Array.isArray(message.winList))
                    return "winList: array expected";
                for (var i = 0; i < message.winList.length; ++i)
                    if (!$util.isString(message.winList[i]))
                        return "winList: string[] expected";
            }
            if (message.jpList != null && message.hasOwnProperty("jpList")) {
                if (!Array.isArray(message.jpList))
                    return "jpList: array expected";
                for (var i = 0; i < message.jpList.length; ++i)
                    if (typeof message.jpList[i] !== "number")
                        return "jpList: number[] expected";
            }
            if (message.totalWinRate != null && message.hasOwnProperty("totalWinRate"))
                if (typeof message.totalWinRate !== "number")
                    return "totalWinRate: number expected";
            if (message.toadyWinRate != null && message.hasOwnProperty("toadyWinRate"))
                if (typeof message.toadyWinRate !== "number")
                    return "toadyWinRate: number expected";
            return null;
        };

        /**
         * Creates a GetMachineDetialRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetMachineDetialRes} GetMachineDetialRes
         */
        GetMachineDetialRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetMachineDetialRes)
                return object;
            var message = new $root.PK5Client.GetMachineDetialRes();
            if (object.machindId != null)
                message.machindId = String(object.machindId);
            if (object.winList) {
                if (!Array.isArray(object.winList))
                    throw TypeError(".PK5Client.GetMachineDetialRes.winList: array expected");
                message.winList = [];
                for (var i = 0; i < object.winList.length; ++i)
                    message.winList[i] = String(object.winList[i]);
            }
            if (object.jpList) {
                if (!Array.isArray(object.jpList))
                    throw TypeError(".PK5Client.GetMachineDetialRes.jpList: array expected");
                message.jpList = [];
                for (var i = 0; i < object.jpList.length; ++i)
                    message.jpList[i] = Number(object.jpList[i]);
            }
            if (object.totalWinRate != null)
                message.totalWinRate = Number(object.totalWinRate);
            if (object.toadyWinRate != null)
                message.toadyWinRate = Number(object.toadyWinRate);
            return message;
        };

        /**
         * Creates a plain object from a GetMachineDetialRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {PK5Client.GetMachineDetialRes} message GetMachineDetialRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetMachineDetialRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.winList = [];
                object.jpList = [];
            }
            if (options.defaults) {
                object.machindId = "";
                object.totalWinRate = 0;
                object.toadyWinRate = 0;
            }
            if (message.machindId != null && message.hasOwnProperty("machindId"))
                object.machindId = message.machindId;
            if (message.winList && message.winList.length) {
                object.winList = [];
                for (var j = 0; j < message.winList.length; ++j)
                    object.winList[j] = message.winList[j];
            }
            if (message.jpList && message.jpList.length) {
                object.jpList = [];
                for (var j = 0; j < message.jpList.length; ++j)
                    object.jpList[j] = options.json && !isFinite(message.jpList[j]) ? String(message.jpList[j]) : message.jpList[j];
            }
            if (message.totalWinRate != null && message.hasOwnProperty("totalWinRate"))
                object.totalWinRate = options.json && !isFinite(message.totalWinRate) ? String(message.totalWinRate) : message.totalWinRate;
            if (message.toadyWinRate != null && message.hasOwnProperty("toadyWinRate"))
                object.toadyWinRate = options.json && !isFinite(message.toadyWinRate) ? String(message.toadyWinRate) : message.toadyWinRate;
            return object;
        };

        /**
         * Converts this GetMachineDetialRes to JSON.
         * @function toJSON
         * @memberof PK5Client.GetMachineDetialRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetMachineDetialRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetMachineDetialRes
         * @function getTypeUrl
         * @memberof PK5Client.GetMachineDetialRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetMachineDetialRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetMachineDetialRes";
        };

        return GetMachineDetialRes;
    })();

    PK5Client.IntoGame = (function() {

        /**
         * Properties of an IntoGame.
         * @memberof PK5Client
         * @interface IIntoGame
         * @property {string|null} [machindId] IntoGame machindId
         */

        /**
         * Constructs a new IntoGame.
         * @memberof PK5Client
         * @classdesc Represents an IntoGame.
         * @implements IIntoGame
         * @constructor
         * @param {PK5Client.IIntoGame=} [properties] Properties to set
         */
        function IntoGame(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * IntoGame machindId.
         * @member {string} machindId
         * @memberof PK5Client.IntoGame
         * @instance
         */
        IntoGame.prototype.machindId = "";

        /**
         * Creates a new IntoGame instance using the specified properties.
         * @function create
         * @memberof PK5Client.IntoGame
         * @static
         * @param {PK5Client.IIntoGame=} [properties] Properties to set
         * @returns {PK5Client.IntoGame} IntoGame instance
         */
        IntoGame.create = function create(properties) {
            return new IntoGame(properties);
        };

        /**
         * Encodes the specified IntoGame message. Does not implicitly {@link PK5Client.IntoGame.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.IntoGame
         * @static
         * @param {PK5Client.IIntoGame} message IntoGame message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        IntoGame.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.machindId != null && Object.hasOwnProperty.call(message, "machindId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.machindId);
            return writer;
        };

        /**
         * Encodes the specified IntoGame message, length delimited. Does not implicitly {@link PK5Client.IntoGame.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.IntoGame
         * @static
         * @param {PK5Client.IIntoGame} message IntoGame message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        IntoGame.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an IntoGame message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.IntoGame
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.IntoGame} IntoGame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        IntoGame.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.IntoGame();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.machindId = reader.string();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an IntoGame message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.IntoGame
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.IntoGame} IntoGame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        IntoGame.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an IntoGame message.
         * @function verify
         * @memberof PK5Client.IntoGame
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        IntoGame.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.machindId != null && message.hasOwnProperty("machindId"))
                if (!$util.isString(message.machindId))
                    return "machindId: string expected";
            return null;
        };

        /**
         * Creates an IntoGame message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.IntoGame
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.IntoGame} IntoGame
         */
        IntoGame.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.IntoGame)
                return object;
            var message = new $root.PK5Client.IntoGame();
            if (object.machindId != null)
                message.machindId = String(object.machindId);
            return message;
        };

        /**
         * Creates a plain object from an IntoGame message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.IntoGame
         * @static
         * @param {PK5Client.IntoGame} message IntoGame
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        IntoGame.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.machindId = "";
            if (message.machindId != null && message.hasOwnProperty("machindId"))
                object.machindId = message.machindId;
            return object;
        };

        /**
         * Converts this IntoGame to JSON.
         * @function toJSON
         * @memberof PK5Client.IntoGame
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        IntoGame.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for IntoGame
         * @function getTypeUrl
         * @memberof PK5Client.IntoGame
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        IntoGame.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.IntoGame";
        };

        return IntoGame;
    })();

    PK5Client.IntoGameRes = (function() {

        /**
         * Properties of an IntoGameRes.
         * @memberof PK5Client
         * @interface IIntoGameRes
         * @property {boolean|null} [result] IntoGameRes result
         */

        /**
         * Constructs a new IntoGameRes.
         * @memberof PK5Client
         * @classdesc Represents an IntoGameRes.
         * @implements IIntoGameRes
         * @constructor
         * @param {PK5Client.IIntoGameRes=} [properties] Properties to set
         */
        function IntoGameRes(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * IntoGameRes result.
         * @member {boolean} result
         * @memberof PK5Client.IntoGameRes
         * @instance
         */
        IntoGameRes.prototype.result = false;

        /**
         * Creates a new IntoGameRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {PK5Client.IIntoGameRes=} [properties] Properties to set
         * @returns {PK5Client.IntoGameRes} IntoGameRes instance
         */
        IntoGameRes.create = function create(properties) {
            return new IntoGameRes(properties);
        };

        /**
         * Encodes the specified IntoGameRes message. Does not implicitly {@link PK5Client.IntoGameRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {PK5Client.IIntoGameRes} message IntoGameRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        IntoGameRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            return writer;
        };

        /**
         * Encodes the specified IntoGameRes message, length delimited. Does not implicitly {@link PK5Client.IntoGameRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {PK5Client.IIntoGameRes} message IntoGameRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        IntoGameRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an IntoGameRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.IntoGameRes} IntoGameRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        IntoGameRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.IntoGameRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an IntoGameRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.IntoGameRes} IntoGameRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        IntoGameRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an IntoGameRes message.
         * @function verify
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        IntoGameRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            return null;
        };

        /**
         * Creates an IntoGameRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.IntoGameRes} IntoGameRes
         */
        IntoGameRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.IntoGameRes)
                return object;
            var message = new $root.PK5Client.IntoGameRes();
            if (object.result != null)
                message.result = Boolean(object.result);
            return message;
        };

        /**
         * Creates a plain object from an IntoGameRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {PK5Client.IntoGameRes} message IntoGameRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        IntoGameRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.result = false;
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            return object;
        };

        /**
         * Converts this IntoGameRes to JSON.
         * @function toJSON
         * @memberof PK5Client.IntoGameRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        IntoGameRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for IntoGameRes
         * @function getTypeUrl
         * @memberof PK5Client.IntoGameRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        IntoGameRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.IntoGameRes";
        };

        return IntoGameRes;
    })();

    PK5Client.Bet = (function() {

        /**
         * Properties of a Bet.
         * @memberof PK5Client
         * @interface IBet
         * @property {number|null} [betNum] Bet betNum
         * @property {number|null} [f] Bet f
         */

        /**
         * Constructs a new Bet.
         * @memberof PK5Client
         * @classdesc Represents a Bet.
         * @implements IBet
         * @constructor
         * @param {PK5Client.IBet=} [properties] Properties to set
         */
        function Bet(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Bet betNum.
         * @member {number} betNum
         * @memberof PK5Client.Bet
         * @instance
         */
        Bet.prototype.betNum = 0;

        /**
         * Bet f.
         * @member {number} f
         * @memberof PK5Client.Bet
         * @instance
         */
        Bet.prototype.f = 0;

        /**
         * Creates a new Bet instance using the specified properties.
         * @function create
         * @memberof PK5Client.Bet
         * @static
         * @param {PK5Client.IBet=} [properties] Properties to set
         * @returns {PK5Client.Bet} Bet instance
         */
        Bet.create = function create(properties) {
            return new Bet(properties);
        };

        /**
         * Encodes the specified Bet message. Does not implicitly {@link PK5Client.Bet.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.Bet
         * @static
         * @param {PK5Client.IBet} message Bet message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bet.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.betNum != null && Object.hasOwnProperty.call(message, "betNum"))
                writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.betNum);
            if (message.f != null && Object.hasOwnProperty.call(message, "f"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.f);
            return writer;
        };

        /**
         * Encodes the specified Bet message, length delimited. Does not implicitly {@link PK5Client.Bet.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.Bet
         * @static
         * @param {PK5Client.IBet} message Bet message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Bet.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Bet message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.Bet
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.Bet} Bet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bet.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.Bet();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.betNum = reader.sint32();
                        break;
                    }
                case 2: {
                        message.f = reader.sint32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Bet message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.Bet
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.Bet} Bet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Bet.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Bet message.
         * @function verify
         * @memberof PK5Client.Bet
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Bet.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.betNum != null && message.hasOwnProperty("betNum"))
                if (!$util.isInteger(message.betNum))
                    return "betNum: integer expected";
            if (message.f != null && message.hasOwnProperty("f"))
                if (!$util.isInteger(message.f))
                    return "f: integer expected";
            return null;
        };

        /**
         * Creates a Bet message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.Bet
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.Bet} Bet
         */
        Bet.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.Bet)
                return object;
            var message = new $root.PK5Client.Bet();
            if (object.betNum != null)
                message.betNum = object.betNum | 0;
            if (object.f != null)
                message.f = object.f | 0;
            return message;
        };

        /**
         * Creates a plain object from a Bet message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.Bet
         * @static
         * @param {PK5Client.Bet} message Bet
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Bet.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.betNum = 0;
                object.f = 0;
            }
            if (message.betNum != null && message.hasOwnProperty("betNum"))
                object.betNum = message.betNum;
            if (message.f != null && message.hasOwnProperty("f"))
                object.f = message.f;
            return object;
        };

        /**
         * Converts this Bet to JSON.
         * @function toJSON
         * @memberof PK5Client.Bet
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Bet.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Bet
         * @function getTypeUrl
         * @memberof PK5Client.Bet
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Bet.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.Bet";
        };

        return Bet;
    })();

    PK5Client.BetRes = (function() {

        /**
         * Properties of a BetRes.
         * @memberof PK5Client
         * @interface IBetRes
         * @property {boolean|null} [result] BetRes result
         * @property {Array.<number>|null} [pockers] BetRes pockers
         * @property {PK5Client.IWinResult|null} [winResult] BetRes winResult
         * @property {number|null} [uid] BetRes uid
         * @property {Array.<PK5Client.IPredict>|null} [predict] BetRes predict
         * @property {Array.<number>|null} [jpList] BetRes jpList
         * @property {number|null} [nowGold] BetRes nowGold
         * @property {Array.<number>|null} [DoubleBetList] BetRes DoubleBetList
         * @property {Array.<number>|null} [fakeJpList] BetRes fakeJpList
         */

        /**
         * Constructs a new BetRes.
         * @memberof PK5Client
         * @classdesc Represents a BetRes.
         * @implements IBetRes
         * @constructor
         * @param {PK5Client.IBetRes=} [properties] Properties to set
         */
        function BetRes(properties) {
            this.pockers = [];
            this.predict = [];
            this.jpList = [];
            this.DoubleBetList = [];
            this.fakeJpList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * BetRes result.
         * @member {boolean} result
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.result = false;

        /**
         * BetRes pockers.
         * @member {Array.<number>} pockers
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.pockers = $util.emptyArray;

        /**
         * BetRes winResult.
         * @member {PK5Client.IWinResult|null|undefined} winResult
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.winResult = null;

        /**
         * BetRes uid.
         * @member {number} uid
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.uid = 0;

        /**
         * BetRes predict.
         * @member {Array.<PK5Client.IPredict>} predict
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.predict = $util.emptyArray;

        /**
         * BetRes jpList.
         * @member {Array.<number>} jpList
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.jpList = $util.emptyArray;

        /**
         * BetRes nowGold.
         * @member {number} nowGold
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.nowGold = 0;

        /**
         * BetRes DoubleBetList.
         * @member {Array.<number>} DoubleBetList
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.DoubleBetList = $util.emptyArray;

        /**
         * BetRes fakeJpList.
         * @member {Array.<number>} fakeJpList
         * @memberof PK5Client.BetRes
         * @instance
         */
        BetRes.prototype.fakeJpList = $util.emptyArray;

        /**
         * Creates a new BetRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.BetRes
         * @static
         * @param {PK5Client.IBetRes=} [properties] Properties to set
         * @returns {PK5Client.BetRes} BetRes instance
         */
        BetRes.create = function create(properties) {
            return new BetRes(properties);
        };

        /**
         * Encodes the specified BetRes message. Does not implicitly {@link PK5Client.BetRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.BetRes
         * @static
         * @param {PK5Client.IBetRes} message BetRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            if (message.pockers != null && message.pockers.length) {
                writer.uint32(/* id 2, wireType 2 =*/18).fork();
                for (var i = 0; i < message.pockers.length; ++i)
                    writer.sint32(message.pockers[i]);
                writer.ldelim();
            }
            if (message.winResult != null && Object.hasOwnProperty.call(message, "winResult"))
                $root.PK5Client.WinResult.encode(message.winResult, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.uid);
            if (message.predict != null && message.predict.length)
                for (var i = 0; i < message.predict.length; ++i)
                    $root.PK5Client.Predict.encode(message.predict[i], writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            if (message.jpList != null && message.jpList.length) {
                writer.uint32(/* id 6, wireType 2 =*/50).fork();
                for (var i = 0; i < message.jpList.length; ++i)
                    writer.double(message.jpList[i]);
                writer.ldelim();
            }
            if (message.nowGold != null && Object.hasOwnProperty.call(message, "nowGold"))
                writer.uint32(/* id 7, wireType 0 =*/56).int32(message.nowGold);
            if (message.DoubleBetList != null && message.DoubleBetList.length) {
                writer.uint32(/* id 8, wireType 2 =*/66).fork();
                for (var i = 0; i < message.DoubleBetList.length; ++i)
                    writer.int32(message.DoubleBetList[i]);
                writer.ldelim();
            }
            if (message.fakeJpList != null && message.fakeJpList.length) {
                writer.uint32(/* id 9, wireType 2 =*/74).fork();
                for (var i = 0; i < message.fakeJpList.length; ++i)
                    writer.double(message.fakeJpList[i]);
                writer.ldelim();
            }
            return writer;
        };

        /**
         * Encodes the specified BetRes message, length delimited. Does not implicitly {@link PK5Client.BetRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.BetRes
         * @static
         * @param {PK5Client.IBetRes} message BetRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a BetRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.BetRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.BetRes} BetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.BetRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                case 2: {
                        if (!(message.pockers && message.pockers.length))
                            message.pockers = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.pockers.push(reader.sint32());
                        } else
                            message.pockers.push(reader.sint32());
                        break;
                    }
                case 3: {
                        message.winResult = $root.PK5Client.WinResult.decode(reader, reader.uint32());
                        break;
                    }
                case 4: {
                        message.uid = reader.int32();
                        break;
                    }
                case 5: {
                        if (!(message.predict && message.predict.length))
                            message.predict = [];
                        message.predict.push($root.PK5Client.Predict.decode(reader, reader.uint32()));
                        break;
                    }
                case 6: {
                        if (!(message.jpList && message.jpList.length))
                            message.jpList = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.jpList.push(reader.double());
                        } else
                            message.jpList.push(reader.double());
                        break;
                    }
                case 7: {
                        message.nowGold = reader.int32();
                        break;
                    }
                case 8: {
                        if (!(message.DoubleBetList && message.DoubleBetList.length))
                            message.DoubleBetList = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.DoubleBetList.push(reader.int32());
                        } else
                            message.DoubleBetList.push(reader.int32());
                        break;
                    }
                case 9: {
                        if (!(message.fakeJpList && message.fakeJpList.length))
                            message.fakeJpList = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.fakeJpList.push(reader.double());
                        } else
                            message.fakeJpList.push(reader.double());
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a BetRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.BetRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.BetRes} BetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a BetRes message.
         * @function verify
         * @memberof PK5Client.BetRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        BetRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            if (message.pockers != null && message.hasOwnProperty("pockers")) {
                if (!Array.isArray(message.pockers))
                    return "pockers: array expected";
                for (var i = 0; i < message.pockers.length; ++i)
                    if (!$util.isInteger(message.pockers[i]))
                        return "pockers: integer[] expected";
            }
            if (message.winResult != null && message.hasOwnProperty("winResult")) {
                var error = $root.PK5Client.WinResult.verify(message.winResult);
                if (error)
                    return "winResult." + error;
            }
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            if (message.predict != null && message.hasOwnProperty("predict")) {
                if (!Array.isArray(message.predict))
                    return "predict: array expected";
                for (var i = 0; i < message.predict.length; ++i) {
                    var error = $root.PK5Client.Predict.verify(message.predict[i]);
                    if (error)
                        return "predict." + error;
                }
            }
            if (message.jpList != null && message.hasOwnProperty("jpList")) {
                if (!Array.isArray(message.jpList))
                    return "jpList: array expected";
                for (var i = 0; i < message.jpList.length; ++i)
                    if (typeof message.jpList[i] !== "number")
                        return "jpList: number[] expected";
            }
            if (message.nowGold != null && message.hasOwnProperty("nowGold"))
                if (!$util.isInteger(message.nowGold))
                    return "nowGold: integer expected";
            if (message.DoubleBetList != null && message.hasOwnProperty("DoubleBetList")) {
                if (!Array.isArray(message.DoubleBetList))
                    return "DoubleBetList: array expected";
                for (var i = 0; i < message.DoubleBetList.length; ++i)
                    if (!$util.isInteger(message.DoubleBetList[i]))
                        return "DoubleBetList: integer[] expected";
            }
            if (message.fakeJpList != null && message.hasOwnProperty("fakeJpList")) {
                if (!Array.isArray(message.fakeJpList))
                    return "fakeJpList: array expected";
                for (var i = 0; i < message.fakeJpList.length; ++i)
                    if (typeof message.fakeJpList[i] !== "number")
                        return "fakeJpList: number[] expected";
            }
            return null;
        };

        /**
         * Creates a BetRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.BetRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.BetRes} BetRes
         */
        BetRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.BetRes)
                return object;
            var message = new $root.PK5Client.BetRes();
            if (object.result != null)
                message.result = Boolean(object.result);
            if (object.pockers) {
                if (!Array.isArray(object.pockers))
                    throw TypeError(".PK5Client.BetRes.pockers: array expected");
                message.pockers = [];
                for (var i = 0; i < object.pockers.length; ++i)
                    message.pockers[i] = object.pockers[i] | 0;
            }
            if (object.winResult != null) {
                if (typeof object.winResult !== "object")
                    throw TypeError(".PK5Client.BetRes.winResult: object expected");
                message.winResult = $root.PK5Client.WinResult.fromObject(object.winResult);
            }
            if (object.uid != null)
                message.uid = object.uid | 0;
            if (object.predict) {
                if (!Array.isArray(object.predict))
                    throw TypeError(".PK5Client.BetRes.predict: array expected");
                message.predict = [];
                for (var i = 0; i < object.predict.length; ++i) {
                    if (typeof object.predict[i] !== "object")
                        throw TypeError(".PK5Client.BetRes.predict: object expected");
                    message.predict[i] = $root.PK5Client.Predict.fromObject(object.predict[i]);
                }
            }
            if (object.jpList) {
                if (!Array.isArray(object.jpList))
                    throw TypeError(".PK5Client.BetRes.jpList: array expected");
                message.jpList = [];
                for (var i = 0; i < object.jpList.length; ++i)
                    message.jpList[i] = Number(object.jpList[i]);
            }
            if (object.nowGold != null)
                message.nowGold = object.nowGold | 0;
            if (object.DoubleBetList) {
                if (!Array.isArray(object.DoubleBetList))
                    throw TypeError(".PK5Client.BetRes.DoubleBetList: array expected");
                message.DoubleBetList = [];
                for (var i = 0; i < object.DoubleBetList.length; ++i)
                    message.DoubleBetList[i] = object.DoubleBetList[i] | 0;
            }
            if (object.fakeJpList) {
                if (!Array.isArray(object.fakeJpList))
                    throw TypeError(".PK5Client.BetRes.fakeJpList: array expected");
                message.fakeJpList = [];
                for (var i = 0; i < object.fakeJpList.length; ++i)
                    message.fakeJpList[i] = Number(object.fakeJpList[i]);
            }
            return message;
        };

        /**
         * Creates a plain object from a BetRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.BetRes
         * @static
         * @param {PK5Client.BetRes} message BetRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        BetRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.pockers = [];
                object.predict = [];
                object.jpList = [];
                object.DoubleBetList = [];
                object.fakeJpList = [];
            }
            if (options.defaults) {
                object.result = false;
                object.winResult = null;
                object.uid = 0;
                object.nowGold = 0;
            }
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            if (message.pockers && message.pockers.length) {
                object.pockers = [];
                for (var j = 0; j < message.pockers.length; ++j)
                    object.pockers[j] = message.pockers[j];
            }
            if (message.winResult != null && message.hasOwnProperty("winResult"))
                object.winResult = $root.PK5Client.WinResult.toObject(message.winResult, options);
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.predict && message.predict.length) {
                object.predict = [];
                for (var j = 0; j < message.predict.length; ++j)
                    object.predict[j] = $root.PK5Client.Predict.toObject(message.predict[j], options);
            }
            if (message.jpList && message.jpList.length) {
                object.jpList = [];
                for (var j = 0; j < message.jpList.length; ++j)
                    object.jpList[j] = options.json && !isFinite(message.jpList[j]) ? String(message.jpList[j]) : message.jpList[j];
            }
            if (message.nowGold != null && message.hasOwnProperty("nowGold"))
                object.nowGold = message.nowGold;
            if (message.DoubleBetList && message.DoubleBetList.length) {
                object.DoubleBetList = [];
                for (var j = 0; j < message.DoubleBetList.length; ++j)
                    object.DoubleBetList[j] = message.DoubleBetList[j];
            }
            if (message.fakeJpList && message.fakeJpList.length) {
                object.fakeJpList = [];
                for (var j = 0; j < message.fakeJpList.length; ++j)
                    object.fakeJpList[j] = options.json && !isFinite(message.fakeJpList[j]) ? String(message.fakeJpList[j]) : message.fakeJpList[j];
            }
            return object;
        };

        /**
         * Converts this BetRes to JSON.
         * @function toJSON
         * @memberof PK5Client.BetRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        BetRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for BetRes
         * @function getTypeUrl
         * @memberof PK5Client.BetRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        BetRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.BetRes";
        };

        return BetRes;
    })();

    PK5Client.Predict = (function() {

        /**
         * Properties of a Predict.
         * @memberof PK5Client
         * @interface IPredict
         * @property {Array.<number>|null} [card] Predict card
         */

        /**
         * Constructs a new Predict.
         * @memberof PK5Client
         * @classdesc Represents a Predict.
         * @implements IPredict
         * @constructor
         * @param {PK5Client.IPredict=} [properties] Properties to set
         */
        function Predict(properties) {
            this.card = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Predict card.
         * @member {Array.<number>} card
         * @memberof PK5Client.Predict
         * @instance
         */
        Predict.prototype.card = $util.emptyArray;

        /**
         * Creates a new Predict instance using the specified properties.
         * @function create
         * @memberof PK5Client.Predict
         * @static
         * @param {PK5Client.IPredict=} [properties] Properties to set
         * @returns {PK5Client.Predict} Predict instance
         */
        Predict.create = function create(properties) {
            return new Predict(properties);
        };

        /**
         * Encodes the specified Predict message. Does not implicitly {@link PK5Client.Predict.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.Predict
         * @static
         * @param {PK5Client.IPredict} message Predict message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Predict.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.card != null && message.card.length) {
                writer.uint32(/* id 1, wireType 2 =*/10).fork();
                for (var i = 0; i < message.card.length; ++i)
                    writer.sint32(message.card[i]);
                writer.ldelim();
            }
            return writer;
        };

        /**
         * Encodes the specified Predict message, length delimited. Does not implicitly {@link PK5Client.Predict.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.Predict
         * @static
         * @param {PK5Client.IPredict} message Predict message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Predict.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Predict message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.Predict
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.Predict} Predict
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Predict.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.Predict();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        if (!(message.card && message.card.length))
                            message.card = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.card.push(reader.sint32());
                        } else
                            message.card.push(reader.sint32());
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Predict message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.Predict
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.Predict} Predict
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Predict.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Predict message.
         * @function verify
         * @memberof PK5Client.Predict
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Predict.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.card != null && message.hasOwnProperty("card")) {
                if (!Array.isArray(message.card))
                    return "card: array expected";
                for (var i = 0; i < message.card.length; ++i)
                    if (!$util.isInteger(message.card[i]))
                        return "card: integer[] expected";
            }
            return null;
        };

        /**
         * Creates a Predict message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.Predict
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.Predict} Predict
         */
        Predict.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.Predict)
                return object;
            var message = new $root.PK5Client.Predict();
            if (object.card) {
                if (!Array.isArray(object.card))
                    throw TypeError(".PK5Client.Predict.card: array expected");
                message.card = [];
                for (var i = 0; i < object.card.length; ++i)
                    message.card[i] = object.card[i] | 0;
            }
            return message;
        };

        /**
         * Creates a plain object from a Predict message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.Predict
         * @static
         * @param {PK5Client.Predict} message Predict
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Predict.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.card = [];
            if (message.card && message.card.length) {
                object.card = [];
                for (var j = 0; j < message.card.length; ++j)
                    object.card[j] = message.card[j];
            }
            return object;
        };

        /**
         * Converts this Predict to JSON.
         * @function toJSON
         * @memberof PK5Client.Predict
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Predict.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Predict
         * @function getTypeUrl
         * @memberof PK5Client.Predict
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Predict.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.Predict";
        };

        return Predict;
    })();

    PK5Client.WinResult = (function() {

        /**
         * Properties of a WinResult.
         * @memberof PK5Client
         * @interface IWinResult
         * @property {number|null} [win] WinResult win
         * @property {number|null} [winGold] WinResult winGold
         * @property {number|null} [uid] WinResult uid
         * @property {number|null} [winType] WinResult winType
         * @property {Array.<number>|null} [winPoker] WinResult winPoker
         * @property {number|null} [winJP] WinResult winJP
         * @property {number|null} [winJP2] WinResult winJP2
         * @property {number|null} [winJP2Gold] WinResult winJP2Gold
         * @property {number|null} [winJPGold] WinResult winJPGold
         * @property {number|Long|null} [bonusLeft] WinResult bonusLeft
         * @property {number|Long|null} [bonusRight] WinResult bonusRight
         * @property {Array.<number|Long>|null} [jpSet] WinResult jpSet
         * @property {number|Long|null} [betFull] WinResult betFull
         * @property {number|Long|null} [fullhouseWin] WinResult fullhouseWin
         */

        /**
         * Constructs a new WinResult.
         * @memberof PK5Client
         * @classdesc Represents a WinResult.
         * @implements IWinResult
         * @constructor
         * @param {PK5Client.IWinResult=} [properties] Properties to set
         */
        function WinResult(properties) {
            this.winPoker = [];
            this.jpSet = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * WinResult win.
         * @member {number} win
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.win = 0;

        /**
         * WinResult winGold.
         * @member {number} winGold
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winGold = 0;

        /**
         * WinResult uid.
         * @member {number} uid
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.uid = 0;

        /**
         * WinResult winType.
         * @member {number} winType
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winType = 0;

        /**
         * WinResult winPoker.
         * @member {Array.<number>} winPoker
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winPoker = $util.emptyArray;

        /**
         * WinResult winJP.
         * @member {number} winJP
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winJP = 0;

        /**
         * WinResult winJP2.
         * @member {number} winJP2
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winJP2 = 0;

        /**
         * WinResult winJP2Gold.
         * @member {number} winJP2Gold
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winJP2Gold = 0;

        /**
         * WinResult winJPGold.
         * @member {number} winJPGold
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.winJPGold = 0;

        /**
         * WinResult bonusLeft.
         * @member {number|Long} bonusLeft
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.bonusLeft = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * WinResult bonusRight.
         * @member {number|Long} bonusRight
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.bonusRight = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * WinResult jpSet.
         * @member {Array.<number|Long>} jpSet
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.jpSet = $util.emptyArray;

        /**
         * WinResult betFull.
         * @member {number|Long} betFull
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.betFull = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * WinResult fullhouseWin.
         * @member {number|Long} fullhouseWin
         * @memberof PK5Client.WinResult
         * @instance
         */
        WinResult.prototype.fullhouseWin = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Creates a new WinResult instance using the specified properties.
         * @function create
         * @memberof PK5Client.WinResult
         * @static
         * @param {PK5Client.IWinResult=} [properties] Properties to set
         * @returns {PK5Client.WinResult} WinResult instance
         */
        WinResult.create = function create(properties) {
            return new WinResult(properties);
        };

        /**
         * Encodes the specified WinResult message. Does not implicitly {@link PK5Client.WinResult.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.WinResult
         * @static
         * @param {PK5Client.IWinResult} message WinResult message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WinResult.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.win != null && Object.hasOwnProperty.call(message, "win"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.win);
            if (message.winGold != null && Object.hasOwnProperty.call(message, "winGold"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.winGold);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.uid);
            if (message.winType != null && Object.hasOwnProperty.call(message, "winType"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.winType);
            if (message.winPoker != null && message.winPoker.length) {
                writer.uint32(/* id 5, wireType 2 =*/42).fork();
                for (var i = 0; i < message.winPoker.length; ++i)
                    writer.int32(message.winPoker[i]);
                writer.ldelim();
            }
            if (message.winJP != null && Object.hasOwnProperty.call(message, "winJP"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.winJP);
            if (message.winJP2 != null && Object.hasOwnProperty.call(message, "winJP2"))
                writer.uint32(/* id 7, wireType 0 =*/56).int32(message.winJP2);
            if (message.winJP2Gold != null && Object.hasOwnProperty.call(message, "winJP2Gold"))
                writer.uint32(/* id 8, wireType 0 =*/64).int32(message.winJP2Gold);
            if (message.winJPGold != null && Object.hasOwnProperty.call(message, "winJPGold"))
                writer.uint32(/* id 9, wireType 0 =*/72).int32(message.winJPGold);
            if (message.bonusLeft != null && Object.hasOwnProperty.call(message, "bonusLeft"))
                writer.uint32(/* id 10, wireType 0 =*/80).int64(message.bonusLeft);
            if (message.bonusRight != null && Object.hasOwnProperty.call(message, "bonusRight"))
                writer.uint32(/* id 11, wireType 0 =*/88).int64(message.bonusRight);
            if (message.jpSet != null && message.jpSet.length) {
                writer.uint32(/* id 12, wireType 2 =*/98).fork();
                for (var i = 0; i < message.jpSet.length; ++i)
                    writer.int64(message.jpSet[i]);
                writer.ldelim();
            }
            if (message.betFull != null && Object.hasOwnProperty.call(message, "betFull"))
                writer.uint32(/* id 13, wireType 0 =*/104).int64(message.betFull);
            if (message.fullhouseWin != null && Object.hasOwnProperty.call(message, "fullhouseWin"))
                writer.uint32(/* id 14, wireType 0 =*/112).int64(message.fullhouseWin);
            return writer;
        };

        /**
         * Encodes the specified WinResult message, length delimited. Does not implicitly {@link PK5Client.WinResult.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.WinResult
         * @static
         * @param {PK5Client.IWinResult} message WinResult message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        WinResult.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a WinResult message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.WinResult
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.WinResult} WinResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WinResult.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.WinResult();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.win = reader.int32();
                        break;
                    }
                case 2: {
                        message.winGold = reader.int32();
                        break;
                    }
                case 3: {
                        message.uid = reader.int32();
                        break;
                    }
                case 4: {
                        message.winType = reader.int32();
                        break;
                    }
                case 5: {
                        if (!(message.winPoker && message.winPoker.length))
                            message.winPoker = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.winPoker.push(reader.int32());
                        } else
                            message.winPoker.push(reader.int32());
                        break;
                    }
                case 6: {
                        message.winJP = reader.int32();
                        break;
                    }
                case 7: {
                        message.winJP2 = reader.int32();
                        break;
                    }
                case 8: {
                        message.winJP2Gold = reader.int32();
                        break;
                    }
                case 9: {
                        message.winJPGold = reader.int32();
                        break;
                    }
                case 10: {
                        message.bonusLeft = reader.int64();
                        break;
                    }
                case 11: {
                        message.bonusRight = reader.int64();
                        break;
                    }
                case 12: {
                        if (!(message.jpSet && message.jpSet.length))
                            message.jpSet = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.jpSet.push(reader.int64());
                        } else
                            message.jpSet.push(reader.int64());
                        break;
                    }
                case 13: {
                        message.betFull = reader.int64();
                        break;
                    }
                case 14: {
                        message.fullhouseWin = reader.int64();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a WinResult message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.WinResult
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.WinResult} WinResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        WinResult.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a WinResult message.
         * @function verify
         * @memberof PK5Client.WinResult
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        WinResult.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.win != null && message.hasOwnProperty("win"))
                if (!$util.isInteger(message.win))
                    return "win: integer expected";
            if (message.winGold != null && message.hasOwnProperty("winGold"))
                if (!$util.isInteger(message.winGold))
                    return "winGold: integer expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            if (message.winType != null && message.hasOwnProperty("winType"))
                if (!$util.isInteger(message.winType))
                    return "winType: integer expected";
            if (message.winPoker != null && message.hasOwnProperty("winPoker")) {
                if (!Array.isArray(message.winPoker))
                    return "winPoker: array expected";
                for (var i = 0; i < message.winPoker.length; ++i)
                    if (!$util.isInteger(message.winPoker[i]))
                        return "winPoker: integer[] expected";
            }
            if (message.winJP != null && message.hasOwnProperty("winJP"))
                if (!$util.isInteger(message.winJP))
                    return "winJP: integer expected";
            if (message.winJP2 != null && message.hasOwnProperty("winJP2"))
                if (!$util.isInteger(message.winJP2))
                    return "winJP2: integer expected";
            if (message.winJP2Gold != null && message.hasOwnProperty("winJP2Gold"))
                if (!$util.isInteger(message.winJP2Gold))
                    return "winJP2Gold: integer expected";
            if (message.winJPGold != null && message.hasOwnProperty("winJPGold"))
                if (!$util.isInteger(message.winJPGold))
                    return "winJPGold: integer expected";
            if (message.bonusLeft != null && message.hasOwnProperty("bonusLeft"))
                if (!$util.isInteger(message.bonusLeft) && !(message.bonusLeft && $util.isInteger(message.bonusLeft.low) && $util.isInteger(message.bonusLeft.high)))
                    return "bonusLeft: integer|Long expected";
            if (message.bonusRight != null && message.hasOwnProperty("bonusRight"))
                if (!$util.isInteger(message.bonusRight) && !(message.bonusRight && $util.isInteger(message.bonusRight.low) && $util.isInteger(message.bonusRight.high)))
                    return "bonusRight: integer|Long expected";
            if (message.jpSet != null && message.hasOwnProperty("jpSet")) {
                if (!Array.isArray(message.jpSet))
                    return "jpSet: array expected";
                for (var i = 0; i < message.jpSet.length; ++i)
                    if (!$util.isInteger(message.jpSet[i]) && !(message.jpSet[i] && $util.isInteger(message.jpSet[i].low) && $util.isInteger(message.jpSet[i].high)))
                        return "jpSet: integer|Long[] expected";
            }
            if (message.betFull != null && message.hasOwnProperty("betFull"))
                if (!$util.isInteger(message.betFull) && !(message.betFull && $util.isInteger(message.betFull.low) && $util.isInteger(message.betFull.high)))
                    return "betFull: integer|Long expected";
            if (message.fullhouseWin != null && message.hasOwnProperty("fullhouseWin"))
                if (!$util.isInteger(message.fullhouseWin) && !(message.fullhouseWin && $util.isInteger(message.fullhouseWin.low) && $util.isInteger(message.fullhouseWin.high)))
                    return "fullhouseWin: integer|Long expected";
            return null;
        };

        /**
         * Creates a WinResult message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.WinResult
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.WinResult} WinResult
         */
        WinResult.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.WinResult)
                return object;
            var message = new $root.PK5Client.WinResult();
            if (object.win != null)
                message.win = object.win | 0;
            if (object.winGold != null)
                message.winGold = object.winGold | 0;
            if (object.uid != null)
                message.uid = object.uid | 0;
            if (object.winType != null)
                message.winType = object.winType | 0;
            if (object.winPoker) {
                if (!Array.isArray(object.winPoker))
                    throw TypeError(".PK5Client.WinResult.winPoker: array expected");
                message.winPoker = [];
                for (var i = 0; i < object.winPoker.length; ++i)
                    message.winPoker[i] = object.winPoker[i] | 0;
            }
            if (object.winJP != null)
                message.winJP = object.winJP | 0;
            if (object.winJP2 != null)
                message.winJP2 = object.winJP2 | 0;
            if (object.winJP2Gold != null)
                message.winJP2Gold = object.winJP2Gold | 0;
            if (object.winJPGold != null)
                message.winJPGold = object.winJPGold | 0;
            if (object.bonusLeft != null)
                if ($util.Long)
                    (message.bonusLeft = $util.Long.fromValue(object.bonusLeft)).unsigned = false;
                else if (typeof object.bonusLeft === "string")
                    message.bonusLeft = parseInt(object.bonusLeft, 10);
                else if (typeof object.bonusLeft === "number")
                    message.bonusLeft = object.bonusLeft;
                else if (typeof object.bonusLeft === "object")
                    message.bonusLeft = new $util.LongBits(object.bonusLeft.low >>> 0, object.bonusLeft.high >>> 0).toNumber();
            if (object.bonusRight != null)
                if ($util.Long)
                    (message.bonusRight = $util.Long.fromValue(object.bonusRight)).unsigned = false;
                else if (typeof object.bonusRight === "string")
                    message.bonusRight = parseInt(object.bonusRight, 10);
                else if (typeof object.bonusRight === "number")
                    message.bonusRight = object.bonusRight;
                else if (typeof object.bonusRight === "object")
                    message.bonusRight = new $util.LongBits(object.bonusRight.low >>> 0, object.bonusRight.high >>> 0).toNumber();
            if (object.jpSet) {
                if (!Array.isArray(object.jpSet))
                    throw TypeError(".PK5Client.WinResult.jpSet: array expected");
                message.jpSet = [];
                for (var i = 0; i < object.jpSet.length; ++i)
                    if ($util.Long)
                        (message.jpSet[i] = $util.Long.fromValue(object.jpSet[i])).unsigned = false;
                    else if (typeof object.jpSet[i] === "string")
                        message.jpSet[i] = parseInt(object.jpSet[i], 10);
                    else if (typeof object.jpSet[i] === "number")
                        message.jpSet[i] = object.jpSet[i];
                    else if (typeof object.jpSet[i] === "object")
                        message.jpSet[i] = new $util.LongBits(object.jpSet[i].low >>> 0, object.jpSet[i].high >>> 0).toNumber();
            }
            if (object.betFull != null)
                if ($util.Long)
                    (message.betFull = $util.Long.fromValue(object.betFull)).unsigned = false;
                else if (typeof object.betFull === "string")
                    message.betFull = parseInt(object.betFull, 10);
                else if (typeof object.betFull === "number")
                    message.betFull = object.betFull;
                else if (typeof object.betFull === "object")
                    message.betFull = new $util.LongBits(object.betFull.low >>> 0, object.betFull.high >>> 0).toNumber();
            if (object.fullhouseWin != null)
                if ($util.Long)
                    (message.fullhouseWin = $util.Long.fromValue(object.fullhouseWin)).unsigned = false;
                else if (typeof object.fullhouseWin === "string")
                    message.fullhouseWin = parseInt(object.fullhouseWin, 10);
                else if (typeof object.fullhouseWin === "number")
                    message.fullhouseWin = object.fullhouseWin;
                else if (typeof object.fullhouseWin === "object")
                    message.fullhouseWin = new $util.LongBits(object.fullhouseWin.low >>> 0, object.fullhouseWin.high >>> 0).toNumber();
            return message;
        };

        /**
         * Creates a plain object from a WinResult message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.WinResult
         * @static
         * @param {PK5Client.WinResult} message WinResult
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        WinResult.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.winPoker = [];
                object.jpSet = [];
            }
            if (options.defaults) {
                object.win = 0;
                object.winGold = 0;
                object.uid = 0;
                object.winType = 0;
                object.winJP = 0;
                object.winJP2 = 0;
                object.winJP2Gold = 0;
                object.winJPGold = 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.bonusLeft = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.bonusLeft = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.bonusRight = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.bonusRight = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.betFull = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.betFull = options.longs === String ? "0" : 0;
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.fullhouseWin = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.fullhouseWin = options.longs === String ? "0" : 0;
            }
            if (message.win != null && message.hasOwnProperty("win"))
                object.win = message.win;
            if (message.winGold != null && message.hasOwnProperty("winGold"))
                object.winGold = message.winGold;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.winType != null && message.hasOwnProperty("winType"))
                object.winType = message.winType;
            if (message.winPoker && message.winPoker.length) {
                object.winPoker = [];
                for (var j = 0; j < message.winPoker.length; ++j)
                    object.winPoker[j] = message.winPoker[j];
            }
            if (message.winJP != null && message.hasOwnProperty("winJP"))
                object.winJP = message.winJP;
            if (message.winJP2 != null && message.hasOwnProperty("winJP2"))
                object.winJP2 = message.winJP2;
            if (message.winJP2Gold != null && message.hasOwnProperty("winJP2Gold"))
                object.winJP2Gold = message.winJP2Gold;
            if (message.winJPGold != null && message.hasOwnProperty("winJPGold"))
                object.winJPGold = message.winJPGold;
            if (message.bonusLeft != null && message.hasOwnProperty("bonusLeft"))
                if (typeof message.bonusLeft === "number")
                    object.bonusLeft = options.longs === String ? String(message.bonusLeft) : message.bonusLeft;
                else
                    object.bonusLeft = options.longs === String ? $util.Long.prototype.toString.call(message.bonusLeft) : options.longs === Number ? new $util.LongBits(message.bonusLeft.low >>> 0, message.bonusLeft.high >>> 0).toNumber() : message.bonusLeft;
            if (message.bonusRight != null && message.hasOwnProperty("bonusRight"))
                if (typeof message.bonusRight === "number")
                    object.bonusRight = options.longs === String ? String(message.bonusRight) : message.bonusRight;
                else
                    object.bonusRight = options.longs === String ? $util.Long.prototype.toString.call(message.bonusRight) : options.longs === Number ? new $util.LongBits(message.bonusRight.low >>> 0, message.bonusRight.high >>> 0).toNumber() : message.bonusRight;
            if (message.jpSet && message.jpSet.length) {
                object.jpSet = [];
                for (var j = 0; j < message.jpSet.length; ++j)
                    if (typeof message.jpSet[j] === "number")
                        object.jpSet[j] = options.longs === String ? String(message.jpSet[j]) : message.jpSet[j];
                    else
                        object.jpSet[j] = options.longs === String ? $util.Long.prototype.toString.call(message.jpSet[j]) : options.longs === Number ? new $util.LongBits(message.jpSet[j].low >>> 0, message.jpSet[j].high >>> 0).toNumber() : message.jpSet[j];
            }
            if (message.betFull != null && message.hasOwnProperty("betFull"))
                if (typeof message.betFull === "number")
                    object.betFull = options.longs === String ? String(message.betFull) : message.betFull;
                else
                    object.betFull = options.longs === String ? $util.Long.prototype.toString.call(message.betFull) : options.longs === Number ? new $util.LongBits(message.betFull.low >>> 0, message.betFull.high >>> 0).toNumber() : message.betFull;
            if (message.fullhouseWin != null && message.hasOwnProperty("fullhouseWin"))
                if (typeof message.fullhouseWin === "number")
                    object.fullhouseWin = options.longs === String ? String(message.fullhouseWin) : message.fullhouseWin;
                else
                    object.fullhouseWin = options.longs === String ? $util.Long.prototype.toString.call(message.fullhouseWin) : options.longs === Number ? new $util.LongBits(message.fullhouseWin.low >>> 0, message.fullhouseWin.high >>> 0).toNumber() : message.fullhouseWin;
            return object;
        };

        /**
         * Converts this WinResult to JSON.
         * @function toJSON
         * @memberof PK5Client.WinResult
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        WinResult.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for WinResult
         * @function getTypeUrl
         * @memberof PK5Client.WinResult
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        WinResult.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.WinResult";
        };

        return WinResult;
    })();

    PK5Client.BetDouble = (function() {

        /**
         * Properties of a BetDouble.
         * @memberof PK5Client
         * @interface IBetDouble
         * @property {number|null} [betBigger] BetDouble betBigger
         * @property {number|null} [betSmaller] BetDouble betSmaller
         * @property {number|null} [betNum] BetDouble betNum
         */

        /**
         * Constructs a new BetDouble.
         * @memberof PK5Client
         * @classdesc Represents a BetDouble.
         * @implements IBetDouble
         * @constructor
         * @param {PK5Client.IBetDouble=} [properties] Properties to set
         */
        function BetDouble(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * BetDouble betBigger.
         * @member {number} betBigger
         * @memberof PK5Client.BetDouble
         * @instance
         */
        BetDouble.prototype.betBigger = 0;

        /**
         * BetDouble betSmaller.
         * @member {number} betSmaller
         * @memberof PK5Client.BetDouble
         * @instance
         */
        BetDouble.prototype.betSmaller = 0;

        /**
         * BetDouble betNum.
         * @member {number} betNum
         * @memberof PK5Client.BetDouble
         * @instance
         */
        BetDouble.prototype.betNum = 0;

        /**
         * Creates a new BetDouble instance using the specified properties.
         * @function create
         * @memberof PK5Client.BetDouble
         * @static
         * @param {PK5Client.IBetDouble=} [properties] Properties to set
         * @returns {PK5Client.BetDouble} BetDouble instance
         */
        BetDouble.create = function create(properties) {
            return new BetDouble(properties);
        };

        /**
         * Encodes the specified BetDouble message. Does not implicitly {@link PK5Client.BetDouble.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.BetDouble
         * @static
         * @param {PK5Client.IBetDouble} message BetDouble message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetDouble.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.betBigger != null && Object.hasOwnProperty.call(message, "betBigger"))
                writer.uint32(/* id 1, wireType 0 =*/8).sint32(message.betBigger);
            if (message.betSmaller != null && Object.hasOwnProperty.call(message, "betSmaller"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.betSmaller);
            if (message.betNum != null && Object.hasOwnProperty.call(message, "betNum"))
                writer.uint32(/* id 3, wireType 0 =*/24).sint32(message.betNum);
            return writer;
        };

        /**
         * Encodes the specified BetDouble message, length delimited. Does not implicitly {@link PK5Client.BetDouble.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.BetDouble
         * @static
         * @param {PK5Client.IBetDouble} message BetDouble message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetDouble.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a BetDouble message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.BetDouble
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.BetDouble} BetDouble
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetDouble.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.BetDouble();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.betBigger = reader.sint32();
                        break;
                    }
                case 2: {
                        message.betSmaller = reader.sint32();
                        break;
                    }
                case 3: {
                        message.betNum = reader.sint32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a BetDouble message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.BetDouble
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.BetDouble} BetDouble
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetDouble.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a BetDouble message.
         * @function verify
         * @memberof PK5Client.BetDouble
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        BetDouble.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.betBigger != null && message.hasOwnProperty("betBigger"))
                if (!$util.isInteger(message.betBigger))
                    return "betBigger: integer expected";
            if (message.betSmaller != null && message.hasOwnProperty("betSmaller"))
                if (!$util.isInteger(message.betSmaller))
                    return "betSmaller: integer expected";
            if (message.betNum != null && message.hasOwnProperty("betNum"))
                if (!$util.isInteger(message.betNum))
                    return "betNum: integer expected";
            return null;
        };

        /**
         * Creates a BetDouble message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.BetDouble
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.BetDouble} BetDouble
         */
        BetDouble.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.BetDouble)
                return object;
            var message = new $root.PK5Client.BetDouble();
            if (object.betBigger != null)
                message.betBigger = object.betBigger | 0;
            if (object.betSmaller != null)
                message.betSmaller = object.betSmaller | 0;
            if (object.betNum != null)
                message.betNum = object.betNum | 0;
            return message;
        };

        /**
         * Creates a plain object from a BetDouble message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.BetDouble
         * @static
         * @param {PK5Client.BetDouble} message BetDouble
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        BetDouble.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.betBigger = 0;
                object.betSmaller = 0;
                object.betNum = 0;
            }
            if (message.betBigger != null && message.hasOwnProperty("betBigger"))
                object.betBigger = message.betBigger;
            if (message.betSmaller != null && message.hasOwnProperty("betSmaller"))
                object.betSmaller = message.betSmaller;
            if (message.betNum != null && message.hasOwnProperty("betNum"))
                object.betNum = message.betNum;
            return object;
        };

        /**
         * Converts this BetDouble to JSON.
         * @function toJSON
         * @memberof PK5Client.BetDouble
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        BetDouble.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for BetDouble
         * @function getTypeUrl
         * @memberof PK5Client.BetDouble
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        BetDouble.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.BetDouble";
        };

        return BetDouble;
    })();

    PK5Client.BetDoubleRes = (function() {

        /**
         * Properties of a BetDoubleRes.
         * @memberof PK5Client
         * @interface IBetDoubleRes
         * @property {boolean|null} [result] BetDoubleRes result
         * @property {number|null} [win] BetDoubleRes win
         * @property {boolean|null} [isFinish] BetDoubleRes isFinish
         * @property {PK5Client.IWinResult|null} [winResult] BetDoubleRes winResult
         * @property {number|null} [uid] BetDoubleRes uid
         * @property {number|null} [nowGold] BetDoubleRes nowGold
         * @property {Array.<number>|null} [DoubleBetList] BetDoubleRes DoubleBetList
         */

        /**
         * Constructs a new BetDoubleRes.
         * @memberof PK5Client
         * @classdesc Represents a BetDoubleRes.
         * @implements IBetDoubleRes
         * @constructor
         * @param {PK5Client.IBetDoubleRes=} [properties] Properties to set
         */
        function BetDoubleRes(properties) {
            this.DoubleBetList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * BetDoubleRes result.
         * @member {boolean} result
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.result = false;

        /**
         * BetDoubleRes win.
         * @member {number} win
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.win = 0;

        /**
         * BetDoubleRes isFinish.
         * @member {boolean} isFinish
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.isFinish = false;

        /**
         * BetDoubleRes winResult.
         * @member {PK5Client.IWinResult|null|undefined} winResult
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.winResult = null;

        /**
         * BetDoubleRes uid.
         * @member {number} uid
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.uid = 0;

        /**
         * BetDoubleRes nowGold.
         * @member {number} nowGold
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.nowGold = 0;

        /**
         * BetDoubleRes DoubleBetList.
         * @member {Array.<number>} DoubleBetList
         * @memberof PK5Client.BetDoubleRes
         * @instance
         */
        BetDoubleRes.prototype.DoubleBetList = $util.emptyArray;

        /**
         * Creates a new BetDoubleRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {PK5Client.IBetDoubleRes=} [properties] Properties to set
         * @returns {PK5Client.BetDoubleRes} BetDoubleRes instance
         */
        BetDoubleRes.create = function create(properties) {
            return new BetDoubleRes(properties);
        };

        /**
         * Encodes the specified BetDoubleRes message. Does not implicitly {@link PK5Client.BetDoubleRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {PK5Client.IBetDoubleRes} message BetDoubleRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetDoubleRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            if (message.win != null && Object.hasOwnProperty.call(message, "win"))
                writer.uint32(/* id 2, wireType 0 =*/16).sint32(message.win);
            if (message.isFinish != null && Object.hasOwnProperty.call(message, "isFinish"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.isFinish);
            if (message.winResult != null && Object.hasOwnProperty.call(message, "winResult"))
                $root.PK5Client.WinResult.encode(message.winResult, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.uid);
            if (message.nowGold != null && Object.hasOwnProperty.call(message, "nowGold"))
                writer.uint32(/* id 7, wireType 0 =*/56).int32(message.nowGold);
            if (message.DoubleBetList != null && message.DoubleBetList.length) {
                writer.uint32(/* id 8, wireType 2 =*/66).fork();
                for (var i = 0; i < message.DoubleBetList.length; ++i)
                    writer.int32(message.DoubleBetList[i]);
                writer.ldelim();
            }
            return writer;
        };

        /**
         * Encodes the specified BetDoubleRes message, length delimited. Does not implicitly {@link PK5Client.BetDoubleRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {PK5Client.IBetDoubleRes} message BetDoubleRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetDoubleRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a BetDoubleRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.BetDoubleRes} BetDoubleRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetDoubleRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.BetDoubleRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                case 2: {
                        message.win = reader.sint32();
                        break;
                    }
                case 3: {
                        message.isFinish = reader.bool();
                        break;
                    }
                case 4: {
                        message.winResult = $root.PK5Client.WinResult.decode(reader, reader.uint32());
                        break;
                    }
                case 5: {
                        message.uid = reader.int32();
                        break;
                    }
                case 7: {
                        message.nowGold = reader.int32();
                        break;
                    }
                case 8: {
                        if (!(message.DoubleBetList && message.DoubleBetList.length))
                            message.DoubleBetList = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.DoubleBetList.push(reader.int32());
                        } else
                            message.DoubleBetList.push(reader.int32());
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a BetDoubleRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.BetDoubleRes} BetDoubleRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetDoubleRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a BetDoubleRes message.
         * @function verify
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        BetDoubleRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            if (message.win != null && message.hasOwnProperty("win"))
                if (!$util.isInteger(message.win))
                    return "win: integer expected";
            if (message.isFinish != null && message.hasOwnProperty("isFinish"))
                if (typeof message.isFinish !== "boolean")
                    return "isFinish: boolean expected";
            if (message.winResult != null && message.hasOwnProperty("winResult")) {
                var error = $root.PK5Client.WinResult.verify(message.winResult);
                if (error)
                    return "winResult." + error;
            }
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            if (message.nowGold != null && message.hasOwnProperty("nowGold"))
                if (!$util.isInteger(message.nowGold))
                    return "nowGold: integer expected";
            if (message.DoubleBetList != null && message.hasOwnProperty("DoubleBetList")) {
                if (!Array.isArray(message.DoubleBetList))
                    return "DoubleBetList: array expected";
                for (var i = 0; i < message.DoubleBetList.length; ++i)
                    if (!$util.isInteger(message.DoubleBetList[i]))
                        return "DoubleBetList: integer[] expected";
            }
            return null;
        };

        /**
         * Creates a BetDoubleRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.BetDoubleRes} BetDoubleRes
         */
        BetDoubleRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.BetDoubleRes)
                return object;
            var message = new $root.PK5Client.BetDoubleRes();
            if (object.result != null)
                message.result = Boolean(object.result);
            if (object.win != null)
                message.win = object.win | 0;
            if (object.isFinish != null)
                message.isFinish = Boolean(object.isFinish);
            if (object.winResult != null) {
                if (typeof object.winResult !== "object")
                    throw TypeError(".PK5Client.BetDoubleRes.winResult: object expected");
                message.winResult = $root.PK5Client.WinResult.fromObject(object.winResult);
            }
            if (object.uid != null)
                message.uid = object.uid | 0;
            if (object.nowGold != null)
                message.nowGold = object.nowGold | 0;
            if (object.DoubleBetList) {
                if (!Array.isArray(object.DoubleBetList))
                    throw TypeError(".PK5Client.BetDoubleRes.DoubleBetList: array expected");
                message.DoubleBetList = [];
                for (var i = 0; i < object.DoubleBetList.length; ++i)
                    message.DoubleBetList[i] = object.DoubleBetList[i] | 0;
            }
            return message;
        };

        /**
         * Creates a plain object from a BetDoubleRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {PK5Client.BetDoubleRes} message BetDoubleRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        BetDoubleRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.DoubleBetList = [];
            if (options.defaults) {
                object.result = false;
                object.win = 0;
                object.isFinish = false;
                object.winResult = null;
                object.uid = 0;
                object.nowGold = 0;
            }
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            if (message.win != null && message.hasOwnProperty("win"))
                object.win = message.win;
            if (message.isFinish != null && message.hasOwnProperty("isFinish"))
                object.isFinish = message.isFinish;
            if (message.winResult != null && message.hasOwnProperty("winResult"))
                object.winResult = $root.PK5Client.WinResult.toObject(message.winResult, options);
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.nowGold != null && message.hasOwnProperty("nowGold"))
                object.nowGold = message.nowGold;
            if (message.DoubleBetList && message.DoubleBetList.length) {
                object.DoubleBetList = [];
                for (var j = 0; j < message.DoubleBetList.length; ++j)
                    object.DoubleBetList[j] = message.DoubleBetList[j];
            }
            return object;
        };

        /**
         * Converts this BetDoubleRes to JSON.
         * @function toJSON
         * @memberof PK5Client.BetDoubleRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        BetDoubleRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for BetDoubleRes
         * @function getTypeUrl
         * @memberof PK5Client.BetDoubleRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        BetDoubleRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.BetDoubleRes";
        };

        return BetDoubleRes;
    })();

    PK5Client.BetGiveUp = (function() {

        /**
         * Properties of a BetGiveUp.
         * @memberof PK5Client
         * @interface IBetGiveUp
         */

        /**
         * Constructs a new BetGiveUp.
         * @memberof PK5Client
         * @classdesc Represents a BetGiveUp.
         * @implements IBetGiveUp
         * @constructor
         * @param {PK5Client.IBetGiveUp=} [properties] Properties to set
         */
        function BetGiveUp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new BetGiveUp instance using the specified properties.
         * @function create
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {PK5Client.IBetGiveUp=} [properties] Properties to set
         * @returns {PK5Client.BetGiveUp} BetGiveUp instance
         */
        BetGiveUp.create = function create(properties) {
            return new BetGiveUp(properties);
        };

        /**
         * Encodes the specified BetGiveUp message. Does not implicitly {@link PK5Client.BetGiveUp.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {PK5Client.IBetGiveUp} message BetGiveUp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetGiveUp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified BetGiveUp message, length delimited. Does not implicitly {@link PK5Client.BetGiveUp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {PK5Client.IBetGiveUp} message BetGiveUp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetGiveUp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a BetGiveUp message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.BetGiveUp} BetGiveUp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetGiveUp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.BetGiveUp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a BetGiveUp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.BetGiveUp} BetGiveUp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetGiveUp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a BetGiveUp message.
         * @function verify
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        BetGiveUp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a BetGiveUp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.BetGiveUp} BetGiveUp
         */
        BetGiveUp.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.BetGiveUp)
                return object;
            return new $root.PK5Client.BetGiveUp();
        };

        /**
         * Creates a plain object from a BetGiveUp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {PK5Client.BetGiveUp} message BetGiveUp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        BetGiveUp.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this BetGiveUp to JSON.
         * @function toJSON
         * @memberof PK5Client.BetGiveUp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        BetGiveUp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for BetGiveUp
         * @function getTypeUrl
         * @memberof PK5Client.BetGiveUp
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        BetGiveUp.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.BetGiveUp";
        };

        return BetGiveUp;
    })();

    PK5Client.BetGiveUpRes = (function() {

        /**
         * Properties of a BetGiveUpRes.
         * @memberof PK5Client
         * @interface IBetGiveUpRes
         * @property {boolean|null} [result] BetGiveUpRes result
         * @property {number|null} [uid] BetGiveUpRes uid
         */

        /**
         * Constructs a new BetGiveUpRes.
         * @memberof PK5Client
         * @classdesc Represents a BetGiveUpRes.
         * @implements IBetGiveUpRes
         * @constructor
         * @param {PK5Client.IBetGiveUpRes=} [properties] Properties to set
         */
        function BetGiveUpRes(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * BetGiveUpRes result.
         * @member {boolean} result
         * @memberof PK5Client.BetGiveUpRes
         * @instance
         */
        BetGiveUpRes.prototype.result = false;

        /**
         * BetGiveUpRes uid.
         * @member {number} uid
         * @memberof PK5Client.BetGiveUpRes
         * @instance
         */
        BetGiveUpRes.prototype.uid = 0;

        /**
         * Creates a new BetGiveUpRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {PK5Client.IBetGiveUpRes=} [properties] Properties to set
         * @returns {PK5Client.BetGiveUpRes} BetGiveUpRes instance
         */
        BetGiveUpRes.create = function create(properties) {
            return new BetGiveUpRes(properties);
        };

        /**
         * Encodes the specified BetGiveUpRes message. Does not implicitly {@link PK5Client.BetGiveUpRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {PK5Client.IBetGiveUpRes} message BetGiveUpRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetGiveUpRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.uid);
            return writer;
        };

        /**
         * Encodes the specified BetGiveUpRes message, length delimited. Does not implicitly {@link PK5Client.BetGiveUpRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {PK5Client.IBetGiveUpRes} message BetGiveUpRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        BetGiveUpRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a BetGiveUpRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.BetGiveUpRes} BetGiveUpRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetGiveUpRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.BetGiveUpRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                case 2: {
                        message.uid = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a BetGiveUpRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.BetGiveUpRes} BetGiveUpRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        BetGiveUpRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a BetGiveUpRes message.
         * @function verify
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        BetGiveUpRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            return null;
        };

        /**
         * Creates a BetGiveUpRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.BetGiveUpRes} BetGiveUpRes
         */
        BetGiveUpRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.BetGiveUpRes)
                return object;
            var message = new $root.PK5Client.BetGiveUpRes();
            if (object.result != null)
                message.result = Boolean(object.result);
            if (object.uid != null)
                message.uid = object.uid | 0;
            return message;
        };

        /**
         * Creates a plain object from a BetGiveUpRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {PK5Client.BetGiveUpRes} message BetGiveUpRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        BetGiveUpRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.result = false;
                object.uid = 0;
            }
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            return object;
        };

        /**
         * Converts this BetGiveUpRes to JSON.
         * @function toJSON
         * @memberof PK5Client.BetGiveUpRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        BetGiveUpRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for BetGiveUpRes
         * @function getTypeUrl
         * @memberof PK5Client.BetGiveUpRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        BetGiveUpRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.BetGiveUpRes";
        };

        return BetGiveUpRes;
    })();

    PK5Client.LeaveMachine = (function() {

        /**
         * Properties of a LeaveMachine.
         * @memberof PK5Client
         * @interface ILeaveMachine
         */

        /**
         * Constructs a new LeaveMachine.
         * @memberof PK5Client
         * @classdesc Represents a LeaveMachine.
         * @implements ILeaveMachine
         * @constructor
         * @param {PK5Client.ILeaveMachine=} [properties] Properties to set
         */
        function LeaveMachine(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new LeaveMachine instance using the specified properties.
         * @function create
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {PK5Client.ILeaveMachine=} [properties] Properties to set
         * @returns {PK5Client.LeaveMachine} LeaveMachine instance
         */
        LeaveMachine.create = function create(properties) {
            return new LeaveMachine(properties);
        };

        /**
         * Encodes the specified LeaveMachine message. Does not implicitly {@link PK5Client.LeaveMachine.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {PK5Client.ILeaveMachine} message LeaveMachine message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LeaveMachine.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified LeaveMachine message, length delimited. Does not implicitly {@link PK5Client.LeaveMachine.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {PK5Client.ILeaveMachine} message LeaveMachine message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LeaveMachine.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a LeaveMachine message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.LeaveMachine} LeaveMachine
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LeaveMachine.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.LeaveMachine();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a LeaveMachine message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.LeaveMachine} LeaveMachine
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LeaveMachine.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a LeaveMachine message.
         * @function verify
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        LeaveMachine.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a LeaveMachine message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.LeaveMachine} LeaveMachine
         */
        LeaveMachine.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.LeaveMachine)
                return object;
            return new $root.PK5Client.LeaveMachine();
        };

        /**
         * Creates a plain object from a LeaveMachine message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {PK5Client.LeaveMachine} message LeaveMachine
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        LeaveMachine.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this LeaveMachine to JSON.
         * @function toJSON
         * @memberof PK5Client.LeaveMachine
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        LeaveMachine.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for LeaveMachine
         * @function getTypeUrl
         * @memberof PK5Client.LeaveMachine
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        LeaveMachine.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.LeaveMachine";
        };

        return LeaveMachine;
    })();

    PK5Client.LeaveMachineRes = (function() {

        /**
         * Properties of a LeaveMachineRes.
         * @memberof PK5Client
         * @interface ILeaveMachineRes
         * @property {boolean|null} [result] LeaveMachineRes result
         */

        /**
         * Constructs a new LeaveMachineRes.
         * @memberof PK5Client
         * @classdesc Represents a LeaveMachineRes.
         * @implements ILeaveMachineRes
         * @constructor
         * @param {PK5Client.ILeaveMachineRes=} [properties] Properties to set
         */
        function LeaveMachineRes(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LeaveMachineRes result.
         * @member {boolean} result
         * @memberof PK5Client.LeaveMachineRes
         * @instance
         */
        LeaveMachineRes.prototype.result = false;

        /**
         * Creates a new LeaveMachineRes instance using the specified properties.
         * @function create
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {PK5Client.ILeaveMachineRes=} [properties] Properties to set
         * @returns {PK5Client.LeaveMachineRes} LeaveMachineRes instance
         */
        LeaveMachineRes.create = function create(properties) {
            return new LeaveMachineRes(properties);
        };

        /**
         * Encodes the specified LeaveMachineRes message. Does not implicitly {@link PK5Client.LeaveMachineRes.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {PK5Client.ILeaveMachineRes} message LeaveMachineRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LeaveMachineRes.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            return writer;
        };

        /**
         * Encodes the specified LeaveMachineRes message, length delimited. Does not implicitly {@link PK5Client.LeaveMachineRes.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {PK5Client.ILeaveMachineRes} message LeaveMachineRes message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LeaveMachineRes.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a LeaveMachineRes message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.LeaveMachineRes} LeaveMachineRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LeaveMachineRes.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.LeaveMachineRes();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a LeaveMachineRes message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.LeaveMachineRes} LeaveMachineRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LeaveMachineRes.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a LeaveMachineRes message.
         * @function verify
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        LeaveMachineRes.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            return null;
        };

        /**
         * Creates a LeaveMachineRes message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.LeaveMachineRes} LeaveMachineRes
         */
        LeaveMachineRes.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.LeaveMachineRes)
                return object;
            var message = new $root.PK5Client.LeaveMachineRes();
            if (object.result != null)
                message.result = Boolean(object.result);
            return message;
        };

        /**
         * Creates a plain object from a LeaveMachineRes message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {PK5Client.LeaveMachineRes} message LeaveMachineRes
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        LeaveMachineRes.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.result = false;
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            return object;
        };

        /**
         * Converts this LeaveMachineRes to JSON.
         * @function toJSON
         * @memberof PK5Client.LeaveMachineRes
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        LeaveMachineRes.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for LeaveMachineRes
         * @function getTypeUrl
         * @memberof PK5Client.LeaveMachineRes
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        LeaveMachineRes.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.LeaveMachineRes";
        };

        return LeaveMachineRes;
    })();

    PK5Client.GetActivityListReq = (function() {

        /**
         * Properties of a GetActivityListReq.
         * @memberof PK5Client
         * @interface IGetActivityListReq
         */

        /**
         * Constructs a new GetActivityListReq.
         * @memberof PK5Client
         * @classdesc Represents a GetActivityListReq.
         * @implements IGetActivityListReq
         * @constructor
         * @param {PK5Client.IGetActivityListReq=} [properties] Properties to set
         */
        function GetActivityListReq(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new GetActivityListReq instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {PK5Client.IGetActivityListReq=} [properties] Properties to set
         * @returns {PK5Client.GetActivityListReq} GetActivityListReq instance
         */
        GetActivityListReq.create = function create(properties) {
            return new GetActivityListReq(properties);
        };

        /**
         * Encodes the specified GetActivityListReq message. Does not implicitly {@link PK5Client.GetActivityListReq.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {PK5Client.IGetActivityListReq} message GetActivityListReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetActivityListReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified GetActivityListReq message, length delimited. Does not implicitly {@link PK5Client.GetActivityListReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {PK5Client.IGetActivityListReq} message GetActivityListReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetActivityListReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetActivityListReq message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetActivityListReq} GetActivityListReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetActivityListReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetActivityListReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetActivityListReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetActivityListReq} GetActivityListReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetActivityListReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetActivityListReq message.
         * @function verify
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetActivityListReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a GetActivityListReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetActivityListReq} GetActivityListReq
         */
        GetActivityListReq.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetActivityListReq)
                return object;
            return new $root.PK5Client.GetActivityListReq();
        };

        /**
         * Creates a plain object from a GetActivityListReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {PK5Client.GetActivityListReq} message GetActivityListReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetActivityListReq.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this GetActivityListReq to JSON.
         * @function toJSON
         * @memberof PK5Client.GetActivityListReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetActivityListReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetActivityListReq
         * @function getTypeUrl
         * @memberof PK5Client.GetActivityListReq
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetActivityListReq.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetActivityListReq";
        };

        return GetActivityListReq;
    })();

    PK5Client.ActivityResp = (function() {

        /**
         * Properties of an ActivityResp.
         * @memberof PK5Client
         * @interface IActivityResp
         * @property {Array.<PK5Client.IActivity>|null} [activitys] ActivityResp activitys
         */

        /**
         * Constructs a new ActivityResp.
         * @memberof PK5Client
         * @classdesc Represents an ActivityResp.
         * @implements IActivityResp
         * @constructor
         * @param {PK5Client.IActivityResp=} [properties] Properties to set
         */
        function ActivityResp(properties) {
            this.activitys = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ActivityResp activitys.
         * @member {Array.<PK5Client.IActivity>} activitys
         * @memberof PK5Client.ActivityResp
         * @instance
         */
        ActivityResp.prototype.activitys = $util.emptyArray;

        /**
         * Creates a new ActivityResp instance using the specified properties.
         * @function create
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {PK5Client.IActivityResp=} [properties] Properties to set
         * @returns {PK5Client.ActivityResp} ActivityResp instance
         */
        ActivityResp.create = function create(properties) {
            return new ActivityResp(properties);
        };

        /**
         * Encodes the specified ActivityResp message. Does not implicitly {@link PK5Client.ActivityResp.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {PK5Client.IActivityResp} message ActivityResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ActivityResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.activitys != null && message.activitys.length)
                for (var i = 0; i < message.activitys.length; ++i)
                    $root.PK5Client.Activity.encode(message.activitys[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified ActivityResp message, length delimited. Does not implicitly {@link PK5Client.ActivityResp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {PK5Client.IActivityResp} message ActivityResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ActivityResp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an ActivityResp message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.ActivityResp} ActivityResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ActivityResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.ActivityResp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        if (!(message.activitys && message.activitys.length))
                            message.activitys = [];
                        message.activitys.push($root.PK5Client.Activity.decode(reader, reader.uint32()));
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an ActivityResp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.ActivityResp} ActivityResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ActivityResp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an ActivityResp message.
         * @function verify
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        ActivityResp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.activitys != null && message.hasOwnProperty("activitys")) {
                if (!Array.isArray(message.activitys))
                    return "activitys: array expected";
                for (var i = 0; i < message.activitys.length; ++i) {
                    var error = $root.PK5Client.Activity.verify(message.activitys[i]);
                    if (error)
                        return "activitys." + error;
                }
            }
            return null;
        };

        /**
         * Creates an ActivityResp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.ActivityResp} ActivityResp
         */
        ActivityResp.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.ActivityResp)
                return object;
            var message = new $root.PK5Client.ActivityResp();
            if (object.activitys) {
                if (!Array.isArray(object.activitys))
                    throw TypeError(".PK5Client.ActivityResp.activitys: array expected");
                message.activitys = [];
                for (var i = 0; i < object.activitys.length; ++i) {
                    if (typeof object.activitys[i] !== "object")
                        throw TypeError(".PK5Client.ActivityResp.activitys: object expected");
                    message.activitys[i] = $root.PK5Client.Activity.fromObject(object.activitys[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from an ActivityResp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {PK5Client.ActivityResp} message ActivityResp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ActivityResp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.activitys = [];
            if (message.activitys && message.activitys.length) {
                object.activitys = [];
                for (var j = 0; j < message.activitys.length; ++j)
                    object.activitys[j] = $root.PK5Client.Activity.toObject(message.activitys[j], options);
            }
            return object;
        };

        /**
         * Converts this ActivityResp to JSON.
         * @function toJSON
         * @memberof PK5Client.ActivityResp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        ActivityResp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for ActivityResp
         * @function getTypeUrl
         * @memberof PK5Client.ActivityResp
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        ActivityResp.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.ActivityResp";
        };

        return ActivityResp;
    })();

    PK5Client.Reward = (function() {

        /**
         * Properties of a Reward.
         * @memberof PK5Client
         * @interface IReward
         * @property {number|null} [itemId] Reward itemId
         * @property {number|null} [itemNum] Reward itemNum
         */

        /**
         * Constructs a new Reward.
         * @memberof PK5Client
         * @classdesc Represents a Reward.
         * @implements IReward
         * @constructor
         * @param {PK5Client.IReward=} [properties] Properties to set
         */
        function Reward(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Reward itemId.
         * @member {number} itemId
         * @memberof PK5Client.Reward
         * @instance
         */
        Reward.prototype.itemId = 0;

        /**
         * Reward itemNum.
         * @member {number} itemNum
         * @memberof PK5Client.Reward
         * @instance
         */
        Reward.prototype.itemNum = 0;

        /**
         * Creates a new Reward instance using the specified properties.
         * @function create
         * @memberof PK5Client.Reward
         * @static
         * @param {PK5Client.IReward=} [properties] Properties to set
         * @returns {PK5Client.Reward} Reward instance
         */
        Reward.create = function create(properties) {
            return new Reward(properties);
        };

        /**
         * Encodes the specified Reward message. Does not implicitly {@link PK5Client.Reward.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.Reward
         * @static
         * @param {PK5Client.IReward} message Reward message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Reward.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.itemId != null && Object.hasOwnProperty.call(message, "itemId"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.itemId);
            if (message.itemNum != null && Object.hasOwnProperty.call(message, "itemNum"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.itemNum);
            return writer;
        };

        /**
         * Encodes the specified Reward message, length delimited. Does not implicitly {@link PK5Client.Reward.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.Reward
         * @static
         * @param {PK5Client.IReward} message Reward message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Reward.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Reward message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.Reward
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.Reward} Reward
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Reward.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.Reward();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.itemId = reader.int32();
                        break;
                    }
                case 2: {
                        message.itemNum = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Reward message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.Reward
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.Reward} Reward
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Reward.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Reward message.
         * @function verify
         * @memberof PK5Client.Reward
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Reward.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.itemId != null && message.hasOwnProperty("itemId"))
                if (!$util.isInteger(message.itemId))
                    return "itemId: integer expected";
            if (message.itemNum != null && message.hasOwnProperty("itemNum"))
                if (!$util.isInteger(message.itemNum))
                    return "itemNum: integer expected";
            return null;
        };

        /**
         * Creates a Reward message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.Reward
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.Reward} Reward
         */
        Reward.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.Reward)
                return object;
            var message = new $root.PK5Client.Reward();
            if (object.itemId != null)
                message.itemId = object.itemId | 0;
            if (object.itemNum != null)
                message.itemNum = object.itemNum | 0;
            return message;
        };

        /**
         * Creates a plain object from a Reward message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.Reward
         * @static
         * @param {PK5Client.Reward} message Reward
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Reward.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.itemId = 0;
                object.itemNum = 0;
            }
            if (message.itemId != null && message.hasOwnProperty("itemId"))
                object.itemId = message.itemId;
            if (message.itemNum != null && message.hasOwnProperty("itemNum"))
                object.itemNum = message.itemNum;
            return object;
        };

        /**
         * Converts this Reward to JSON.
         * @function toJSON
         * @memberof PK5Client.Reward
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Reward.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Reward
         * @function getTypeUrl
         * @memberof PK5Client.Reward
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Reward.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.Reward";
        };

        return Reward;
    })();

    PK5Client.Activity = (function() {

        /**
         * Properties of an Activity.
         * @memberof PK5Client
         * @interface IActivity
         * @property {number|null} [actId] Activity actId
         * @property {string|null} [title] Activity title
         * @property {string|null} [content] Activity content
         * @property {string|null} [imgUrl] Activity imgUrl
         * @property {number|null} [actType] Activity actType
         * @property {Array.<PK5Client.IReward>|null} [rewards] Activity rewards
         */

        /**
         * Constructs a new Activity.
         * @memberof PK5Client
         * @classdesc Represents an Activity.
         * @implements IActivity
         * @constructor
         * @param {PK5Client.IActivity=} [properties] Properties to set
         */
        function Activity(properties) {
            this.rewards = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Activity actId.
         * @member {number} actId
         * @memberof PK5Client.Activity
         * @instance
         */
        Activity.prototype.actId = 0;

        /**
         * Activity title.
         * @member {string} title
         * @memberof PK5Client.Activity
         * @instance
         */
        Activity.prototype.title = "";

        /**
         * Activity content.
         * @member {string} content
         * @memberof PK5Client.Activity
         * @instance
         */
        Activity.prototype.content = "";

        /**
         * Activity imgUrl.
         * @member {string} imgUrl
         * @memberof PK5Client.Activity
         * @instance
         */
        Activity.prototype.imgUrl = "";

        /**
         * Activity actType.
         * @member {number} actType
         * @memberof PK5Client.Activity
         * @instance
         */
        Activity.prototype.actType = 0;

        /**
         * Activity rewards.
         * @member {Array.<PK5Client.IReward>} rewards
         * @memberof PK5Client.Activity
         * @instance
         */
        Activity.prototype.rewards = $util.emptyArray;

        /**
         * Creates a new Activity instance using the specified properties.
         * @function create
         * @memberof PK5Client.Activity
         * @static
         * @param {PK5Client.IActivity=} [properties] Properties to set
         * @returns {PK5Client.Activity} Activity instance
         */
        Activity.create = function create(properties) {
            return new Activity(properties);
        };

        /**
         * Encodes the specified Activity message. Does not implicitly {@link PK5Client.Activity.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.Activity
         * @static
         * @param {PK5Client.IActivity} message Activity message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Activity.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.actId != null && Object.hasOwnProperty.call(message, "actId"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.actId);
            if (message.title != null && Object.hasOwnProperty.call(message, "title"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.title);
            if (message.content != null && Object.hasOwnProperty.call(message, "content"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.content);
            if (message.imgUrl != null && Object.hasOwnProperty.call(message, "imgUrl"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.imgUrl);
            if (message.actType != null && Object.hasOwnProperty.call(message, "actType"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.actType);
            if (message.rewards != null && message.rewards.length)
                for (var i = 0; i < message.rewards.length; ++i)
                    $root.PK5Client.Reward.encode(message.rewards[i], writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified Activity message, length delimited. Does not implicitly {@link PK5Client.Activity.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.Activity
         * @static
         * @param {PK5Client.IActivity} message Activity message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Activity.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an Activity message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.Activity
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.Activity} Activity
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Activity.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.Activity();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.actId = reader.int32();
                        break;
                    }
                case 2: {
                        message.title = reader.string();
                        break;
                    }
                case 3: {
                        message.content = reader.string();
                        break;
                    }
                case 4: {
                        message.imgUrl = reader.string();
                        break;
                    }
                case 5: {
                        message.actType = reader.int32();
                        break;
                    }
                case 6: {
                        if (!(message.rewards && message.rewards.length))
                            message.rewards = [];
                        message.rewards.push($root.PK5Client.Reward.decode(reader, reader.uint32()));
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an Activity message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.Activity
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.Activity} Activity
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Activity.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an Activity message.
         * @function verify
         * @memberof PK5Client.Activity
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Activity.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.actId != null && message.hasOwnProperty("actId"))
                if (!$util.isInteger(message.actId))
                    return "actId: integer expected";
            if (message.title != null && message.hasOwnProperty("title"))
                if (!$util.isString(message.title))
                    return "title: string expected";
            if (message.content != null && message.hasOwnProperty("content"))
                if (!$util.isString(message.content))
                    return "content: string expected";
            if (message.imgUrl != null && message.hasOwnProperty("imgUrl"))
                if (!$util.isString(message.imgUrl))
                    return "imgUrl: string expected";
            if (message.actType != null && message.hasOwnProperty("actType"))
                if (!$util.isInteger(message.actType))
                    return "actType: integer expected";
            if (message.rewards != null && message.hasOwnProperty("rewards")) {
                if (!Array.isArray(message.rewards))
                    return "rewards: array expected";
                for (var i = 0; i < message.rewards.length; ++i) {
                    var error = $root.PK5Client.Reward.verify(message.rewards[i]);
                    if (error)
                        return "rewards." + error;
                }
            }
            return null;
        };

        /**
         * Creates an Activity message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.Activity
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.Activity} Activity
         */
        Activity.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.Activity)
                return object;
            var message = new $root.PK5Client.Activity();
            if (object.actId != null)
                message.actId = object.actId | 0;
            if (object.title != null)
                message.title = String(object.title);
            if (object.content != null)
                message.content = String(object.content);
            if (object.imgUrl != null)
                message.imgUrl = String(object.imgUrl);
            if (object.actType != null)
                message.actType = object.actType | 0;
            if (object.rewards) {
                if (!Array.isArray(object.rewards))
                    throw TypeError(".PK5Client.Activity.rewards: array expected");
                message.rewards = [];
                for (var i = 0; i < object.rewards.length; ++i) {
                    if (typeof object.rewards[i] !== "object")
                        throw TypeError(".PK5Client.Activity.rewards: object expected");
                    message.rewards[i] = $root.PK5Client.Reward.fromObject(object.rewards[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from an Activity message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.Activity
         * @static
         * @param {PK5Client.Activity} message Activity
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Activity.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.rewards = [];
            if (options.defaults) {
                object.actId = 0;
                object.title = "";
                object.content = "";
                object.imgUrl = "";
                object.actType = 0;
            }
            if (message.actId != null && message.hasOwnProperty("actId"))
                object.actId = message.actId;
            if (message.title != null && message.hasOwnProperty("title"))
                object.title = message.title;
            if (message.content != null && message.hasOwnProperty("content"))
                object.content = message.content;
            if (message.imgUrl != null && message.hasOwnProperty("imgUrl"))
                object.imgUrl = message.imgUrl;
            if (message.actType != null && message.hasOwnProperty("actType"))
                object.actType = message.actType;
            if (message.rewards && message.rewards.length) {
                object.rewards = [];
                for (var j = 0; j < message.rewards.length; ++j)
                    object.rewards[j] = $root.PK5Client.Reward.toObject(message.rewards[j], options);
            }
            return object;
        };

        /**
         * Converts this Activity to JSON.
         * @function toJSON
         * @memberof PK5Client.Activity
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Activity.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Activity
         * @function getTypeUrl
         * @memberof PK5Client.Activity
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Activity.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.Activity";
        };

        return Activity;
    })();

    PK5Client.GetActivityRewardReq = (function() {

        /**
         * Properties of a GetActivityRewardReq.
         * @memberof PK5Client
         * @interface IGetActivityRewardReq
         * @property {number|null} [actId] GetActivityRewardReq actId
         * @property {number|null} [uid] GetActivityRewardReq uid
         */

        /**
         * Constructs a new GetActivityRewardReq.
         * @memberof PK5Client
         * @classdesc Represents a GetActivityRewardReq.
         * @implements IGetActivityRewardReq
         * @constructor
         * @param {PK5Client.IGetActivityRewardReq=} [properties] Properties to set
         */
        function GetActivityRewardReq(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetActivityRewardReq actId.
         * @member {number} actId
         * @memberof PK5Client.GetActivityRewardReq
         * @instance
         */
        GetActivityRewardReq.prototype.actId = 0;

        /**
         * GetActivityRewardReq uid.
         * @member {number} uid
         * @memberof PK5Client.GetActivityRewardReq
         * @instance
         */
        GetActivityRewardReq.prototype.uid = 0;

        /**
         * Creates a new GetActivityRewardReq instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {PK5Client.IGetActivityRewardReq=} [properties] Properties to set
         * @returns {PK5Client.GetActivityRewardReq} GetActivityRewardReq instance
         */
        GetActivityRewardReq.create = function create(properties) {
            return new GetActivityRewardReq(properties);
        };

        /**
         * Encodes the specified GetActivityRewardReq message. Does not implicitly {@link PK5Client.GetActivityRewardReq.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {PK5Client.IGetActivityRewardReq} message GetActivityRewardReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetActivityRewardReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.actId != null && Object.hasOwnProperty.call(message, "actId"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.actId);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.uid);
            return writer;
        };

        /**
         * Encodes the specified GetActivityRewardReq message, length delimited. Does not implicitly {@link PK5Client.GetActivityRewardReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {PK5Client.IGetActivityRewardReq} message GetActivityRewardReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetActivityRewardReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetActivityRewardReq message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetActivityRewardReq} GetActivityRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetActivityRewardReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetActivityRewardReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.actId = reader.int32();
                        break;
                    }
                case 2: {
                        message.uid = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetActivityRewardReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetActivityRewardReq} GetActivityRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetActivityRewardReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetActivityRewardReq message.
         * @function verify
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetActivityRewardReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.actId != null && message.hasOwnProperty("actId"))
                if (!$util.isInteger(message.actId))
                    return "actId: integer expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            return null;
        };

        /**
         * Creates a GetActivityRewardReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetActivityRewardReq} GetActivityRewardReq
         */
        GetActivityRewardReq.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetActivityRewardReq)
                return object;
            var message = new $root.PK5Client.GetActivityRewardReq();
            if (object.actId != null)
                message.actId = object.actId | 0;
            if (object.uid != null)
                message.uid = object.uid | 0;
            return message;
        };

        /**
         * Creates a plain object from a GetActivityRewardReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {PK5Client.GetActivityRewardReq} message GetActivityRewardReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetActivityRewardReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.actId = 0;
                object.uid = 0;
            }
            if (message.actId != null && message.hasOwnProperty("actId"))
                object.actId = message.actId;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            return object;
        };

        /**
         * Converts this GetActivityRewardReq to JSON.
         * @function toJSON
         * @memberof PK5Client.GetActivityRewardReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetActivityRewardReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetActivityRewardReq
         * @function getTypeUrl
         * @memberof PK5Client.GetActivityRewardReq
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetActivityRewardReq.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetActivityRewardReq";
        };

        return GetActivityRewardReq;
    })();

    PK5Client.GetActivityRewardResp = (function() {

        /**
         * Properties of a GetActivityRewardResp.
         * @memberof PK5Client
         * @interface IGetActivityRewardResp
         * @property {boolean|null} [result] GetActivityRewardResp result
         * @property {number|null} [uid] GetActivityRewardResp uid
         * @property {PK5Client.IReward|null} [reward] GetActivityRewardResp reward
         */

        /**
         * Constructs a new GetActivityRewardResp.
         * @memberof PK5Client
         * @classdesc Represents a GetActivityRewardResp.
         * @implements IGetActivityRewardResp
         * @constructor
         * @param {PK5Client.IGetActivityRewardResp=} [properties] Properties to set
         */
        function GetActivityRewardResp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetActivityRewardResp result.
         * @member {boolean} result
         * @memberof PK5Client.GetActivityRewardResp
         * @instance
         */
        GetActivityRewardResp.prototype.result = false;

        /**
         * GetActivityRewardResp uid.
         * @member {number} uid
         * @memberof PK5Client.GetActivityRewardResp
         * @instance
         */
        GetActivityRewardResp.prototype.uid = 0;

        /**
         * GetActivityRewardResp reward.
         * @member {PK5Client.IReward|null|undefined} reward
         * @memberof PK5Client.GetActivityRewardResp
         * @instance
         */
        GetActivityRewardResp.prototype.reward = null;

        /**
         * Creates a new GetActivityRewardResp instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {PK5Client.IGetActivityRewardResp=} [properties] Properties to set
         * @returns {PK5Client.GetActivityRewardResp} GetActivityRewardResp instance
         */
        GetActivityRewardResp.create = function create(properties) {
            return new GetActivityRewardResp(properties);
        };

        /**
         * Encodes the specified GetActivityRewardResp message. Does not implicitly {@link PK5Client.GetActivityRewardResp.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {PK5Client.IGetActivityRewardResp} message GetActivityRewardResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetActivityRewardResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.uid);
            if (message.reward != null && Object.hasOwnProperty.call(message, "reward"))
                $root.PK5Client.Reward.encode(message.reward, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified GetActivityRewardResp message, length delimited. Does not implicitly {@link PK5Client.GetActivityRewardResp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {PK5Client.IGetActivityRewardResp} message GetActivityRewardResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetActivityRewardResp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetActivityRewardResp message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetActivityRewardResp} GetActivityRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetActivityRewardResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetActivityRewardResp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                case 2: {
                        message.uid = reader.int32();
                        break;
                    }
                case 3: {
                        message.reward = $root.PK5Client.Reward.decode(reader, reader.uint32());
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetActivityRewardResp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetActivityRewardResp} GetActivityRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetActivityRewardResp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetActivityRewardResp message.
         * @function verify
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetActivityRewardResp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            if (message.reward != null && message.hasOwnProperty("reward")) {
                var error = $root.PK5Client.Reward.verify(message.reward);
                if (error)
                    return "reward." + error;
            }
            return null;
        };

        /**
         * Creates a GetActivityRewardResp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetActivityRewardResp} GetActivityRewardResp
         */
        GetActivityRewardResp.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetActivityRewardResp)
                return object;
            var message = new $root.PK5Client.GetActivityRewardResp();
            if (object.result != null)
                message.result = Boolean(object.result);
            if (object.uid != null)
                message.uid = object.uid | 0;
            if (object.reward != null) {
                if (typeof object.reward !== "object")
                    throw TypeError(".PK5Client.GetActivityRewardResp.reward: object expected");
                message.reward = $root.PK5Client.Reward.fromObject(object.reward);
            }
            return message;
        };

        /**
         * Creates a plain object from a GetActivityRewardResp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {PK5Client.GetActivityRewardResp} message GetActivityRewardResp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetActivityRewardResp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.result = false;
                object.uid = 0;
                object.reward = null;
            }
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.reward != null && message.hasOwnProperty("reward"))
                object.reward = $root.PK5Client.Reward.toObject(message.reward, options);
            return object;
        };

        /**
         * Converts this GetActivityRewardResp to JSON.
         * @function toJSON
         * @memberof PK5Client.GetActivityRewardResp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetActivityRewardResp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetActivityRewardResp
         * @function getTypeUrl
         * @memberof PK5Client.GetActivityRewardResp
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetActivityRewardResp.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetActivityRewardResp";
        };

        return GetActivityRewardResp;
    })();

    PK5Client.GetAdRewardReq = (function() {

        /**
         * Properties of a GetAdRewardReq.
         * @memberof PK5Client
         * @interface IGetAdRewardReq
         * @property {number|null} [uid] GetAdRewardReq uid
         */

        /**
         * Constructs a new GetAdRewardReq.
         * @memberof PK5Client
         * @classdesc Represents a GetAdRewardReq.
         * @implements IGetAdRewardReq
         * @constructor
         * @param {PK5Client.IGetAdRewardReq=} [properties] Properties to set
         */
        function GetAdRewardReq(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetAdRewardReq uid.
         * @member {number} uid
         * @memberof PK5Client.GetAdRewardReq
         * @instance
         */
        GetAdRewardReq.prototype.uid = 0;

        /**
         * Creates a new GetAdRewardReq instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {PK5Client.IGetAdRewardReq=} [properties] Properties to set
         * @returns {PK5Client.GetAdRewardReq} GetAdRewardReq instance
         */
        GetAdRewardReq.create = function create(properties) {
            return new GetAdRewardReq(properties);
        };

        /**
         * Encodes the specified GetAdRewardReq message. Does not implicitly {@link PK5Client.GetAdRewardReq.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {PK5Client.IGetAdRewardReq} message GetAdRewardReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetAdRewardReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.uid);
            return writer;
        };

        /**
         * Encodes the specified GetAdRewardReq message, length delimited. Does not implicitly {@link PK5Client.GetAdRewardReq.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {PK5Client.IGetAdRewardReq} message GetAdRewardReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetAdRewardReq.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetAdRewardReq message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetAdRewardReq} GetAdRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetAdRewardReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetAdRewardReq();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.uid = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetAdRewardReq message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetAdRewardReq} GetAdRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetAdRewardReq.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetAdRewardReq message.
         * @function verify
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetAdRewardReq.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            return null;
        };

        /**
         * Creates a GetAdRewardReq message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetAdRewardReq} GetAdRewardReq
         */
        GetAdRewardReq.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetAdRewardReq)
                return object;
            var message = new $root.PK5Client.GetAdRewardReq();
            if (object.uid != null)
                message.uid = object.uid | 0;
            return message;
        };

        /**
         * Creates a plain object from a GetAdRewardReq message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {PK5Client.GetAdRewardReq} message GetAdRewardReq
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetAdRewardReq.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.uid = 0;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            return object;
        };

        /**
         * Converts this GetAdRewardReq to JSON.
         * @function toJSON
         * @memberof PK5Client.GetAdRewardReq
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetAdRewardReq.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetAdRewardReq
         * @function getTypeUrl
         * @memberof PK5Client.GetAdRewardReq
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetAdRewardReq.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetAdRewardReq";
        };

        return GetAdRewardReq;
    })();

    PK5Client.GetAdRewardResp = (function() {

        /**
         * Properties of a GetAdRewardResp.
         * @memberof PK5Client
         * @interface IGetAdRewardResp
         * @property {boolean|null} [result] GetAdRewardResp result
         * @property {number|null} [uid] GetAdRewardResp uid
         * @property {PK5Client.IReward|null} [reward] GetAdRewardResp reward
         */

        /**
         * Constructs a new GetAdRewardResp.
         * @memberof PK5Client
         * @classdesc Represents a GetAdRewardResp.
         * @implements IGetAdRewardResp
         * @constructor
         * @param {PK5Client.IGetAdRewardResp=} [properties] Properties to set
         */
        function GetAdRewardResp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GetAdRewardResp result.
         * @member {boolean} result
         * @memberof PK5Client.GetAdRewardResp
         * @instance
         */
        GetAdRewardResp.prototype.result = false;

        /**
         * GetAdRewardResp uid.
         * @member {number} uid
         * @memberof PK5Client.GetAdRewardResp
         * @instance
         */
        GetAdRewardResp.prototype.uid = 0;

        /**
         * GetAdRewardResp reward.
         * @member {PK5Client.IReward|null|undefined} reward
         * @memberof PK5Client.GetAdRewardResp
         * @instance
         */
        GetAdRewardResp.prototype.reward = null;

        /**
         * Creates a new GetAdRewardResp instance using the specified properties.
         * @function create
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {PK5Client.IGetAdRewardResp=} [properties] Properties to set
         * @returns {PK5Client.GetAdRewardResp} GetAdRewardResp instance
         */
        GetAdRewardResp.create = function create(properties) {
            return new GetAdRewardResp(properties);
        };

        /**
         * Encodes the specified GetAdRewardResp message. Does not implicitly {@link PK5Client.GetAdRewardResp.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {PK5Client.IGetAdRewardResp} message GetAdRewardResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetAdRewardResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.result != null && Object.hasOwnProperty.call(message, "result"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.result);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.uid);
            if (message.reward != null && Object.hasOwnProperty.call(message, "reward"))
                $root.PK5Client.Reward.encode(message.reward, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified GetAdRewardResp message, length delimited. Does not implicitly {@link PK5Client.GetAdRewardResp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {PK5Client.IGetAdRewardResp} message GetAdRewardResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GetAdRewardResp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GetAdRewardResp message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.GetAdRewardResp} GetAdRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetAdRewardResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.GetAdRewardResp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.result = reader.bool();
                        break;
                    }
                case 2: {
                        message.uid = reader.int32();
                        break;
                    }
                case 3: {
                        message.reward = $root.PK5Client.Reward.decode(reader, reader.uint32());
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GetAdRewardResp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.GetAdRewardResp} GetAdRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GetAdRewardResp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GetAdRewardResp message.
         * @function verify
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GetAdRewardResp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.result != null && message.hasOwnProperty("result"))
                if (typeof message.result !== "boolean")
                    return "result: boolean expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isInteger(message.uid))
                    return "uid: integer expected";
            if (message.reward != null && message.hasOwnProperty("reward")) {
                var error = $root.PK5Client.Reward.verify(message.reward);
                if (error)
                    return "reward." + error;
            }
            return null;
        };

        /**
         * Creates a GetAdRewardResp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.GetAdRewardResp} GetAdRewardResp
         */
        GetAdRewardResp.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.GetAdRewardResp)
                return object;
            var message = new $root.PK5Client.GetAdRewardResp();
            if (object.result != null)
                message.result = Boolean(object.result);
            if (object.uid != null)
                message.uid = object.uid | 0;
            if (object.reward != null) {
                if (typeof object.reward !== "object")
                    throw TypeError(".PK5Client.GetAdRewardResp.reward: object expected");
                message.reward = $root.PK5Client.Reward.fromObject(object.reward);
            }
            return message;
        };

        /**
         * Creates a plain object from a GetAdRewardResp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {PK5Client.GetAdRewardResp} message GetAdRewardResp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GetAdRewardResp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.result = false;
                object.uid = 0;
                object.reward = null;
            }
            if (message.result != null && message.hasOwnProperty("result"))
                object.result = message.result;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.reward != null && message.hasOwnProperty("reward"))
                object.reward = $root.PK5Client.Reward.toObject(message.reward, options);
            return object;
        };

        /**
         * Converts this GetAdRewardResp to JSON.
         * @function toJSON
         * @memberof PK5Client.GetAdRewardResp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GetAdRewardResp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for GetAdRewardResp
         * @function getTypeUrl
         * @memberof PK5Client.GetAdRewardResp
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        GetAdRewardResp.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.GetAdRewardResp";
        };

        return GetAdRewardResp;
    })();

    PK5Client.Error = (function() {

        /**
         * Properties of an Error.
         * @memberof PK5Client
         * @interface IError
         * @property {string|null} [msg] Error msg
         * @property {number|null} [code] Error code
         */

        /**
         * Constructs a new Error.
         * @memberof PK5Client
         * @classdesc Represents an Error.
         * @implements IError
         * @constructor
         * @param {PK5Client.IError=} [properties] Properties to set
         */
        function Error(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Error msg.
         * @member {string} msg
         * @memberof PK5Client.Error
         * @instance
         */
        Error.prototype.msg = "";

        /**
         * Error code.
         * @member {number} code
         * @memberof PK5Client.Error
         * @instance
         */
        Error.prototype.code = 0;

        /**
         * Creates a new Error instance using the specified properties.
         * @function create
         * @memberof PK5Client.Error
         * @static
         * @param {PK5Client.IError=} [properties] Properties to set
         * @returns {PK5Client.Error} Error instance
         */
        Error.create = function create(properties) {
            return new Error(properties);
        };

        /**
         * Encodes the specified Error message. Does not implicitly {@link PK5Client.Error.verify|verify} messages.
         * @function encode
         * @memberof PK5Client.Error
         * @static
         * @param {PK5Client.IError} message Error message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Error.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.msg != null && Object.hasOwnProperty.call(message, "msg"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.msg);
            if (message.code != null && Object.hasOwnProperty.call(message, "code"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.code);
            return writer;
        };

        /**
         * Encodes the specified Error message, length delimited. Does not implicitly {@link PK5Client.Error.verify|verify} messages.
         * @function encodeDelimited
         * @memberof PK5Client.Error
         * @static
         * @param {PK5Client.IError} message Error message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Error.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an Error message from the specified reader or buffer.
         * @function decode
         * @memberof PK5Client.Error
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {PK5Client.Error} Error
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Error.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PK5Client.Error();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.msg = reader.string();
                        break;
                    }
                case 2: {
                        message.code = reader.int32();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an Error message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof PK5Client.Error
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {PK5Client.Error} Error
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Error.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an Error message.
         * @function verify
         * @memberof PK5Client.Error
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Error.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.msg != null && message.hasOwnProperty("msg"))
                if (!$util.isString(message.msg))
                    return "msg: string expected";
            if (message.code != null && message.hasOwnProperty("code"))
                if (!$util.isInteger(message.code))
                    return "code: integer expected";
            return null;
        };

        /**
         * Creates an Error message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof PK5Client.Error
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {PK5Client.Error} Error
         */
        Error.fromObject = function fromObject(object) {
            if (object instanceof $root.PK5Client.Error)
                return object;
            var message = new $root.PK5Client.Error();
            if (object.msg != null)
                message.msg = String(object.msg);
            if (object.code != null)
                message.code = object.code | 0;
            return message;
        };

        /**
         * Creates a plain object from an Error message. Also converts values to other types if specified.
         * @function toObject
         * @memberof PK5Client.Error
         * @static
         * @param {PK5Client.Error} message Error
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Error.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.msg = "";
                object.code = 0;
            }
            if (message.msg != null && message.hasOwnProperty("msg"))
                object.msg = message.msg;
            if (message.code != null && message.hasOwnProperty("code"))
                object.code = message.code;
            return object;
        };

        /**
         * Converts this Error to JSON.
         * @function toJSON
         * @memberof PK5Client.Error
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Error.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for Error
         * @function getTypeUrl
         * @memberof PK5Client.Error
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        Error.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/PK5Client.Error";
        };

        return Error;
    })();

    /**
     * Key enum.
     * @name PK5Client.Key
     * @enum {number}
     * @property {number} CONNECT=1 CONNECT value
     * @property {number} CTS_USER_AUTH_CONNECT=2 CTS_USER_AUTH_CONNECT value
     * @property {number} STC_USER_AUTH_CONNECT_RES=3 STC_USER_AUTH_CONNECT_RES value
     * @property {number} CTS_SPK_GET_BET_RANGE=100 CTS_SPK_GET_BET_RANGE value
     * @property {number} STC_SPK_GET_BET_RANGE_RES=101 STC_SPK_GET_BET_RANGE_RES value
     * @property {number} CTS_SPK_SET_BET=102 CTS_SPK_SET_BET value
     * @property {number} STC_SPK_SET_BET_RES=103 STC_SPK_SET_BET_RES value
     * @property {number} CTS_SPK_GET_MACHINE_LIST=104 CTS_SPK_GET_MACHINE_LIST value
     * @property {number} STC_SPK_GET_MACHINE_LIST_RES=105 STC_SPK_GET_MACHINE_LIST_RES value
     * @property {number} CTS_SPK_GET_MACHINE_DETIAL=106 CTS_SPK_GET_MACHINE_DETIAL value
     * @property {number} STC_SPK_GET_MACHINE_DETIAL_RES=107 STC_SPK_GET_MACHINE_DETIAL_RES value
     * @property {number} CTS_SPK_INTO_GAME=108 CTS_SPK_INTO_GAME value
     * @property {number} STC_SPK_INTO_GAME_RES=109 STC_SPK_INTO_GAME_RES value
     * @property {number} CTS_SPK_BET=110 CTS_SPK_BET value
     * @property {number} STC_SPK_BET_RES=111 STC_SPK_BET_RES value
     * @property {number} CTS_SPK_BET_DOUBEL=112 CTS_SPK_BET_DOUBEL value
     * @property {number} STC_SPK_BET_DOUBEL_RES=113 STC_SPK_BET_DOUBEL_RES value
     * @property {number} CTS_SPK_LEAVE_MACHINE=116 CTS_SPK_LEAVE_MACHINE value
     * @property {number} STC_SPK_LEAVE_MACHINE_RES=117 STC_SPK_LEAVE_MACHINE_RES value
     * @property {number} STC_ERROR=900 STC_ERROR value
     */
    PK5Client.Key = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[1] = "CONNECT"] = 1;
        values[valuesById[2] = "CTS_USER_AUTH_CONNECT"] = 2;
        values[valuesById[3] = "STC_USER_AUTH_CONNECT_RES"] = 3;
        values[valuesById[100] = "CTS_SPK_GET_BET_RANGE"] = 100;
        values[valuesById[101] = "STC_SPK_GET_BET_RANGE_RES"] = 101;
        values[valuesById[102] = "CTS_SPK_SET_BET"] = 102;
        values[valuesById[103] = "STC_SPK_SET_BET_RES"] = 103;
        values[valuesById[104] = "CTS_SPK_GET_MACHINE_LIST"] = 104;
        values[valuesById[105] = "STC_SPK_GET_MACHINE_LIST_RES"] = 105;
        values[valuesById[106] = "CTS_SPK_GET_MACHINE_DETIAL"] = 106;
        values[valuesById[107] = "STC_SPK_GET_MACHINE_DETIAL_RES"] = 107;
        values[valuesById[108] = "CTS_SPK_INTO_GAME"] = 108;
        values[valuesById[109] = "STC_SPK_INTO_GAME_RES"] = 109;
        values[valuesById[110] = "CTS_SPK_BET"] = 110;
        values[valuesById[111] = "STC_SPK_BET_RES"] = 111;
        values[valuesById[112] = "CTS_SPK_BET_DOUBEL"] = 112;
        values[valuesById[113] = "STC_SPK_BET_DOUBEL_RES"] = 113;
        values[valuesById[116] = "CTS_SPK_LEAVE_MACHINE"] = 116;
        values[valuesById[117] = "STC_SPK_LEAVE_MACHINE_RES"] = 117;
        values[valuesById[900] = "STC_ERROR"] = 900;
        return values;
    })();

    return PK5Client;
})();

module.exports = $root;
