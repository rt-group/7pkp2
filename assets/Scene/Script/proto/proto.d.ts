// DO NOT EDIT! This is a generated file. Edit the JSDoc in src/*.js instead and run 'npm run build:types'.

/** Namespace PK5Client. */
export namespace PK5Client {

    /** Properties of a Connect. */
    interface IConnect {

        /** Connect result */
        result?: (boolean|null);
    }

    /** Represents a Connect. */
    class Connect implements IConnect {

        /**
         * Constructs a new Connect.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IConnect);

        /** Connect result. */
        public result: boolean;

        /**
         * Creates a new Connect instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Connect instance
         */
        public static create(properties?: PK5Client.IConnect): PK5Client.Connect;

        /**
         * Encodes the specified Connect message. Does not implicitly {@link PK5Client.Connect.verify|verify} messages.
         * @param message Connect message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IConnect, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Connect message, length delimited. Does not implicitly {@link PK5Client.Connect.verify|verify} messages.
         * @param message Connect message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IConnect, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Connect message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Connect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.Connect;

        /**
         * Decodes a Connect message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Connect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.Connect;

        /**
         * Verifies a Connect message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Connect message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Connect
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.Connect;

        /**
         * Creates a plain object from a Connect message. Also converts values to other types if specified.
         * @param message Connect
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.Connect, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Connect to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for Connect
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an AuthConnect. */
    interface IAuthConnect {

        /** AuthConnect account */
        account?: (string|null);

        /** AuthConnect pwd */
        pwd?: (string|null);

        /** AuthConnect vendor */
        vendor?: (string|null);

        /** AuthConnect platform */
        platform?: (string|null);

        /** AuthConnect token */
        token?: (string|null);
    }

    /** Represents an AuthConnect. */
    class AuthConnect implements IAuthConnect {

        /**
         * Constructs a new AuthConnect.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IAuthConnect);

        /** AuthConnect account. */
        public account: string;

        /** AuthConnect pwd. */
        public pwd: string;

        /** AuthConnect vendor. */
        public vendor: string;

        /** AuthConnect platform. */
        public platform: string;

        /** AuthConnect token. */
        public token: string;

        /**
         * Creates a new AuthConnect instance using the specified properties.
         * @param [properties] Properties to set
         * @returns AuthConnect instance
         */
        public static create(properties?: PK5Client.IAuthConnect): PK5Client.AuthConnect;

        /**
         * Encodes the specified AuthConnect message. Does not implicitly {@link PK5Client.AuthConnect.verify|verify} messages.
         * @param message AuthConnect message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IAuthConnect, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified AuthConnect message, length delimited. Does not implicitly {@link PK5Client.AuthConnect.verify|verify} messages.
         * @param message AuthConnect message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IAuthConnect, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an AuthConnect message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns AuthConnect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.AuthConnect;

        /**
         * Decodes an AuthConnect message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns AuthConnect
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.AuthConnect;

        /**
         * Verifies an AuthConnect message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an AuthConnect message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns AuthConnect
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.AuthConnect;

        /**
         * Creates a plain object from an AuthConnect message. Also converts values to other types if specified.
         * @param message AuthConnect
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.AuthConnect, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this AuthConnect to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for AuthConnect
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an AuthConnectRes. */
    interface IAuthConnectRes {

        /** AuthConnectRes name */
        name?: (string|null);

        /** AuthConnectRes money */
        money?: (number|null);

        /** AuthConnectRes uid */
        uid?: (number|null);

        /** AuthConnectRes watchAdTimeNow */
        watchAdTimeNow?: (number|null);
    }

    /** Represents an AuthConnectRes. */
    class AuthConnectRes implements IAuthConnectRes {

        /**
         * Constructs a new AuthConnectRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IAuthConnectRes);

        /** AuthConnectRes name. */
        public name: string;

        /** AuthConnectRes money. */
        public money: number;

        /** AuthConnectRes uid. */
        public uid: number;

        /** AuthConnectRes watchAdTimeNow. */
        public watchAdTimeNow: number;

        /**
         * Creates a new AuthConnectRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns AuthConnectRes instance
         */
        public static create(properties?: PK5Client.IAuthConnectRes): PK5Client.AuthConnectRes;

        /**
         * Encodes the specified AuthConnectRes message. Does not implicitly {@link PK5Client.AuthConnectRes.verify|verify} messages.
         * @param message AuthConnectRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IAuthConnectRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified AuthConnectRes message, length delimited. Does not implicitly {@link PK5Client.AuthConnectRes.verify|verify} messages.
         * @param message AuthConnectRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IAuthConnectRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an AuthConnectRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns AuthConnectRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.AuthConnectRes;

        /**
         * Decodes an AuthConnectRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns AuthConnectRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.AuthConnectRes;

        /**
         * Verifies an AuthConnectRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an AuthConnectRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns AuthConnectRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.AuthConnectRes;

        /**
         * Creates a plain object from an AuthConnectRes message. Also converts values to other types if specified.
         * @param message AuthConnectRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.AuthConnectRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this AuthConnectRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for AuthConnectRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetBetRange. */
    interface IGetBetRange {
    }

    /** Represents a GetBetRange. */
    class GetBetRange implements IGetBetRange {

        /**
         * Constructs a new GetBetRange.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetBetRange);

        /**
         * Creates a new GetBetRange instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetBetRange instance
         */
        public static create(properties?: PK5Client.IGetBetRange): PK5Client.GetBetRange;

        /**
         * Encodes the specified GetBetRange message. Does not implicitly {@link PK5Client.GetBetRange.verify|verify} messages.
         * @param message GetBetRange message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetBetRange, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetBetRange message, length delimited. Does not implicitly {@link PK5Client.GetBetRange.verify|verify} messages.
         * @param message GetBetRange message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetBetRange, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetBetRange message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetBetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetBetRange;

        /**
         * Decodes a GetBetRange message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetBetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetBetRange;

        /**
         * Verifies a GetBetRange message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetBetRange message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetBetRange
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetBetRange;

        /**
         * Creates a plain object from a GetBetRange message. Also converts values to other types if specified.
         * @param message GetBetRange
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetBetRange, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetBetRange to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetBetRange
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetBetRangeRes. */
    interface IGetBetRangeRes {

        /** GetBetRangeRes betranges */
        betranges?: (PK5Client.IBetRange[]|null);
    }

    /** Represents a GetBetRangeRes. */
    class GetBetRangeRes implements IGetBetRangeRes {

        /**
         * Constructs a new GetBetRangeRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetBetRangeRes);

        /** GetBetRangeRes betranges. */
        public betranges: PK5Client.IBetRange[];

        /**
         * Creates a new GetBetRangeRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetBetRangeRes instance
         */
        public static create(properties?: PK5Client.IGetBetRangeRes): PK5Client.GetBetRangeRes;

        /**
         * Encodes the specified GetBetRangeRes message. Does not implicitly {@link PK5Client.GetBetRangeRes.verify|verify} messages.
         * @param message GetBetRangeRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetBetRangeRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetBetRangeRes message, length delimited. Does not implicitly {@link PK5Client.GetBetRangeRes.verify|verify} messages.
         * @param message GetBetRangeRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetBetRangeRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetBetRangeRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetBetRangeRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetBetRangeRes;

        /**
         * Decodes a GetBetRangeRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetBetRangeRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetBetRangeRes;

        /**
         * Verifies a GetBetRangeRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetBetRangeRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetBetRangeRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetBetRangeRes;

        /**
         * Creates a plain object from a GetBetRangeRes message. Also converts values to other types if specified.
         * @param message GetBetRangeRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetBetRangeRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetBetRangeRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetBetRangeRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a BetRange. */
    interface IBetRange {

        /** BetRange betName */
        betName?: (string|null);

        /** BetRange miniBetMoney */
        miniBetMoney?: (number[]|null);

        /** BetRange maxMachineNum */
        maxMachineNum?: (number|null);

        /** BetRange betIndex */
        betIndex?: (number|null);

        /** BetRange uid */
        uid?: (number|null);
    }

    /** Represents a BetRange. */
    class BetRange implements IBetRange {

        /**
         * Constructs a new BetRange.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBetRange);

        /** BetRange betName. */
        public betName: string;

        /** BetRange miniBetMoney. */
        public miniBetMoney: number[];

        /** BetRange maxMachineNum. */
        public maxMachineNum: number;

        /** BetRange betIndex. */
        public betIndex: number;

        /** BetRange uid. */
        public uid: number;

        /**
         * Creates a new BetRange instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetRange instance
         */
        public static create(properties?: PK5Client.IBetRange): PK5Client.BetRange;

        /**
         * Encodes the specified BetRange message. Does not implicitly {@link PK5Client.BetRange.verify|verify} messages.
         * @param message BetRange message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBetRange, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified BetRange message, length delimited. Does not implicitly {@link PK5Client.BetRange.verify|verify} messages.
         * @param message BetRange message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBetRange, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a BetRange message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.BetRange;

        /**
         * Decodes a BetRange message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetRange
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.BetRange;

        /**
         * Verifies a BetRange message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a BetRange message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns BetRange
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.BetRange;

        /**
         * Creates a plain object from a BetRange message. Also converts values to other types if specified.
         * @param message BetRange
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.BetRange, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this BetRange to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for BetRange
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a SetBet. */
    interface ISetBet {

        /** SetBet betIndex */
        betIndex?: (number|null);
    }

    /** Represents a SetBet. */
    class SetBet implements ISetBet {

        /**
         * Constructs a new SetBet.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.ISetBet);

        /** SetBet betIndex. */
        public betIndex: number;

        /**
         * Creates a new SetBet instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SetBet instance
         */
        public static create(properties?: PK5Client.ISetBet): PK5Client.SetBet;

        /**
         * Encodes the specified SetBet message. Does not implicitly {@link PK5Client.SetBet.verify|verify} messages.
         * @param message SetBet message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.ISetBet, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SetBet message, length delimited. Does not implicitly {@link PK5Client.SetBet.verify|verify} messages.
         * @param message SetBet message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.ISetBet, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SetBet message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SetBet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.SetBet;

        /**
         * Decodes a SetBet message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SetBet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.SetBet;

        /**
         * Verifies a SetBet message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SetBet message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SetBet
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.SetBet;

        /**
         * Creates a plain object from a SetBet message. Also converts values to other types if specified.
         * @param message SetBet
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.SetBet, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SetBet to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for SetBet
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a SetBetRes. */
    interface ISetBetRes {

        /** SetBetRes result */
        result?: (boolean|null);

        /** SetBetRes uid */
        uid?: (number|null);
    }

    /** Represents a SetBetRes. */
    class SetBetRes implements ISetBetRes {

        /**
         * Constructs a new SetBetRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.ISetBetRes);

        /** SetBetRes result. */
        public result: boolean;

        /** SetBetRes uid. */
        public uid: number;

        /**
         * Creates a new SetBetRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SetBetRes instance
         */
        public static create(properties?: PK5Client.ISetBetRes): PK5Client.SetBetRes;

        /**
         * Encodes the specified SetBetRes message. Does not implicitly {@link PK5Client.SetBetRes.verify|verify} messages.
         * @param message SetBetRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.ISetBetRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SetBetRes message, length delimited. Does not implicitly {@link PK5Client.SetBetRes.verify|verify} messages.
         * @param message SetBetRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.ISetBetRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SetBetRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SetBetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.SetBetRes;

        /**
         * Decodes a SetBetRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SetBetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.SetBetRes;

        /**
         * Verifies a SetBetRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SetBetRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SetBetRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.SetBetRes;

        /**
         * Creates a plain object from a SetBetRes message. Also converts values to other types if specified.
         * @param message SetBetRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.SetBetRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SetBetRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for SetBetRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetMachineList. */
    interface IGetMachineList {

        /** GetMachineList page */
        page?: (number|null);

        /** GetMachineList pageNum */
        pageNum?: (number|null);
    }

    /** Represents a GetMachineList. */
    class GetMachineList implements IGetMachineList {

        /**
         * Constructs a new GetMachineList.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetMachineList);

        /** GetMachineList page. */
        public page: number;

        /** GetMachineList pageNum. */
        public pageNum: number;

        /**
         * Creates a new GetMachineList instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetMachineList instance
         */
        public static create(properties?: PK5Client.IGetMachineList): PK5Client.GetMachineList;

        /**
         * Encodes the specified GetMachineList message. Does not implicitly {@link PK5Client.GetMachineList.verify|verify} messages.
         * @param message GetMachineList message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetMachineList, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetMachineList message, length delimited. Does not implicitly {@link PK5Client.GetMachineList.verify|verify} messages.
         * @param message GetMachineList message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetMachineList, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetMachineList message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetMachineList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetMachineList;

        /**
         * Decodes a GetMachineList message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetMachineList
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetMachineList;

        /**
         * Verifies a GetMachineList message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetMachineList message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetMachineList
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetMachineList;

        /**
         * Creates a plain object from a GetMachineList message. Also converts values to other types if specified.
         * @param message GetMachineList
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetMachineList, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetMachineList to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetMachineList
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetMachineListRes. */
    interface IGetMachineListRes {

        /** GetMachineListRes hasPeople */
        hasPeople?: (string[]|null);

        /** GetMachineListRes machineIds */
        machineIds?: (string[]|null);

        /** GetMachineListRes isLock */
        isLock?: (string[]|null);
    }

    /** Represents a GetMachineListRes. */
    class GetMachineListRes implements IGetMachineListRes {

        /**
         * Constructs a new GetMachineListRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetMachineListRes);

        /** GetMachineListRes hasPeople. */
        public hasPeople: string[];

        /** GetMachineListRes machineIds. */
        public machineIds: string[];

        /** GetMachineListRes isLock. */
        public isLock: string[];

        /**
         * Creates a new GetMachineListRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetMachineListRes instance
         */
        public static create(properties?: PK5Client.IGetMachineListRes): PK5Client.GetMachineListRes;

        /**
         * Encodes the specified GetMachineListRes message. Does not implicitly {@link PK5Client.GetMachineListRes.verify|verify} messages.
         * @param message GetMachineListRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetMachineListRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetMachineListRes message, length delimited. Does not implicitly {@link PK5Client.GetMachineListRes.verify|verify} messages.
         * @param message GetMachineListRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetMachineListRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetMachineListRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetMachineListRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetMachineListRes;

        /**
         * Decodes a GetMachineListRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetMachineListRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetMachineListRes;

        /**
         * Verifies a GetMachineListRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetMachineListRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetMachineListRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetMachineListRes;

        /**
         * Creates a plain object from a GetMachineListRes message. Also converts values to other types if specified.
         * @param message GetMachineListRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetMachineListRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetMachineListRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetMachineListRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetMachineDetial. */
    interface IGetMachineDetial {

        /** GetMachineDetial machindId */
        machindId?: (string|null);
    }

    /** Represents a GetMachineDetial. */
    class GetMachineDetial implements IGetMachineDetial {

        /**
         * Constructs a new GetMachineDetial.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetMachineDetial);

        /** GetMachineDetial machindId. */
        public machindId: string;

        /**
         * Creates a new GetMachineDetial instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetMachineDetial instance
         */
        public static create(properties?: PK5Client.IGetMachineDetial): PK5Client.GetMachineDetial;

        /**
         * Encodes the specified GetMachineDetial message. Does not implicitly {@link PK5Client.GetMachineDetial.verify|verify} messages.
         * @param message GetMachineDetial message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetMachineDetial, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetMachineDetial message, length delimited. Does not implicitly {@link PK5Client.GetMachineDetial.verify|verify} messages.
         * @param message GetMachineDetial message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetMachineDetial, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetMachineDetial message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetMachineDetial
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetMachineDetial;

        /**
         * Decodes a GetMachineDetial message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetMachineDetial
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetMachineDetial;

        /**
         * Verifies a GetMachineDetial message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetMachineDetial message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetMachineDetial
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetMachineDetial;

        /**
         * Creates a plain object from a GetMachineDetial message. Also converts values to other types if specified.
         * @param message GetMachineDetial
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetMachineDetial, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetMachineDetial to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetMachineDetial
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetMachineDetialRes. */
    interface IGetMachineDetialRes {

        /** GetMachineDetialRes machindId */
        machindId?: (string|null);

        /** GetMachineDetialRes winList */
        winList?: (string[]|null);

        /** GetMachineDetialRes jpList */
        jpList?: (number[]|null);

        /** GetMachineDetialRes totalWinRate */
        totalWinRate?: (number|null);

        /** GetMachineDetialRes toadyWinRate */
        toadyWinRate?: (number|null);
    }

    /** Represents a GetMachineDetialRes. */
    class GetMachineDetialRes implements IGetMachineDetialRes {

        /**
         * Constructs a new GetMachineDetialRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetMachineDetialRes);

        /** GetMachineDetialRes machindId. */
        public machindId: string;

        /** GetMachineDetialRes winList. */
        public winList: string[];

        /** GetMachineDetialRes jpList. */
        public jpList: number[];

        /** GetMachineDetialRes totalWinRate. */
        public totalWinRate: number;

        /** GetMachineDetialRes toadyWinRate. */
        public toadyWinRate: number;

        /**
         * Creates a new GetMachineDetialRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetMachineDetialRes instance
         */
        public static create(properties?: PK5Client.IGetMachineDetialRes): PK5Client.GetMachineDetialRes;

        /**
         * Encodes the specified GetMachineDetialRes message. Does not implicitly {@link PK5Client.GetMachineDetialRes.verify|verify} messages.
         * @param message GetMachineDetialRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetMachineDetialRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetMachineDetialRes message, length delimited. Does not implicitly {@link PK5Client.GetMachineDetialRes.verify|verify} messages.
         * @param message GetMachineDetialRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetMachineDetialRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetMachineDetialRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetMachineDetialRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetMachineDetialRes;

        /**
         * Decodes a GetMachineDetialRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetMachineDetialRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetMachineDetialRes;

        /**
         * Verifies a GetMachineDetialRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetMachineDetialRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetMachineDetialRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetMachineDetialRes;

        /**
         * Creates a plain object from a GetMachineDetialRes message. Also converts values to other types if specified.
         * @param message GetMachineDetialRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetMachineDetialRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetMachineDetialRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetMachineDetialRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an IntoGame. */
    interface IIntoGame {

        /** IntoGame machindId */
        machindId?: (string|null);
    }

    /** Represents an IntoGame. */
    class IntoGame implements IIntoGame {

        /**
         * Constructs a new IntoGame.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IIntoGame);

        /** IntoGame machindId. */
        public machindId: string;

        /**
         * Creates a new IntoGame instance using the specified properties.
         * @param [properties] Properties to set
         * @returns IntoGame instance
         */
        public static create(properties?: PK5Client.IIntoGame): PK5Client.IntoGame;

        /**
         * Encodes the specified IntoGame message. Does not implicitly {@link PK5Client.IntoGame.verify|verify} messages.
         * @param message IntoGame message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IIntoGame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified IntoGame message, length delimited. Does not implicitly {@link PK5Client.IntoGame.verify|verify} messages.
         * @param message IntoGame message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IIntoGame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an IntoGame message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns IntoGame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.IntoGame;

        /**
         * Decodes an IntoGame message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns IntoGame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.IntoGame;

        /**
         * Verifies an IntoGame message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an IntoGame message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns IntoGame
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.IntoGame;

        /**
         * Creates a plain object from an IntoGame message. Also converts values to other types if specified.
         * @param message IntoGame
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.IntoGame, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this IntoGame to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for IntoGame
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an IntoGameRes. */
    interface IIntoGameRes {

        /** IntoGameRes result */
        result?: (boolean|null);
    }

    /** Represents an IntoGameRes. */
    class IntoGameRes implements IIntoGameRes {

        /**
         * Constructs a new IntoGameRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IIntoGameRes);

        /** IntoGameRes result. */
        public result: boolean;

        /**
         * Creates a new IntoGameRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns IntoGameRes instance
         */
        public static create(properties?: PK5Client.IIntoGameRes): PK5Client.IntoGameRes;

        /**
         * Encodes the specified IntoGameRes message. Does not implicitly {@link PK5Client.IntoGameRes.verify|verify} messages.
         * @param message IntoGameRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IIntoGameRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified IntoGameRes message, length delimited. Does not implicitly {@link PK5Client.IntoGameRes.verify|verify} messages.
         * @param message IntoGameRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IIntoGameRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an IntoGameRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns IntoGameRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.IntoGameRes;

        /**
         * Decodes an IntoGameRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns IntoGameRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.IntoGameRes;

        /**
         * Verifies an IntoGameRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an IntoGameRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns IntoGameRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.IntoGameRes;

        /**
         * Creates a plain object from an IntoGameRes message. Also converts values to other types if specified.
         * @param message IntoGameRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.IntoGameRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this IntoGameRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for IntoGameRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a Bet. */
    interface IBet {

        /** Bet betNum */
        betNum?: (number|null);

        /** Bet f */
        f?: (number|null);
    }

    /** Represents a Bet. */
    class Bet implements IBet {

        /**
         * Constructs a new Bet.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBet);

        /** Bet betNum. */
        public betNum: number;

        /** Bet f. */
        public f: number;

        /**
         * Creates a new Bet instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Bet instance
         */
        public static create(properties?: PK5Client.IBet): PK5Client.Bet;

        /**
         * Encodes the specified Bet message. Does not implicitly {@link PK5Client.Bet.verify|verify} messages.
         * @param message Bet message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBet, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Bet message, length delimited. Does not implicitly {@link PK5Client.Bet.verify|verify} messages.
         * @param message Bet message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBet, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Bet message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Bet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.Bet;

        /**
         * Decodes a Bet message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Bet
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.Bet;

        /**
         * Verifies a Bet message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Bet message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Bet
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.Bet;

        /**
         * Creates a plain object from a Bet message. Also converts values to other types if specified.
         * @param message Bet
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.Bet, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Bet to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for Bet
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a BetRes. */
    interface IBetRes {

        /** BetRes result */
        result?: (boolean|null);

        /** BetRes pockers */
        pockers?: (number[]|null);

        /** BetRes winResult */
        winResult?: (PK5Client.IWinResult|null);

        /** BetRes uid */
        uid?: (number|null);

        /** BetRes predict */
        predict?: (PK5Client.IPredict[]|null);

        /** BetRes jpList */
        jpList?: (number[]|null);

        /** BetRes nowGold */
        nowGold?: (number|null);

        /** BetRes DoubleBetList */
        DoubleBetList?: (number[]|null);

        /** BetRes fakeJpList */
        fakeJpList?: (number[]|null);
    }

    /** Represents a BetRes. */
    class BetRes implements IBetRes {

        /**
         * Constructs a new BetRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBetRes);

        /** BetRes result. */
        public result: boolean;

        /** BetRes pockers. */
        public pockers: number[];

        /** BetRes winResult. */
        public winResult?: (PK5Client.IWinResult|null);

        /** BetRes uid. */
        public uid: number;

        /** BetRes predict. */
        public predict: PK5Client.IPredict[];

        /** BetRes jpList. */
        public jpList: number[];

        /** BetRes nowGold. */
        public nowGold: number;

        /** BetRes DoubleBetList. */
        public DoubleBetList: number[];

        /** BetRes fakeJpList. */
        public fakeJpList: number[];

        /**
         * Creates a new BetRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetRes instance
         */
        public static create(properties?: PK5Client.IBetRes): PK5Client.BetRes;

        /**
         * Encodes the specified BetRes message. Does not implicitly {@link PK5Client.BetRes.verify|verify} messages.
         * @param message BetRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBetRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified BetRes message, length delimited. Does not implicitly {@link PK5Client.BetRes.verify|verify} messages.
         * @param message BetRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBetRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a BetRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.BetRes;

        /**
         * Decodes a BetRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.BetRes;

        /**
         * Verifies a BetRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a BetRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns BetRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.BetRes;

        /**
         * Creates a plain object from a BetRes message. Also converts values to other types if specified.
         * @param message BetRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.BetRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this BetRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for BetRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a Predict. */
    interface IPredict {

        /** Predict card */
        card?: (number[]|null);
    }

    /** Represents a Predict. */
    class Predict implements IPredict {

        /**
         * Constructs a new Predict.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IPredict);

        /** Predict card. */
        public card: number[];

        /**
         * Creates a new Predict instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Predict instance
         */
        public static create(properties?: PK5Client.IPredict): PK5Client.Predict;

        /**
         * Encodes the specified Predict message. Does not implicitly {@link PK5Client.Predict.verify|verify} messages.
         * @param message Predict message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IPredict, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Predict message, length delimited. Does not implicitly {@link PK5Client.Predict.verify|verify} messages.
         * @param message Predict message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IPredict, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Predict message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Predict
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.Predict;

        /**
         * Decodes a Predict message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Predict
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.Predict;

        /**
         * Verifies a Predict message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Predict message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Predict
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.Predict;

        /**
         * Creates a plain object from a Predict message. Also converts values to other types if specified.
         * @param message Predict
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.Predict, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Predict to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for Predict
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a WinResult. */
    interface IWinResult {

        /** WinResult win */
        win?: (number|null);

        /** WinResult winGold */
        winGold?: (number|null);

        /** WinResult uid */
        uid?: (number|null);

        /** WinResult winType */
        winType?: (number|null);

        /** WinResult winPoker */
        winPoker?: (number[]|null);

        /** WinResult winJP */
        winJP?: (number|null);

        /** WinResult winJP2 */
        winJP2?: (number|null);

        /** WinResult winJP2Gold */
        winJP2Gold?: (number|null);

        /** WinResult winJPGold */
        winJPGold?: (number|null);

        /** WinResult bonusLeft */
        bonusLeft?: (number|Long|null);

        /** WinResult bonusRight */
        bonusRight?: (number|Long|null);

        /** WinResult jpSet */
        jpSet?: ((number|Long)[]|null);

        /** WinResult betFull */
        betFull?: (number|Long|null);

        /** WinResult fullhouseWin */
        fullhouseWin?: (number|Long|null);
    }

    /** Represents a WinResult. */
    class WinResult implements IWinResult {

        /**
         * Constructs a new WinResult.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IWinResult);

        /** WinResult win. */
        public win: number;

        /** WinResult winGold. */
        public winGold: number;

        /** WinResult uid. */
        public uid: number;

        /** WinResult winType. */
        public winType: number;

        /** WinResult winPoker. */
        public winPoker: number[];

        /** WinResult winJP. */
        public winJP: number;

        /** WinResult winJP2. */
        public winJP2: number;

        /** WinResult winJP2Gold. */
        public winJP2Gold: number;

        /** WinResult winJPGold. */
        public winJPGold: number;

        /** WinResult bonusLeft. */
        public bonusLeft: (number|Long);

        /** WinResult bonusRight. */
        public bonusRight: (number|Long);

        /** WinResult jpSet. */
        public jpSet: (number|Long)[];

        /** WinResult betFull. */
        public betFull: (number|Long);

        /** WinResult fullhouseWin. */
        public fullhouseWin: (number|Long);

        /**
         * Creates a new WinResult instance using the specified properties.
         * @param [properties] Properties to set
         * @returns WinResult instance
         */
        public static create(properties?: PK5Client.IWinResult): PK5Client.WinResult;

        /**
         * Encodes the specified WinResult message. Does not implicitly {@link PK5Client.WinResult.verify|verify} messages.
         * @param message WinResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IWinResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified WinResult message, length delimited. Does not implicitly {@link PK5Client.WinResult.verify|verify} messages.
         * @param message WinResult message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IWinResult, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a WinResult message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns WinResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.WinResult;

        /**
         * Decodes a WinResult message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns WinResult
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.WinResult;

        /**
         * Verifies a WinResult message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a WinResult message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns WinResult
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.WinResult;

        /**
         * Creates a plain object from a WinResult message. Also converts values to other types if specified.
         * @param message WinResult
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.WinResult, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this WinResult to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for WinResult
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a BetDouble. */
    interface IBetDouble {

        /** BetDouble betBigger */
        betBigger?: (number|null);

        /** BetDouble betSmaller */
        betSmaller?: (number|null);

        /** BetDouble betNum */
        betNum?: (number|null);
    }

    /** Represents a BetDouble. */
    class BetDouble implements IBetDouble {

        /**
         * Constructs a new BetDouble.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBetDouble);

        /** BetDouble betBigger. */
        public betBigger: number;

        /** BetDouble betSmaller. */
        public betSmaller: number;

        /** BetDouble betNum. */
        public betNum: number;

        /**
         * Creates a new BetDouble instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetDouble instance
         */
        public static create(properties?: PK5Client.IBetDouble): PK5Client.BetDouble;

        /**
         * Encodes the specified BetDouble message. Does not implicitly {@link PK5Client.BetDouble.verify|verify} messages.
         * @param message BetDouble message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBetDouble, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified BetDouble message, length delimited. Does not implicitly {@link PK5Client.BetDouble.verify|verify} messages.
         * @param message BetDouble message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBetDouble, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a BetDouble message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetDouble
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.BetDouble;

        /**
         * Decodes a BetDouble message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetDouble
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.BetDouble;

        /**
         * Verifies a BetDouble message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a BetDouble message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns BetDouble
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.BetDouble;

        /**
         * Creates a plain object from a BetDouble message. Also converts values to other types if specified.
         * @param message BetDouble
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.BetDouble, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this BetDouble to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for BetDouble
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a BetDoubleRes. */
    interface IBetDoubleRes {

        /** BetDoubleRes result */
        result?: (boolean|null);

        /** BetDoubleRes win */
        win?: (number|null);

        /** BetDoubleRes isFinish */
        isFinish?: (boolean|null);

        /** BetDoubleRes winResult */
        winResult?: (PK5Client.IWinResult|null);

        /** BetDoubleRes uid */
        uid?: (number|null);

        /** BetDoubleRes nowGold */
        nowGold?: (number|null);

        /** BetDoubleRes DoubleBetList */
        DoubleBetList?: (number[]|null);
    }

    /** Represents a BetDoubleRes. */
    class BetDoubleRes implements IBetDoubleRes {

        /**
         * Constructs a new BetDoubleRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBetDoubleRes);

        /** BetDoubleRes result. */
        public result: boolean;

        /** BetDoubleRes win. */
        public win: number;

        /** BetDoubleRes isFinish. */
        public isFinish: boolean;

        /** BetDoubleRes winResult. */
        public winResult?: (PK5Client.IWinResult|null);

        /** BetDoubleRes uid. */
        public uid: number;

        /** BetDoubleRes nowGold. */
        public nowGold: number;

        /** BetDoubleRes DoubleBetList. */
        public DoubleBetList: number[];

        /**
         * Creates a new BetDoubleRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetDoubleRes instance
         */
        public static create(properties?: PK5Client.IBetDoubleRes): PK5Client.BetDoubleRes;

        /**
         * Encodes the specified BetDoubleRes message. Does not implicitly {@link PK5Client.BetDoubleRes.verify|verify} messages.
         * @param message BetDoubleRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBetDoubleRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified BetDoubleRes message, length delimited. Does not implicitly {@link PK5Client.BetDoubleRes.verify|verify} messages.
         * @param message BetDoubleRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBetDoubleRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a BetDoubleRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetDoubleRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.BetDoubleRes;

        /**
         * Decodes a BetDoubleRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetDoubleRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.BetDoubleRes;

        /**
         * Verifies a BetDoubleRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a BetDoubleRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns BetDoubleRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.BetDoubleRes;

        /**
         * Creates a plain object from a BetDoubleRes message. Also converts values to other types if specified.
         * @param message BetDoubleRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.BetDoubleRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this BetDoubleRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for BetDoubleRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a BetGiveUp. */
    interface IBetGiveUp {
    }

    /** Represents a BetGiveUp. */
    class BetGiveUp implements IBetGiveUp {

        /**
         * Constructs a new BetGiveUp.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBetGiveUp);

        /**
         * Creates a new BetGiveUp instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetGiveUp instance
         */
        public static create(properties?: PK5Client.IBetGiveUp): PK5Client.BetGiveUp;

        /**
         * Encodes the specified BetGiveUp message. Does not implicitly {@link PK5Client.BetGiveUp.verify|verify} messages.
         * @param message BetGiveUp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBetGiveUp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified BetGiveUp message, length delimited. Does not implicitly {@link PK5Client.BetGiveUp.verify|verify} messages.
         * @param message BetGiveUp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBetGiveUp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a BetGiveUp message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetGiveUp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.BetGiveUp;

        /**
         * Decodes a BetGiveUp message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetGiveUp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.BetGiveUp;

        /**
         * Verifies a BetGiveUp message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a BetGiveUp message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns BetGiveUp
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.BetGiveUp;

        /**
         * Creates a plain object from a BetGiveUp message. Also converts values to other types if specified.
         * @param message BetGiveUp
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.BetGiveUp, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this BetGiveUp to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for BetGiveUp
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a BetGiveUpRes. */
    interface IBetGiveUpRes {

        /** BetGiveUpRes result */
        result?: (boolean|null);

        /** BetGiveUpRes uid */
        uid?: (number|null);
    }

    /** Represents a BetGiveUpRes. */
    class BetGiveUpRes implements IBetGiveUpRes {

        /**
         * Constructs a new BetGiveUpRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IBetGiveUpRes);

        /** BetGiveUpRes result. */
        public result: boolean;

        /** BetGiveUpRes uid. */
        public uid: number;

        /**
         * Creates a new BetGiveUpRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns BetGiveUpRes instance
         */
        public static create(properties?: PK5Client.IBetGiveUpRes): PK5Client.BetGiveUpRes;

        /**
         * Encodes the specified BetGiveUpRes message. Does not implicitly {@link PK5Client.BetGiveUpRes.verify|verify} messages.
         * @param message BetGiveUpRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IBetGiveUpRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified BetGiveUpRes message, length delimited. Does not implicitly {@link PK5Client.BetGiveUpRes.verify|verify} messages.
         * @param message BetGiveUpRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IBetGiveUpRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a BetGiveUpRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns BetGiveUpRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.BetGiveUpRes;

        /**
         * Decodes a BetGiveUpRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns BetGiveUpRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.BetGiveUpRes;

        /**
         * Verifies a BetGiveUpRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a BetGiveUpRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns BetGiveUpRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.BetGiveUpRes;

        /**
         * Creates a plain object from a BetGiveUpRes message. Also converts values to other types if specified.
         * @param message BetGiveUpRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.BetGiveUpRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this BetGiveUpRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for BetGiveUpRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a LeaveMachine. */
    interface ILeaveMachine {
    }

    /** Represents a LeaveMachine. */
    class LeaveMachine implements ILeaveMachine {

        /**
         * Constructs a new LeaveMachine.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.ILeaveMachine);

        /**
         * Creates a new LeaveMachine instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LeaveMachine instance
         */
        public static create(properties?: PK5Client.ILeaveMachine): PK5Client.LeaveMachine;

        /**
         * Encodes the specified LeaveMachine message. Does not implicitly {@link PK5Client.LeaveMachine.verify|verify} messages.
         * @param message LeaveMachine message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.ILeaveMachine, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LeaveMachine message, length delimited. Does not implicitly {@link PK5Client.LeaveMachine.verify|verify} messages.
         * @param message LeaveMachine message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.ILeaveMachine, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LeaveMachine message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LeaveMachine
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.LeaveMachine;

        /**
         * Decodes a LeaveMachine message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LeaveMachine
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.LeaveMachine;

        /**
         * Verifies a LeaveMachine message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LeaveMachine message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LeaveMachine
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.LeaveMachine;

        /**
         * Creates a plain object from a LeaveMachine message. Also converts values to other types if specified.
         * @param message LeaveMachine
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.LeaveMachine, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LeaveMachine to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for LeaveMachine
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a LeaveMachineRes. */
    interface ILeaveMachineRes {

        /** LeaveMachineRes result */
        result?: (boolean|null);
    }

    /** Represents a LeaveMachineRes. */
    class LeaveMachineRes implements ILeaveMachineRes {

        /**
         * Constructs a new LeaveMachineRes.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.ILeaveMachineRes);

        /** LeaveMachineRes result. */
        public result: boolean;

        /**
         * Creates a new LeaveMachineRes instance using the specified properties.
         * @param [properties] Properties to set
         * @returns LeaveMachineRes instance
         */
        public static create(properties?: PK5Client.ILeaveMachineRes): PK5Client.LeaveMachineRes;

        /**
         * Encodes the specified LeaveMachineRes message. Does not implicitly {@link PK5Client.LeaveMachineRes.verify|verify} messages.
         * @param message LeaveMachineRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.ILeaveMachineRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified LeaveMachineRes message, length delimited. Does not implicitly {@link PK5Client.LeaveMachineRes.verify|verify} messages.
         * @param message LeaveMachineRes message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.ILeaveMachineRes, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LeaveMachineRes message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LeaveMachineRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.LeaveMachineRes;

        /**
         * Decodes a LeaveMachineRes message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns LeaveMachineRes
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.LeaveMachineRes;

        /**
         * Verifies a LeaveMachineRes message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a LeaveMachineRes message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns LeaveMachineRes
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.LeaveMachineRes;

        /**
         * Creates a plain object from a LeaveMachineRes message. Also converts values to other types if specified.
         * @param message LeaveMachineRes
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.LeaveMachineRes, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this LeaveMachineRes to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for LeaveMachineRes
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetActivityListReq. */
    interface IGetActivityListReq {
    }

    /** Represents a GetActivityListReq. */
    class GetActivityListReq implements IGetActivityListReq {

        /**
         * Constructs a new GetActivityListReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetActivityListReq);

        /**
         * Creates a new GetActivityListReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetActivityListReq instance
         */
        public static create(properties?: PK5Client.IGetActivityListReq): PK5Client.GetActivityListReq;

        /**
         * Encodes the specified GetActivityListReq message. Does not implicitly {@link PK5Client.GetActivityListReq.verify|verify} messages.
         * @param message GetActivityListReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetActivityListReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetActivityListReq message, length delimited. Does not implicitly {@link PK5Client.GetActivityListReq.verify|verify} messages.
         * @param message GetActivityListReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetActivityListReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetActivityListReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetActivityListReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetActivityListReq;

        /**
         * Decodes a GetActivityListReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetActivityListReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetActivityListReq;

        /**
         * Verifies a GetActivityListReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetActivityListReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetActivityListReq
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetActivityListReq;

        /**
         * Creates a plain object from a GetActivityListReq message. Also converts values to other types if specified.
         * @param message GetActivityListReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetActivityListReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetActivityListReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetActivityListReq
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an ActivityResp. */
    interface IActivityResp {

        /** ActivityResp activitys */
        activitys?: (PK5Client.IActivity[]|null);
    }

    /** Represents an ActivityResp. */
    class ActivityResp implements IActivityResp {

        /**
         * Constructs a new ActivityResp.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IActivityResp);

        /** ActivityResp activitys. */
        public activitys: PK5Client.IActivity[];

        /**
         * Creates a new ActivityResp instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ActivityResp instance
         */
        public static create(properties?: PK5Client.IActivityResp): PK5Client.ActivityResp;

        /**
         * Encodes the specified ActivityResp message. Does not implicitly {@link PK5Client.ActivityResp.verify|verify} messages.
         * @param message ActivityResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IActivityResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ActivityResp message, length delimited. Does not implicitly {@link PK5Client.ActivityResp.verify|verify} messages.
         * @param message ActivityResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IActivityResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an ActivityResp message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ActivityResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.ActivityResp;

        /**
         * Decodes an ActivityResp message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ActivityResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.ActivityResp;

        /**
         * Verifies an ActivityResp message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an ActivityResp message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ActivityResp
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.ActivityResp;

        /**
         * Creates a plain object from an ActivityResp message. Also converts values to other types if specified.
         * @param message ActivityResp
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.ActivityResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ActivityResp to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for ActivityResp
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a Reward. */
    interface IReward {

        /** Reward itemId */
        itemId?: (number|null);

        /** Reward itemNum */
        itemNum?: (number|null);
    }

    /** Represents a Reward. */
    class Reward implements IReward {

        /**
         * Constructs a new Reward.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IReward);

        /** Reward itemId. */
        public itemId: number;

        /** Reward itemNum. */
        public itemNum: number;

        /**
         * Creates a new Reward instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Reward instance
         */
        public static create(properties?: PK5Client.IReward): PK5Client.Reward;

        /**
         * Encodes the specified Reward message. Does not implicitly {@link PK5Client.Reward.verify|verify} messages.
         * @param message Reward message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IReward, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Reward message, length delimited. Does not implicitly {@link PK5Client.Reward.verify|verify} messages.
         * @param message Reward message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IReward, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Reward message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Reward
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.Reward;

        /**
         * Decodes a Reward message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Reward
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.Reward;

        /**
         * Verifies a Reward message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Reward message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Reward
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.Reward;

        /**
         * Creates a plain object from a Reward message. Also converts values to other types if specified.
         * @param message Reward
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.Reward, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Reward to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for Reward
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an Activity. */
    interface IActivity {

        /** Activity actId */
        actId?: (number|null);

        /** Activity title */
        title?: (string|null);

        /** Activity content */
        content?: (string|null);

        /** Activity imgUrl */
        imgUrl?: (string|null);

        /** Activity actType */
        actType?: (number|null);

        /** Activity rewards */
        rewards?: (PK5Client.IReward[]|null);
    }

    /** Represents an Activity. */
    class Activity implements IActivity {

        /**
         * Constructs a new Activity.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IActivity);

        /** Activity actId. */
        public actId: number;

        /** Activity title. */
        public title: string;

        /** Activity content. */
        public content: string;

        /** Activity imgUrl. */
        public imgUrl: string;

        /** Activity actType. */
        public actType: number;

        /** Activity rewards. */
        public rewards: PK5Client.IReward[];

        /**
         * Creates a new Activity instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Activity instance
         */
        public static create(properties?: PK5Client.IActivity): PK5Client.Activity;

        /**
         * Encodes the specified Activity message. Does not implicitly {@link PK5Client.Activity.verify|verify} messages.
         * @param message Activity message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IActivity, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Activity message, length delimited. Does not implicitly {@link PK5Client.Activity.verify|verify} messages.
         * @param message Activity message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IActivity, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an Activity message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Activity
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.Activity;

        /**
         * Decodes an Activity message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Activity
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.Activity;

        /**
         * Verifies an Activity message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an Activity message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Activity
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.Activity;

        /**
         * Creates a plain object from an Activity message. Also converts values to other types if specified.
         * @param message Activity
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.Activity, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Activity to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for Activity
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetActivityRewardReq. */
    interface IGetActivityRewardReq {

        /** GetActivityRewardReq actId */
        actId?: (number|null);

        /** GetActivityRewardReq uid */
        uid?: (number|null);
    }

    /** Represents a GetActivityRewardReq. */
    class GetActivityRewardReq implements IGetActivityRewardReq {

        /**
         * Constructs a new GetActivityRewardReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetActivityRewardReq);

        /** GetActivityRewardReq actId. */
        public actId: number;

        /** GetActivityRewardReq uid. */
        public uid: number;

        /**
         * Creates a new GetActivityRewardReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetActivityRewardReq instance
         */
        public static create(properties?: PK5Client.IGetActivityRewardReq): PK5Client.GetActivityRewardReq;

        /**
         * Encodes the specified GetActivityRewardReq message. Does not implicitly {@link PK5Client.GetActivityRewardReq.verify|verify} messages.
         * @param message GetActivityRewardReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetActivityRewardReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetActivityRewardReq message, length delimited. Does not implicitly {@link PK5Client.GetActivityRewardReq.verify|verify} messages.
         * @param message GetActivityRewardReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetActivityRewardReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetActivityRewardReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetActivityRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetActivityRewardReq;

        /**
         * Decodes a GetActivityRewardReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetActivityRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetActivityRewardReq;

        /**
         * Verifies a GetActivityRewardReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetActivityRewardReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetActivityRewardReq
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetActivityRewardReq;

        /**
         * Creates a plain object from a GetActivityRewardReq message. Also converts values to other types if specified.
         * @param message GetActivityRewardReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetActivityRewardReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetActivityRewardReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetActivityRewardReq
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetActivityRewardResp. */
    interface IGetActivityRewardResp {

        /** GetActivityRewardResp result */
        result?: (boolean|null);

        /** GetActivityRewardResp uid */
        uid?: (number|null);

        /** GetActivityRewardResp reward */
        reward?: (PK5Client.IReward|null);
    }

    /** Represents a GetActivityRewardResp. */
    class GetActivityRewardResp implements IGetActivityRewardResp {

        /**
         * Constructs a new GetActivityRewardResp.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetActivityRewardResp);

        /** GetActivityRewardResp result. */
        public result: boolean;

        /** GetActivityRewardResp uid. */
        public uid: number;

        /** GetActivityRewardResp reward. */
        public reward?: (PK5Client.IReward|null);

        /**
         * Creates a new GetActivityRewardResp instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetActivityRewardResp instance
         */
        public static create(properties?: PK5Client.IGetActivityRewardResp): PK5Client.GetActivityRewardResp;

        /**
         * Encodes the specified GetActivityRewardResp message. Does not implicitly {@link PK5Client.GetActivityRewardResp.verify|verify} messages.
         * @param message GetActivityRewardResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetActivityRewardResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetActivityRewardResp message, length delimited. Does not implicitly {@link PK5Client.GetActivityRewardResp.verify|verify} messages.
         * @param message GetActivityRewardResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetActivityRewardResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetActivityRewardResp message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetActivityRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetActivityRewardResp;

        /**
         * Decodes a GetActivityRewardResp message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetActivityRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetActivityRewardResp;

        /**
         * Verifies a GetActivityRewardResp message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetActivityRewardResp message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetActivityRewardResp
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetActivityRewardResp;

        /**
         * Creates a plain object from a GetActivityRewardResp message. Also converts values to other types if specified.
         * @param message GetActivityRewardResp
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetActivityRewardResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetActivityRewardResp to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetActivityRewardResp
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetAdRewardReq. */
    interface IGetAdRewardReq {

        /** GetAdRewardReq uid */
        uid?: (number|null);
    }

    /** Represents a GetAdRewardReq. */
    class GetAdRewardReq implements IGetAdRewardReq {

        /**
         * Constructs a new GetAdRewardReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetAdRewardReq);

        /** GetAdRewardReq uid. */
        public uid: number;

        /**
         * Creates a new GetAdRewardReq instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetAdRewardReq instance
         */
        public static create(properties?: PK5Client.IGetAdRewardReq): PK5Client.GetAdRewardReq;

        /**
         * Encodes the specified GetAdRewardReq message. Does not implicitly {@link PK5Client.GetAdRewardReq.verify|verify} messages.
         * @param message GetAdRewardReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetAdRewardReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetAdRewardReq message, length delimited. Does not implicitly {@link PK5Client.GetAdRewardReq.verify|verify} messages.
         * @param message GetAdRewardReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetAdRewardReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetAdRewardReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetAdRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetAdRewardReq;

        /**
         * Decodes a GetAdRewardReq message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetAdRewardReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetAdRewardReq;

        /**
         * Verifies a GetAdRewardReq message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetAdRewardReq message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetAdRewardReq
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetAdRewardReq;

        /**
         * Creates a plain object from a GetAdRewardReq message. Also converts values to other types if specified.
         * @param message GetAdRewardReq
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetAdRewardReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetAdRewardReq to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetAdRewardReq
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a GetAdRewardResp. */
    interface IGetAdRewardResp {

        /** GetAdRewardResp result */
        result?: (boolean|null);

        /** GetAdRewardResp uid */
        uid?: (number|null);

        /** GetAdRewardResp reward */
        reward?: (PK5Client.IReward|null);
    }

    /** Represents a GetAdRewardResp. */
    class GetAdRewardResp implements IGetAdRewardResp {

        /**
         * Constructs a new GetAdRewardResp.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IGetAdRewardResp);

        /** GetAdRewardResp result. */
        public result: boolean;

        /** GetAdRewardResp uid. */
        public uid: number;

        /** GetAdRewardResp reward. */
        public reward?: (PK5Client.IReward|null);

        /**
         * Creates a new GetAdRewardResp instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GetAdRewardResp instance
         */
        public static create(properties?: PK5Client.IGetAdRewardResp): PK5Client.GetAdRewardResp;

        /**
         * Encodes the specified GetAdRewardResp message. Does not implicitly {@link PK5Client.GetAdRewardResp.verify|verify} messages.
         * @param message GetAdRewardResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IGetAdRewardResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GetAdRewardResp message, length delimited. Does not implicitly {@link PK5Client.GetAdRewardResp.verify|verify} messages.
         * @param message GetAdRewardResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IGetAdRewardResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GetAdRewardResp message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GetAdRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.GetAdRewardResp;

        /**
         * Decodes a GetAdRewardResp message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GetAdRewardResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.GetAdRewardResp;

        /**
         * Verifies a GetAdRewardResp message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GetAdRewardResp message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GetAdRewardResp
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.GetAdRewardResp;

        /**
         * Creates a plain object from a GetAdRewardResp message. Also converts values to other types if specified.
         * @param message GetAdRewardResp
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.GetAdRewardResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GetAdRewardResp to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for GetAdRewardResp
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of an Error. */
    interface IError {

        /** Error msg */
        msg?: (string|null);

        /** Error code */
        code?: (number|null);
    }

    /** Represents an Error. */
    class Error implements IError {

        /**
         * Constructs a new Error.
         * @param [properties] Properties to set
         */
        constructor(properties?: PK5Client.IError);

        /** Error msg. */
        public msg: string;

        /** Error code. */
        public code: number;

        /**
         * Creates a new Error instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Error instance
         */
        public static create(properties?: PK5Client.IError): PK5Client.Error;

        /**
         * Encodes the specified Error message. Does not implicitly {@link PK5Client.Error.verify|verify} messages.
         * @param message Error message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: PK5Client.IError, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Error message, length delimited. Does not implicitly {@link PK5Client.Error.verify|verify} messages.
         * @param message Error message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: PK5Client.IError, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an Error message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Error
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): PK5Client.Error;

        /**
         * Decodes an Error message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Error
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): PK5Client.Error;

        /**
         * Verifies an Error message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an Error message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Error
         */
        public static fromObject(object: { [k: string]: any }): PK5Client.Error;

        /**
         * Creates a plain object from an Error message. Also converts values to other types if specified.
         * @param message Error
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: PK5Client.Error, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Error to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for Error
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Key enum. */
    enum Key {
        CONNECT = 1,
        CTS_USER_AUTH_CONNECT = 2,
        STC_USER_AUTH_CONNECT_RES = 3,
        CTS_SPK_GET_BET_RANGE = 100,
        STC_SPK_GET_BET_RANGE_RES = 101,
        CTS_SPK_SET_BET = 102,
        STC_SPK_SET_BET_RES = 103,
        CTS_SPK_GET_MACHINE_LIST = 104,
        STC_SPK_GET_MACHINE_LIST_RES = 105,
        CTS_SPK_GET_MACHINE_DETIAL = 106,
        STC_SPK_GET_MACHINE_DETIAL_RES = 107,
        CTS_SPK_INTO_GAME = 108,
        STC_SPK_INTO_GAME_RES = 109,
        CTS_SPK_BET = 110,
        STC_SPK_BET_RES = 111,
        CTS_SPK_BET_DOUBEL = 112,
        STC_SPK_BET_DOUBEL_RES = 113,
        CTS_SPK_LEAVE_MACHINE = 116,
        STC_SPK_LEAVE_MACHINE_RES = 117,
        STC_ERROR = 900
    }
}
