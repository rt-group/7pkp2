import { _decorator, Component, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Loading')
export class Loading extends Component {
    public static Instance: Loading = null as unknown as Loading;
    public loadingNum: number = 0;
    public loadingStr: string = "%";

        //初始化
        onLoad(): void{
            if(Loading.Instance === null){
                Loading.Instance = this;
            }else{
                this.destroy();
                return;
            }
        }

        init() {
            this.node.getChildByName("Text").getComponent(Label).string = this.loadingNum + this.loadingStr;
        }

        updateLoadingProgress(num: number) {
            this.loadingNum = num;
            this.node.getChildByName("Text").getComponent(Label).string = this.loadingNum + this.loadingStr;
        }
}

