import { _decorator, Component, instantiate, Node, Prefab } from 'cc';
import { RowLoader } from './RowLoader';
import { CardsLoader } from './CardsLoader';
const { ccclass, property } = _decorator;

@ccclass('Loader')
export class Loader extends Component {
    public static Instance: Loader = null as unknown as Loader;

    @property(Prefab)
    rowLoader: Prefab = null;

    @property(Prefab)
    cardLoader: Prefab = null;

    onLoad(): void{
        if(Loader.Instance === null){
            Loader.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
        let rowPrefab = instantiate(this.rowLoader);
        this.node.addChild(rowPrefab);

        let cardPrefab = instantiate(this.cardLoader);
        this.node.addChild(cardPrefab);
    }

    stopRowLoader() {
        this.node.getChildByName("RowLoader").getComponent(RowLoader).stopLoader();
    }

    showRowLoader() {
        this.node.getChildByName("RowLoader").getComponent(RowLoader).showLoader();
    }

    showCardLoader() {
        this.node.getChildByName("CardsLoader").getComponent(CardsLoader).showLoader();
    }

    stopCardLoader() {
        this.node.getChildByName("CardsLoader").getComponent(CardsLoader).stopLoader();
    }

}

