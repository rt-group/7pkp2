import { _decorator, Component, Node, Animation } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('RowLoader')
export class RowLoader extends Component {
    public static Instance: RowLoader = null as unknown as RowLoader;
    @property(Node) iconAnimation: Node = null!;
    callbackFromParent: Function = () => {};
  
    start() {}
  
    onLoad() {
      if(RowLoader.Instance === null){
        RowLoader.Instance = this;
      }else{
        this.destroy();
        return;
      }
      this.node.active = false;
    }
  
    /**
     * @en
     * Use startTimer to start the timer,
     * It will end automatically and calls callback function on time over
     * @param msgString  Message to be displayed on the PopUp
     * @param func  The callback function which will be called on cross buttons callback.
     */
    showLoader() {
      this.node.active = true;
      this.iconAnimation.getComponent(Animation).play();
    }
  
    stopLoader(func: Function = () => {}) {
      this.node.active = false;
      this.iconAnimation.getComponent(Animation).stop();
      this.callbackFromParent = func;
      this.callbackFromParent && this.callbackFromParent();
    }
}

