import { _decorator, Component, Node, Label, Animation, Vec3, Widget } from "cc";
const { ccclass, property } = _decorator;

export enum LoaderType {
  FULL_SCREEN = "Full_Screen",
  ONLY_MESSAGE = "ONLY_MESSAGE",
}

/**
 * @title PopUp class
 * @author harpinder_singh
 * @notice this class manages the popUps.
 */
@ccclass("CardsLoader")
export class CardsLoader extends Component {
  public static Instance: CardsLoader = null as unknown as CardsLoader;
  @property(Node) iconAnimation: Node = null!;
  callbackFromParent: Function = () => {};

  start() {}

  onLoad() {
    if(CardsLoader.Instance === null){
        CardsLoader.Instance = this;
    }else{
      this.destroy();
      return;
    }
    this.node.active = false;
  }

  /**
   * @en
   * Use startTimer to start the timer,
   * It will end automatically and calls callback function on time over
   * @param msgString  Message to be displayed on the PopUp
   * @param func  The callback function which will be called on cross buttons callback.
   */
  showLoader() {
    this.node.active = true;
    this.iconAnimation.getComponent(Animation).play();
  }

  stopLoader(func: Function = () => {}) {
    this.node.active = false;
    this.iconAnimation.getComponent(Animation).stop();
    this.callbackFromParent = func;
    this.callbackFromParent && this.callbackFromParent();
  }
}
