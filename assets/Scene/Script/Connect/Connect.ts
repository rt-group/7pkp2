import { CCInteger, Component, _decorator, director ,Prefab ,instantiate ,Node ,UITransform ,Label} from "cc";
const {ccclass, property} = _decorator;
import {NotifyComponent } from "../../../../../framework-fish/Core/view/NotifyComponent";
import { STATE } from "../Constant/State";
import { SOCKETSTATE, WSSocket } from "../../../../../framework-fish/Core/lib/Connect/WSSocket";
import { NetWork, SOCKETTYPE } from "../../../../../framework-fish/Core/Adapter/WebsocketAdopter";
import proto from "proto";
import { ENV } from "./Enivornment";
import { MsgHelp } from "../Utils/MsgHelp";
import { PopupManager } from "../Manager/PopupManager";
import { POPUP_BTN_TITLE, POPUP_ICON_TYPE, POPUP_TITLE } from "../Common/Popup";
import { POPUP_MESSAGE } from "../Utils/GameConfig";
// import { SystemProto} from "../proto/systemProto";
@ccclass('Connect')
export class Connect extends NotifyComponent {
//     // start is the main state for ws entry point
//     // recived data and send to server
    public static Instance: Connect = null as unknown as Connect;
    state = STATE.CS_CONNECT;
    wsConn: WSSocket= null ;
    hbInteraval: any = null;

    isConnect= false;

    onLoad(): void{
        if(Connect.Instance === null){
            Connect.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    async start(){
        
        this.connect();
    }

    async connect(){
        console.log( proto.PK5Client);
        this.wsConn = NetWork.getConnect(SOCKETTYPE.WEBSOCKET) as WSSocket;
       
        console.log("COnnecting..this.wsConn.state .",this.wsConn.state)
        if(this.wsConn.state == SOCKETSTATE.CONNECT){
            return;
        }
        this.wsConn.onmessage= this.wsMessage.bind(this);
        // this.wsConn.onopen= this.wsOpen.bind(this);
        this.wsConn.onclose= this.wsClose.bind(this);
        this.wsConn.init(ENV.WS_URL);
                         
    }

    wsOpen(){
        this.wsConn.state = SOCKETSTATE.CONNECT;
        console.log("ws Open") 
        this.isConnect = true;

        var auth = proto.PK5Client.AuthConnect;
        var authData =auth.create({
            account:ENV.Account,
            pwd:ENV.Pwd,
            vendor:ENV.Vendor,
            platform: "web",
            token: ""
        });
        var buffer = auth.encode(authData).finish();
        var key = proto.PK5Client.Key.CTS_USER_AUTH_CONNECT;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws")
        this.sendWS(byteData );
    }

    wsMessage(e){
        console.log("ws mesage get :" ,e )
        //make auth
        var data = e.data;

        var msgObj = MsgHelp.decodeMsg(data);

        var key = msgObj.key;
        var byteData = msgObj.data;

        console.log("key:",key);
        
        this.call(key ,{
            data: byteData
        })
       
    }

    wsClose(e){
        console.log("WS CLOSE",e);
        this.isConnect = false;
        if(this.wsConn){
            this.wsConn.state = SOCKETSTATE.CLOSE;
        }
        PopupManager.Instance.showPopup(POPUP_ICON_TYPE.ERROR_INTERNET, POPUP_TITLE.SOMETHING_WENT_WRONG, POPUP_MESSAGE.NO_INTERNET_CONNECTION, POPUP_BTN_TITLE.RECONNECT)
    }

    send(data: any){
        if( this.wsConn.state == SOCKETSTATE.CONNECT){
            this.sendWS(data);
        }
            
    }


    notify(data: any): void {
        console.log(data);
    }
}