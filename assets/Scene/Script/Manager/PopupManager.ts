import { _decorator, Component, instantiate, Node } from 'cc';
import { ResMgr } from './ResMgr';
import { Resource } from '../Resource';
import { Popup, POPUP_BTN_TITLE, POPUP_ICON_TYPE, POPUP_TITLE } from '../Common/Popup';
import { ConfirmPopup } from '../Common/ConfirmPopup';
import { POPUP_MESSAGE } from '../Utils/GameConfig';
const { ccclass, property } = _decorator;

@ccclass('PopupManager')
export class PopupManager extends Component {
    public static Instance: PopupManager = null as unknown as PopupManager;

    popup = null;
    confirmPop = null;

    onLoad(): void{
        if(PopupManager.Instance === null){
            PopupManager.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {
    }

    createPopup() {
        let popupPrefab = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.ErrorMsg.Popup);
        let popup = instantiate(popupPrefab);
        this.popup = popup;
        this.node.addChild(popup);

        let confirmPopupPrefab = ResMgr.Instance.getAsset(Resource.prefabStr, Resource.prefab.ErrorMsg.ConfirmPopup);
        let confirmPopup = instantiate(confirmPopupPrefab);
        this.confirmPop = confirmPopup;
        this.node.addChild(confirmPopup);

        this.hidePopup();
        this.hideConfirmPopup();
    }


    showPopup(iconType: POPUP_ICON_TYPE, popTitle: POPUP_TITLE, popMessage: POPUP_MESSAGE, popupBtn: POPUP_BTN_TITLE) {
        this.popup.getComponent(Popup).updatePopupTitle(iconType, popTitle, popMessage, popupBtn);
        this.popup.getComponent(Popup).showPopup();
    }

    hidePopup() {
        this.popup.getComponent(Popup).hidePopup();
    }

    showConfirmPopup() {
        this.confirmPop.getComponent(ConfirmPopup).updateMessage("Do you want to resume you existing game");
        this.confirmPop.getComponent(ConfirmPopup).showConfirmPopup();
    }

    hideConfirmPopup() {
        this.confirmPop.getComponent(ConfirmPopup).hideConfirmPopup();
    }
}

