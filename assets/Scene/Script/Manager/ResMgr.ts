import { _decorator, AssetManager, assetManager, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('ResMgr')
export class ResMgr extends Component {
    public static Instance: ResMgr = null as unknown as ResMgr;

    public abBunds: any = {};
    private total: number = 0;
    private now: number = 0;
    private progressFunc: Function | null = null;
    private endFunc: Function | null = null;

    private nowAb: number = 0;
    private totalAb: number = 0;
    private count: number = 0;
    public init(): void {
    }

    //讀取bundle
    private loadAssetsBundle(abName: string, endFunc: Function): void {
        assetManager.loadBundle(abName, (err, bundle) => {
            if (err !== null) {
                console.log("[ResMgr]: Load AssetsBundle Error: " + err);
                console.log("[ResMgr]: Load AssetsBundle Error: " + abName);
                this.abBunds[abName] = null;
            } else {
                console.log("[ResMgr]: Load AssetsBundle Success: " + abName);
                this.abBunds[abName] = bundle;
            }

            if (endFunc) {
                endFunc();
            }
        })
    }

    loadCompleted():void {
        if (this.endFunc !== null) {
            this.endFunc();
        }

    }

    //初始化
    onLoad(): void {
        if (ResMgr.Instance === null) {
            ResMgr.Instance = this;
        } else {
            this.destroy();
            return;
        }
    }

    //讀取資源     
    private loadRes(abBundle: any, url: any, typeClass: any): Promise<AssetManager.Bundle> {
        return new Promise((resolve, reject) => {
            try {
                abBundle.load(url, typeClass, (error: any, asset: any) => {
                    this.now++;
                    if (error) {
                        this.count -- ;
                        reject(error)
                        console.log("load Res " + url + " error: " + error);
                    } else {
                        console.log("load Res " + url + " success");
                        resolve(asset);
                    }

                    if (this.progressFunc) {
                        this.progressFunc(this.now, this.total);
                    }
                
                    if (this.now >= this.count) {
                        if (this.endFunc !== null) {
                            this.endFunc();
                        }
                    }
                })
            } catch (err) {
                console.log(err);
                reject(err);
            }
        })
    }


    //取得資源
    public getAsset(abName: string, resUrl: string): any {
        var bundle = assetManager.getBundle(abName);
        if (bundle === null) {
            console.log("[error]: " + abName + " AssetsBundle not loaded!!!");
            return null;
        }
        return bundle.get(resUrl);
    }

    //釋放資源
    public releaseResPackage(resPkg: any) {
        for (var key in resPkg) {
            var urlSet = resPkg[key].urls;
            for (var i = 0; i < urlSet.length; i++) {
                var bundle: any = assetManager.getBundle(key);
                if (bundle === null) {
                    console.log("[error]: " + key + "AssetsBundle not loaded!!!");
                    continue;
                }
                assetManager.releaseAsset(bundle.get(urlSet[i]));
            }
        }
    }

    //從bundle內取得資源
    public async loadAssetsInAssetsBundle(resPkg: any): Promise<void> {
        for (var key in resPkg) {
            var urlSet = resPkg[key].urls;
            var typeClass = resPkg[key].assetType;
            for (var i = 0; i < urlSet.length; i++) {
                await this.loadRes(this.abBunds[key], urlSet[i], typeClass)                
            }
        }
    }

    //預加載資源
    public preloadResPackage(resPkg: any, progressFunc: Function, endFunc: Function): void {
        this.total = 0;
        this.now = 0;
        this.totalAb = 0;
        this.nowAb = 0;
        this.count = 0;

        this.progressFunc = progressFunc;
        this.endFunc = endFunc;
        for (var key in resPkg) {
            this.totalAb++;
            this.total += resPkg[key].urls.length;
            this.count += resPkg[key].urls.length;
        }

        for (var key in resPkg) {
            this.loadAssetsBundle(key, () => {
                this.nowAb++;
                if (this.nowAb === this.totalAb) {
                    this.loadAssetsInAssetsBundle(resPkg);
                }
            })
        }
    }
}

