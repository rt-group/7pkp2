import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('GameManager')
export class GameManager extends Component {
    public static Instance: GameManager = null as unknown as GameManager;

    onLoad(): void{
        if(GameManager.Instance === null){
            GameManager.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    start() {

    }

    update(deltaTime: number) {
        
    }
}

