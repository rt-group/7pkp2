import { _decorator, Button, Component, Label, Node } from 'cc';
const { ccclass, property } = _decorator;

import proto from "proto";
import { Connect } from '../Connect/Connect';
import { MsgHelp } from '../Utils/MsgHelp';
import { GameInfo } from '../Utils/GameConfig';
@ccclass('RoomManager')
export class RoomManager extends Component {
    public static Instance: RoomManager = null as unknown as RoomManager;

    @property(Node)
    room: Node = null;

    public betRange = [];
    public currentRoom = null;
    public roomList = null;
    public maxMachineNum = null;

    onLoad(): void{
        if(RoomManager.Instance === null){
            RoomManager.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }


    init() {
        this.roomList = this.node.getChildByName("RoomList").children;
    }

    chooseRoom(event, customEventData) {
        this.currentRoom = this.betRange[customEventData].betIndex;
        this.maxMachineNum = this.betRange[customEventData].maxMachineNum;
        GameInfo.currentRoomRange = this.betRange[this.currentRoom - 1];
        this.CS_SetBet();
    }

    CS_SetBet() {
        var setBet = proto.PK5Client.SetBet;
        console.log("CS_SETBET", this.currentRoom);
        var setBetData =setBet.create({
            betIndex: this.currentRoom
        });
        var buffer = setBet.encode(setBetData).finish();
        var key = proto.PK5Client.Key.CTS_SPK_SET_BET;
        var byteData =MsgHelp.encodeMsg(key,buffer);
        console.log("send to ws");
        Connect.Instance.sendWS(byteData );
    }

    // CS_GetMachineList() {
    //     var getMachineList = proto.PK5Client.GetMachineList;
    //     var getMachineListData =getMachineList.create({
    //         betIndex: this.currentRoom
    //     });
    //     var buffer = getMachineList.encode(getMachineListData).finish();
    //     var key = proto.PK5Client.Key.CTS_SPK_GET_MACHINE_LIST;
    //     var byteData =MsgHelp.encodeMsg(key,buffer);
    //     console.log("send to ws");
    //     Connect.Instance.sendWS(byteData );
    // }

    /**
     * 
     * @param betRange 設置房間資料
     */
    setRoomData(betRange) {
        this.init();
        this.betRange = betRange.betranges;
        this.updateRoomData();
    }

    updateRoomData() {
        for(let a = 0; a < this.betRange.length; a++) {
            let range = this.betRange[a].betName.split("-");
            this.room.children[a].getChildByName("Num1").getComponent(Label).string = range[0];
            this.room.children[a].getChildByName("Num2").getComponent(Label).string = (parseInt(range[1]) * 4).toString();
        }
    }

    /**
     * 
     * @returns 取得現在房間資料
     */
    getCurrentRoomDate() {
        return this.betRange[this.currentRoom];
    }
}

