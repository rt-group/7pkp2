import { _decorator, AudioSource, Component, HeightField, ImageAsset, JsonAsset, Node, Prefab, SpriteFrame } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Resource')
export class Resource extends Component {
    public static Instance: Resource = null as unknown as Resource;

    onLoad(): void{
        if(Resource.Instance === null){
            Resource.Instance = this;
        }else{
            this.destroy();
            return;
        }
    }

    public static prefabStr = "Prefab";
    public static soundStr = "Sound";
    public static pokerStr = "Poker";
    public static cardIconStr = "CardIcon"

    public static sound = {
        BGM: "Background",
        Bet: "Bet",
        Button: "Button",
        Click: "Click",
        Click2: "Click2",
        JP: "JP",
        Less4King: "Less4King",
        More4King: "More4King",
        NoWin: "NoWin",
        GameMusic: "GAME_MUSIC",
        ButtonClick: "BUTTON_CLICK"
    }

    public static CardIcon = {
        CLUB: "club",
        DIAMOND: "diamond",
        HEART: "heart",
        SPADE: "spade"
    }

    public static Poker = {
        c1: "1c",
        d1: "1d",
        h1: "1h",
        s1: "1s",
        c2: "2c",
        d2: "2d",
        h2: "2h",
        s2: "2s",
        c3: "3c",
        d3: "3d",
        h3: "3h",
        s3: "3s",
        c4: "4c",
        d4: "4d",
        h4: "4h",
        s4: "4s",
        c5: "5c",
        d5: "5d",
        h5: "5h",
        s5: "5s",
        c6: "6c",
        d6: "6d",
        h6: "6h",
        s6: "6s",
        c7: "7c",
        d7: "7d",
        h7: "7h",
        s7: "7s",
        c8: "8c",
        d8: "8d",
        h8: "8h",
        s8: "8s",
        c9: "9c",
        d9: "9d",
        h9: "9h",
        s9: "9s",
        c10: "10c",
        d10: "10d",
        h10: "10h",
        s10: "10s",
        c11: "11c",
        d11: "11d",
        h11: "11h",
        s11: "11s",
        c12: "12c",
        d12: "12d",
        h12: "12h",
        s12: "12s",
        c13: "13c",
        d13: "13d",
        h13: "13h",
        s13: "13s",
        b14: "14b",
        r14: "14r",
        cardBack: "cardBack"
    }

    public static prefab = {
        MiniGame: {
            MiniGame: "MiniGame/MiniGame",
        },
        Setting: {
            SettingPopup: "Setting/SettingPopup",
        },
        UI: {
            LoadingTable: "UI/LoadingTable",
            LobbyTable: "UI/Lobby",
            LoginTable: "UI/LoginTable",
            RuleTable: "UI/RuleTable"
        },
        Lobby: {
            Machine: "Lobby/Machine",
            Room: "Lobby/RoomTable",
        },
        Load: {
            CardsLoader: "Load/CardsLoader",
        },
        Game: {
            BetTable: "Game/BetTable",
            BonusTable: "Game/BonusTable",
            Card: "Game/Card",
            Game: "Game/Game",
            Chip: "Game/Chip",
            WinLoseLayer: "Game/WinLoseLayer",
            Prediction: "Game/Prediction",
            JPTable: "Game/JPTable",
        },
        ErrorMsg: {
            Popup: "ErrorMsg/Popup",
            ConfirmPopup: "ErrorMsg/ConfirmPopup",
        },
        Common: {
            SidePanel: "Common/SidePanel"
        },

    }

    public static loginPkg = {
        Prefab: {
            assetType: Prefab,
            urls: [
                Resource.prefab.ErrorMsg.Popup,
                Resource.prefab.ErrorMsg.ConfirmPopup,
                Resource.prefab.UI.LoginTable,
            ]
        }
    }

      //資源路徑
    public static loadPkg = {
        Prefab: {
            assetType: Prefab,
            urls: [
                Resource.prefab.UI.LoadingTable
            ]
        }
    }



    public static resPkg = {
        CardIcon:{
            assetType: ImageAsset,
            urls: [
                Resource.CardIcon.CLUB,
                Resource.CardIcon.DIAMOND,
                Resource.CardIcon.HEART,
                Resource.CardIcon.SPADE
            ]
        },
        Poker: {
            assetType: ImageAsset,
            urls: [
                Resource.Poker.c1,
                Resource.Poker.d1,
                Resource.Poker.h1,
                Resource.Poker.s1,
                Resource.Poker.c2,
                Resource.Poker.d2,
                Resource.Poker.h2,
                Resource.Poker.s2,
                Resource.Poker.c3,
                Resource.Poker.d3,
                Resource.Poker.h3,
                Resource.Poker.s3,
                Resource.Poker.s4,
                Resource.Poker.c4,
                Resource.Poker.d4,
                Resource.Poker.h4,
                Resource.Poker.s4,
                Resource.Poker.c5,
                Resource.Poker.d5,
                Resource.Poker.h5,
                Resource.Poker.s5,
                Resource.Poker.c6,
                Resource.Poker.d6,
                Resource.Poker.h6,
                Resource.Poker.s6,
                Resource.Poker.c7,
                Resource.Poker.d7,
                Resource.Poker.h7,
                Resource.Poker.s7,
                Resource.Poker.s8,
                Resource.Poker.c8,
                Resource.Poker.d8,
                Resource.Poker.h8,
                Resource.Poker.c9,
                Resource.Poker.d9,
                Resource.Poker.h9,
                Resource.Poker.s9,
                Resource.Poker.c10,
                Resource.Poker.d10,
                Resource.Poker.h10,
                Resource.Poker.s10,
                Resource.Poker.c11,
                Resource.Poker.d11,
                Resource.Poker.h11,
                Resource.Poker.s11,
                Resource.Poker.c12,
                Resource.Poker.d12,
                Resource.Poker.h12,
                Resource.Poker.s12,
                Resource.Poker.c13,
                Resource.Poker.d13,
                Resource.Poker.h13,
                Resource.Poker.s13,
                Resource.Poker.b14,
                Resource.Poker.r14,
                Resource.Poker.cardBack
            ]
        },
        Sound: {
            assetType: AudioSource,
            urls: [
                Resource.sound.BGM,
                Resource.sound.Bet,
                Resource.sound.Button,
                Resource.sound.Click,
                Resource.sound.Click2,
                Resource.sound.JP,
                Resource.sound.Less4King,
                Resource.sound.More4King,
                Resource.sound.NoWin
            ]
        },
        Prefab: {
            assetType: Prefab,
            urls: [
                Resource.prefab.Common.SidePanel,
                Resource.prefab.Game.BetTable,
                Resource.prefab.Game.BonusTable,
                Resource.prefab.Game.Card,
                Resource.prefab.Game.Game,
                Resource.prefab.Game.Chip,
                Resource.prefab.Game.Prediction,
                Resource.prefab.Game.WinLoseLayer,
                Resource.prefab.Game.JPTable,
                Resource.prefab.Load.CardsLoader,
                Resource.prefab.Lobby.Machine,
                Resource.prefab.Lobby.Room,
                Resource.prefab.MiniGame.MiniGame,
                Resource.prefab.Setting.SettingPopup,
                // Resource.prefab.UI.LoadingTable,
                Resource.prefab.UI.LobbyTable,
                // Resource.prefab.UI.LoginTable,
                Resource.prefab.UI.RuleTable
            ],
        },
    };
}

